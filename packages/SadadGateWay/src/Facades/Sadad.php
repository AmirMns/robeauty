<?php

namespace SadadGateWay\Sadad\Facades;

use Illuminate\Support\Facades\Facade;

class Sadad extends Facade
{
    protected static function getFacadeAccessor() { return 'sadadgateway'; }
}

