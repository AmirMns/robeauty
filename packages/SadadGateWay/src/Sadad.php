<?php

namespace SadadGateWay\Sadad;

class Sadad
{
    protected $Key;
    protected $MerchantId;
    protected $TerminalId;
    protected $url         = "https://sadad.shaparak.ir/vpg/api/v0/Request/PaymentRequest";
    protected $gateway_url = "https://sadad.shaparak.ir/VPG/Purchase?Token=";
    protected $verify_url  = 'https://sadad.shaparak.ir/vpg/api/v0/Advice/Verify';
    public $amount;

    public function __construct($key, $merchant_id, $terminal_id)
    {
        $this->Key = $key;
        $this->MerchantId = $merchant_id;
        $this->TerminalId = $terminal_id;
    }
    public function encrypt_pkcs7($str, $key)
    {
        $key = base64_decode($key);
        $ciphertext = OpenSSL_encrypt($str,"DES-EDE3", $key, OPENSSL_RAW_DATA);
        return base64_encode($ciphertext);
    }
    public function signedData($order_id)
    {
        return $this->encrypt_pkcs7("$this->TerminalId;$order_id;$this->amount","$this->Key");
    }
    public function CallAPI($url, $data = false)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data)));
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;

    }
    public function send($order_id, $redirect_url, $amount)
    {
        $order_id = time();
        $this->amount = $amount * 10;
        $signed_data = $this->signedData($order_id);
        $data = [
            'TerminalId' => $this->TerminalId,
            'MerchantId' => $this->MerchantId,
            'Amount'     => $this->amount,
            'SignData'   =>  $signed_data,
	        'ReturnUrl'  => $redirect_url,
	        'LocalDateTime' => date("m/d/Y g:i:s a"),
            'OrderId'       => $order_id
        ];
        $res = $this->CallAPI($this->url, json_encode($data));
        return json_decode($res);
    }
    public function redirect($res_array)
    {
        if ($res_array->ResCode==0) {
            $Token= $res_array->Token;
            $url= $this->gateway_url . $Token;
            return redirect()->to($url);
        } else {
            return back();
        }
    }
    public function validate($token, $res_code)
    {
        if ($res_code == "0") {
            $verifyData = array('Token' => $token,'SignData'=> $this->encrypt_pkcs7($token,$this->Key));
            $str_data = json_encode($verifyData);
            $res = $this->CallAPI($this->verify_url, $str_data);
            $arrres = json_decode($res);
            if($arrres->ResCode != -1 && $arrres->ResCode == 0) {
                return [
                    'RefID'   => $arrres->RetrivalRefNo,
                    'TraceID' => $arrres->SystemTraceNo,
                    'OrderID' => $arrres->OrderId
                ];
            }
        }
        return false;
    }
}
