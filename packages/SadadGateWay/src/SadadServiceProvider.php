<?php

namespace SadadGateWay\Sadad;

use Illuminate\Support\ServiceProvider;

class SadadServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }
    public function register()
    {
        $this->app->singleton('sadadgateway', function($app) {
            $config = config('services.sadad');
            return new Sadad($config['key'], $config['merchant_id'], $config['terminal_id']);
        });
    }
}
