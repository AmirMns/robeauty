@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Posts list</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3 float-left">
        <a href="{{ route('Admin.posts.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a>
        <h1 class="float-left">Posts</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Count</th>
                <th scope="col">Price</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $post)
            <tr>
                <th scope="row">{{ $post->code }}</th>
                <td>{{ $post->title }}</td>
                <td>{{ $post->count }}</td>
                <td>{{ $post->price }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.posts.edit',['id' => $post->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.posts.destroy',['id' => $post->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $posts->links() }}
    </div>
@endsection
