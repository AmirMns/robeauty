@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Add a post</title>
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
        <h1>Add new post</h1>
        <form action="{{ route('Admin.posts.store') }}" id="addnewpost" method="POST" role="form" enctype="multipart/form-data"
        class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-sm-6 float-left mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control" name="title" id="title"
                value="{{ old('title') }}" placeholder="Post Title">
            </div>
            <div class="input-group col-12 float-left mb-4">
				<div class="col-12" id="toolbar"></div>
				<div class="col-12" id="editor"></div>
                <div class="col-12 p-0 mt-3">
                    <button class="btn btn-outline-primary">Save</button>
                </div>
            </div>

        </form>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/quill.js') }}"></script>
    <script>
    var toolbarOptions = {
		container: ['bold','italic','underline','link','image','strike',
		{'header': [1,2,3,4,5,6,false]},
		{'list': 'ordered'}, {'list': 'bullet'},
		{'script': 'sub'}, {'script': 'super'},
		{'indent': '-1'}, {'indent': '+1'},
		{'direction': 'rtl'}
		],
		'handlers': {
			// handlers object will be merged with default handlers object
			'link': function(value) {
			if (value) {
				var href = prompt('Enter the URL');
				this.quill.format('link', href);
			} else {
				this.quill.format('link', false);
			}
			},
			'image': function(value) {
				this.quill.insertEmbed(10, 'image', 'https://quilljs.com/images/cloud.png');
			}
		}
	};
	var quill = new Quill('#editor', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
    $('#addnewpost').on('submit', function(event){
        event.preventDefault();
        $('.ql-editor').attr('name', 'body');
    });
    $('#post_tags').select2(); </script>
@endsection
