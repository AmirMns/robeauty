@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>ویرایش محصول #{{ $product->id }}</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-0 float-left">
        <a href="{{ route('Admin.products.index') }}" class="btn btn-info float-left mt-2 mr-2">
            <i class="fas fa-angle-left"></i>
        </a>
        <h1 class="float-right">ویرایش محصول #{{ $product->id }}</h1>
        <div class="col-12 p-0 float-right" data-csrf="{{ csrf_token() }}" selectmulti="true"
        data-mainpage="{{ route('home') }}" data-target="{{ route('setphoto', ['id' => $product->id , 'method' => 'products']) }}"
        data-api-token="{{ auth()->user()->api_token }}" id="react-collapsable-gallry"></div>
    </div>
    <div class="col-12 p-4 float-left swiper-container pro-photos">
        <div class="swiper-wrapper">
            @foreach ($product->photos as $photo)
            <div class="swiper-slide float-left">
                <form action="{{ route('Admin.products.photo.delete', ['photo' => $photo->id]) }}" style="position:absolute;top:0;left:0;" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger"><i class="fas fa-times"></i></button>
                </form>
                <a data-lity href="{{ route('home') }}/{{ $photo->file->path }}">
                    <img src="{{ asset($photo->file->thumbnail_path) }}" alt="{{ $photo->file->alt }}">
                </a>
            </div>
            @endforeach
        </div>
        <!-- Add Pagination -->
		<div class="swiper-pagination"></div>
		<button class="swiper-button-next"></button>
		<button class="swiper-button-prev"></button>
    </div>
    <form action="{{ route('Admin.products.update',['product' => $product->id]) }}"
    method="POST" role="form" enctype="multipart/form-data"
    class="float-right col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @include('SHOPCMSVIEW::Pages.Products.edit_form')
    </form>
@endsection
@section('scripts')
    <script>
    $('#product_tags').select2();
    var swiper = new Swiper.default('.swiper-container', {
    effect: 'coverflow',
    autoplay: {
        delay: 2000,
    },
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    loop: false,
    coverflowEffect: {
        rotate: 0,
        stretch: 0,
        depth: 200,
        modifier: 1,
        slideShadows : true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        // when window width is <= 320px
        320: {
        slidesPerView: 1,
        spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
        slidesPerView: 2,
        spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
        slidesPerView: 3,
        spaceBetween: 30
        }
    },
    pagination: {
        el: '.swiper-pagination',
    }, });</script>
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ asset('js/CollapsableGallery.js') }}"></script>
    <script src="{{ asset('js/ck-editor.js') }}"></script>
@endsection
