@csrf
@method('PATCH')
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-font"></i></span>
    </div>
    <input required type="text" class="form-control" name="title" id="title"
    value="{{ (old('title')) ? old('title') : $product->title }}" placeholder="Product Title">
</div>

<div class="input-group col-12 col-sm-8 float-right mb-4">
    <textarea required class="form-control p-3" name="short_description" id="short_description" placeholder="Description ..."
    rows="10" style="resize:none;" maxlength="300"
    >{{ (old('short_description')) ? old('short_description') : $product->short_description }}</textarea>
</div>

<div id="react-content-editor" data-api="{{ route('api.products.show', ['product' => $product->id]) }}" data-lang='en' class="react-content-editor col-12 float-left mb-4"></div>

<div class="input-group col-12 col-sm-8 float-right mb-4">
    <textarea required class="form-control p-3" name="description" id="description" placeholder="Description ..."
    rows="10" style="resize:none;"
    >{{ (old('description')) ? old('description') : $product->description }}</textarea>
</div>

<textarea name="english_description" id="english_description" class="d-none"></textarea>

<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-euro-sign"></i></span>
    </div>
    <input required type="text" class="form-control" name="price" id="price"
    value="{{ (old('price')) ? old('price') : $product->price }}" placeholder="Price EUR">
</div>
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
    </div>
    <input required type="number" class="form-control" name="quantity" id="quantity"
    value="{{ old('quantity') ? old('quantity') : $product->quantity }}" min="0" placeholder="Count">
</div>
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-percentage"></i></span>
    </div>
    <input type="number" class="form-control" name="off" id="off"
    value="{{ (old('off')) ? old('off') : $product->off }}" min="0" max="100" value="0" placeholder="Discount | e.g : 50">
</div>
<div class="input-group col-12 float-right mb-4">
    <input type="checkbox" name="active" id="active" value="{{ old('active') }}" checked>
    <label for="active">Active</label>
</div>
<div class="input-group col-12 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-tag">Tags</i></span>
    </div>
    <select id="product_tags" name="tags[]" title="tags" class="form-control col-6" multiple>
        @foreach ($tags as $tag)
            <option @foreach ($product->tags as $ptag)
            @if ($ptag->id == $tag->id) selected @endif
            @endforeach
            value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>
</div>
<div class="input-group col-12 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-tag">برند</i></span>
    </div>
    <select id="product_tags" name="brand" title="brand" class="form-control col-6">
        @foreach (brands() as $brand)
            <option @if ($product->brand_id == $brand->id) selected @endif value="{{ $brand->id }}">{{ $brand->name }} - {{ $brand->description }}</option>
        @endforeach
    </select>
</div>
<div class="input-group col-12 col-sm-6 m-0 float-right">
    <button type="submit" name="createbanner" class="btn btn-outline-primary">بروزرسانی</button>
</div>
