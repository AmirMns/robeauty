@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>ایجاد محصول</title>
@endsection
@section('pagecontent')
    <h1 class="col-12">ایجاد محصول جدید</h1>
    <form action="{{ route('Admin.products.store') }}" method="POST" role="form" enctype="multipart/form-data"
    class="float-right col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @include('SHOPCMSVIEW::Pages.Products.form')
    </form>
@endsection
@section('scripts')
    <script> $('#product_tags').select2(); </script>
@endsection
