@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Products list</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3 float-left">
        <a href="{{ route('Admin.products.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right">محصولات</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">عنوان</th>
                <th scope="col">تعداد</th>
                <th scope="col">قیمت</th>
                <th scope="col">تخفیف</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
            <tr>
                <th scope="row">{{ $loop->index + 1 }}</th>
                <td>{{ $product->title }}</td>
                <td>{{ $product->quantity }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ zeroIfNull($product->off) }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.products.edit',['product' => $product->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.products.destroy',['product' => $product->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 float-left text-center">
        {{ $products->links() }}
    </div>
@endsection
