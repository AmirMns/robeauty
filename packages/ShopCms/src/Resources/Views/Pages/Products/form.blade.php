@csrf
{{-- Title --}}
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-font"></i></span>
    </div>
    <input required type="text" class="form-control" name="title" id="title"
    value="{{ old('title') }}" placeholder="عنوان">
</div>
{{-- Description --}}
<div class="input-group col-12 col-sm-8 float-right mb-4">
    <textarea required class="form-control p-3" name="short_description" id="short_description" placeholder="چکیده ..."
    rows="5" style="resize:none;" maxlength="300">{{ old('short_description') }}</textarea>
</div>
{{-- Description --}}
<div class="input-group col-12 col-sm-8 float-right mb-4">
    <textarea required class="form-control p-3" name="description" id="description" placeholder="توضیحات ..."
    rows="10" style="resize:none;">{{ old('description') }}</textarea>
</div>
{{-- Price --}}
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
    </div>
    <input required type="text" class="form-control" name="price" id="price"
    value="{{ old('price') }}" placeholder="قیمت به تومان">
</div>
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-box-open"></i></span>
    </div>
    <input required type="number" class="form-control" name="quantity" id="quantity"
    value="{{ old('quantity') }}" min="0" value="0" placeholder="تعداد">
</div>
{{-- <div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-box"></i></span>
    </div>
    <input type="number" class="form-control" name="max_allowed_count_to_show" id="max_allowed_count_to_show"
    value="{{ old('max_allowed_count_to_show') }}" min="0" placeholder="Max allowed count to show">
</div> --}}
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-percentage"></i></span>
    </div>
    <input type="number" class="form-control" name="discount" id="discount"
    value="{{ old('discount') }}" min="0" max="100" value="0" placeholder="تخفیف | مثال : 50">
</div>
{{-- <div class="input-group col-12 float-right mb-4">
    <input type="checkbox" name="active" id="active" value="{{ old('active') }}" checked>
    <label for="active">Active</label>
</div> --}}
<div class="input-group col-12 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-tag">برچسب ها</i></span>
    </div>
    <select id="product_tags" name="tags[]" title="tags" class="form-control col-6" multiple>
        @foreach ($tags as $tag)
            <option value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>
</div>
<div class="input-group col-12 col-sm-6 m-0 float-right">
    <button type="submit" name="createbanner" class="btn btn-outline-primary">Create</button>
</div>
