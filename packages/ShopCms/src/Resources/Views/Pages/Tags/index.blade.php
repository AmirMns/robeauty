@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>لیست برچسب ها</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-right p-3">
        <a href="{{ route('Admin.tags.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right">برچسب ها</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">نام</th>
                <th scope="col">دسته</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tags as $tag)
            <tr>
                <th scope="row">{{ str_replace('-', ' ', $tag->name) }}</th>
                <th>{{ $tag->type }}</th>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.tags.edit',['tag' => $tag->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.tags.destroy',['tag' => $tag->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 float-left text-center">
        {{ $tags->links() }}
    </div>
@endsection
