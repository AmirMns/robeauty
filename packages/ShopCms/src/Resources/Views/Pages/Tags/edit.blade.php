@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>ویرایش برچسب {{ $tag->name }}</title>
@endsection
@section('pagecontent')
    <h1 class="col-12">ویرایش برچسب {{ $tag->name }}</h1>
    <form action="{{ route('Admin.tags.update', ['tag' => $tag->id]) }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    @method('PATCH')
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="name" id="name"
        value="{{ (old('name')) ? old('name') : $tag->name }}" placeholder="نام برچسب">
    </div>
    <div class="input-group col-12 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-tag">دسته</i></span>
        </div>
        <select id="tag_types" name="type" title="type" class="form-control col-6">
            @foreach ($types as $type)
                <option @if ($type == $tag->type) selected @endif
                value="{{ $type }}">{{ ucfirst($type) }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-right">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">بروزرسانی</button>
    </div>
    </form>
@endsection
@section('scripts')
    <script>
    $('#tag_types').select2(); </script>
@endsection
