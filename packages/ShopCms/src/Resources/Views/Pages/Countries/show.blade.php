@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Customers list</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <a href="{{ route('Admin.customers.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a>
        <h1 class="float-left">Customers list of {{ $country->name }} {!! $country->flag() !!}</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Company</th>
                <th scope="col">State</th>
                <th scope="col">City</th>
                <th scope="col">Created date</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($country->states as $state)
                @foreach ($state->customers as $customer)
                    <tr>
                        <th><a href="{{ route('Admin.customers.show',['id' => $customer->id]) }}">{{ $customer->first_name . ' ' . $customer->last_name }}</a></th>
                        <td>{{ $customer->company }}</td>
                        <td>{{ $state->name }}</td>
                        <td>{{ $customer->city }}</td>
                        <td>{{ $customer->created_at->diffforhumans() }}</td>
                        <td><a class="btn btn-outline-info" href="{{ route('Admin.customers.edit',['id' => $customer->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                        <td>
                            <form action="{{ route('Admin.customers.destroy',['id' => $customer->id]) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endforeach
            {{-- @foreach ($customers as $customer)
            <tr>
                <th scope="row">{{ $customer->first_name }} {{ $customer->last_name }}</th>
                <td>{{ $customer->company }}</td>
                <td>{{ $customer->state->country->name }}</td>
                <td>{{ $customer->state->name }}/{{ $customer->city }}</td>
                <td>{{ $customer->created_at->diffForHumans() }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.customers.edit',['id' => $customer->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.customers.destroy',['id' => $customer->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach --}}
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{-- {{ $tags->links() }} --}}
    </div>
@endsection
