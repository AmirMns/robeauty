@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Countries list</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <a href="{{ route('Admin.countries.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a>
        <h1 class="float-left">Countries</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col"><i class="fas fa-flag"></i></th>
                <th scope="col">Name</th>
                <th scope="col">Code</th>
                <th scope="col">States</th>
                <th scope="col">Customers</th>
                <th scope="col">Active</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($countries as $country)
            <tr>
                <th scope="row">{!! $country->flag() !!}</th>
                <th>{{ $country->name }}</th>
                <td>{{ $country->code }}</td>
                <td>
                    <a class="btn btn-warning" href="{{ route('Admin.states.index',
                    ['country' => strtolower($country->name)]) }}">
                    <i class="fas fa-eye">{{ $country->states->count() }}</i>
                    </a>
                </td>
                <td>
                    <a class="btn btn-warning" href="{{ route('Admin.countries.show',
                    ['id' => $country->id]) }}">
                        <i class="fas fa-users"></i>
                    </a>
                </td>
                <td>
                    <label style="margin-left:10px;" class="checkbox-c" for="active">
                        <input type="checkbox" disabled name="active" disabled id="active" @if ($country->active) checked @endif value="true">
                        <span class="check-handle"></span>
                    </label>
                </td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.countries.edit',['id' => $country->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.countries.destroy',['id' => $country->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $countries->links() }}
    </div>
@endsection
