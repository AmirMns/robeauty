@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Editing {{ $country->name }} Country</title>
@endsection
@section('pagecontent')
    <h1>Editing {{ $country->name }} Country</h1>
    <form action="{{ route('Admin.countries.update', ['id' => $country->id]) }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    @method('PATCH')
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="name" id="name"
        value="{{ (old('name')) ? old('name') : $country->name }}" placeholder="Country name">
    </div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hashtag"></i></span>
        </div>
        <input required type="text" class="form-control" name="code" id="code"
        value="{{ (old('code')) ? old('code') : $country->code }}" placeholder="Country name">
    </div>
    <div class="input-group col-12 float-left mb-4">
        <h4>Active : </h4>
        <label style="margin-left:10px;" class="checkbox-c" for="active">
            <input type="checkbox" name="active" id="active" value="true" @if ($country->active) checked @endif>
            <span class="check-handle"></span>
        </label>
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-left">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">Update</button>
    </div>
    </form>
@endsection
