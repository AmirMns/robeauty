@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>گالری ها</title>
@endsection
@section('pagecontent')
<div class="col-12 p-3 float-left">
    @foreach ($galleries as $gallery)
        <a href="{{ route('Admin.galleries.show', ['gallery' => $gallery->id]) }}" class="btn btn-outline-secondary mt-3 ml-3 float-right">
            {{ $gallery->title }}
        </a>
    @endforeach
</div>
@endsection