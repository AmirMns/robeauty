@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>ویرایش گالری #{{ $gallery->id }}</title>
@endsection
@section('pagecontent')
<div class="col-12 p-0 float-right" data-csrf="{{ csrf_token() }}" selectmulti="true"
data-mainpage="{{ route('home') }}" data-target="{{ route('addphoto', ['id' => $gallery->id , 'method' => 'galleryitem']) }}"
data-api-token="{{ auth()->user()->api_token }}" id="react-collapsable-gallry"></div>

<div class="col-12 float-left p-3">
    <div class="col-12 float-left p-3">
        @foreach ($gallery->items as $item)
            <div class="gallery-item bg-secondary col-md-2 col-6 p-1 float-left">
                <img class="col-12 h-auto" src="{{ asset($item->file->path) }}" alt="">
                <form action="{{ route('Admin.galleries.updateItem', ['gallery' => $gallery->id, 'item' => $item->id]) }}" class="mt-3" method="post">
                @csrf
                @method('PUT')
                <input type="text" name="title" placeholder="عنوان" class="form-control" id="title-{{ $item->id }}" value="{{ $item->title }}">
                <input type="text" name="url" placeholder="لینک" class="form-control mt-3" id="url-{{ $item->id }}" value="{{ $item->url }}">
                <textarea style="resize: none;" class="form-control mt-3" name="description" id="description-{{ $item->id }}" maxlength="191" cols="30" rows="2">{{ old('description') ?: $item->description }}</textarea>
                <button type="submit" class="btn btn-primary btn-sm">آپدیت</button>
                </form>
                <form class="mt-2 col-12 text-center" action="{{ route('Admin.galleries.destroyItem', ['gallery' => $gallery->id, 'item' => $item->id]) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-times"></i></button>
                </form>
            </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
    <script>
    $('#product_tags').select2();
    var swiper = new Swiper.default('.swiper-container', {
    effect: 'coverflow',
    autoplay: {
        delay: 2000,
    },
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    loop: false,
    coverflowEffect: {
        rotate: 0,
        stretch: 0,
        depth: 200,
        modifier: 1,
        slideShadows : true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        // when window width is <= 320px
        320: {
        slidesPerView: 1,
        spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
        slidesPerView: 2,
        spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
        slidesPerView: 3,
        spaceBetween: 30
        }
    },
    pagination: {
        el: '.swiper-pagination',
    }, });</script>
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ asset('js/CollapsableGallery.js') }}"></script>
    <script src="{{ asset('js/ck-editor.js') }}"></script>
@endsection