@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>سفارش شماره {{ $order->id + 10000 }}</title>
@endsection
@php
    $totalWithTax = ($order->total / 100) * 9;
@endphp
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <button id="printOrder" class="btn btn-outline-primary hideforprint float-left mt-2 mr-2">
            <i class="fas fa-print"></i>
        </button>
        <div class="col-12 float-right">
            <img src="{{ asset('images/logo2.png') }}" class="float-right" alt=""><p class="col-12 text-right float-right">فروشگاه اینترنتی روبیوتی</p>
        </div>
        <br><br><br>
        <h1 class="float-right">سفارش شماره {{ $order->id + 10000 }}</h1>
        <br>
        <p class="col-12 text-right float-right">تاریخ ثبت سفارش : <span class="time-jalali" data-date="{{ $order->created_at }}"></span></p>
    </div>
    <div class="col-12 float-left hideforprint" data-public-url="{{ route('home') }}" data-order-id="{{ $order->id }}" id="searchform"></div>
    <div class="col-12 mt-4 float-left">
        @if ($order->invoice->payment->paid)
            <div class="col-md-6 col-12 alert alert-primary text-center" style="max-width:50%;"><p class="m-0">پرداخت شده</p></div>
        @endif
    </div>
    <table class="table table-bordered mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">نام محصول</th>
                <th scope="col">قیمت واحد</th>
                <th scope="col">تعداد</th>
                <th scope="col">نرخ کل</th>
                <th scope="col">تخفیف</th>
                <th scope="col">مجموع</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($order->items as $item)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $item->product->title }}</td>
                <td>{{ $item->product->price }}</td>
                <td>{{ $item->quantity }}</td>
                <td>{{ $item->quantity * $item->product->price }}</td>
                <td>{{ number_format($item->offer_amount) }}</td>
                <td>{{ number_format(($item->quantity * $item->product->price) - $item->offer_amount) }}</td>
            </tr>
            @endforeach
            @php
                $post_price = 0;
            @endphp
            @if ($order->invoice && $order->invoice->shipping_method_id)
                @php
                    $shipping_method = \App\Method::whereId($order->invoice->shipping_method_id)->first();
                    $post_price = $shipping_method->price;
                @endphp
                <tr id='shipping-pricing'>
                    <td></td>
                    <td>هزینه ارسال با {{ $shipping_method->title }}</td>
                    <td>---</td>
                    <td>---</td>
                    <td>{{ number_format($post_price) }}</td>
                    <td class="hideforprint">---</td>
                    <td class="hideforprint">{{ number_format($post_price) }}</td>
                </tr>
            @endif
            <div id="react-infos"></div>
        </tbody>
    </table>
    <div class="col-12 mb-4 float-left">
        <p class="text-right">این فاکتور همچنین شامل 9% مالیات است که برابر {{ number_format($totalWithTax) }} تومان می باشد</p>
        <h4>مبلغ کل پرداختی : {{ number_format($order->invoice->payment->amount) }} تومان</h4>
    </div>
    <div style="width: 50%;" class="col-md-6 col-12 text-right float-right mb-4">
        <b><p>مشخصات مشتری {{ ! $order->invoice->recipient ? 'و دریافت کننده' : '' }}: </p></b>
        <p><b>نام مشتری : </b>{{ $order->customer->first_name . " " . $order->customer->last_name }}</p>
        <p><b>شماره تلفن : </b>{{ $order->customer->phone_number }}</p>
        <p><b>کد پستی : </b>{{ $order->customer->postal_code }}</p>
        <p>{{ $order->customer->company ? 'شرکت : ' . $order->customer->company : '' }}</p>
        <p>{!!  $order->customer->city->state->name . ', ' . $order->customer->city->name . "<br>"  . $order->customer->address1 .  ', ' . $order->customer->postal_code . '.' !!}</p>
    </div>
    <div style="width: 50%;" class="col-md-6 col-12 text-right float-right mb-4">
        @if ($order->invoice->recipient)
            @php $recipient = $order->invoice->recipient; @endphp
            <b><p>مشخصات دریافت کننده : </p></b>
            <p><b>نام مشتری : </b>{{ $recipient->fullname }}</p>
            <p><b>شماره تلفن : </b>{{ $recipient->phone_number }}</p>
            <p>{{ $recipient->company ? 'شرکت : ' . $recipient->company : '' }}</p>
            <p>کد پستی : {{ $recipient->postal_code }}</p>
            <p>{!!  $recipient->city->state->name . ', ' . $recipient->city->name . "<br>"  . $recipient->address .  '.' !!}</p>
        @endif
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/SearchProduct.js') }}"></script>
@endsection
