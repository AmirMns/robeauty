@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Editing Order Number #{{ $order->number }}</title>
@endsection
@section('pagecontent')
    <h1>Editing Order Number #{{ $order->number }}</h1>
    <form action="{{ route('Admin.customer-orders.update', ['id' => $order->id]) }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    @method('PATCH')
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-clock"></i></span>
        </div>
        <input required type="text" class="form-control" name="status" id="status"
        value="{{ (old('status')) ? old('status') : $order->status }}" placeholder="Order Status">
    </div>
    <div class="input-group col-12 float-left mb-4">
        <h4>Finished : </h4>
        <label style="margin-left:10px;" class="checkbox-c" for="finished">
            <input type="checkbox" name="finished" id="finished" value="true" @if ($order->finished) checked @endif>
            <span class="check-handle"></span>
        </label>
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-left">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">Update</button>
    </div>
    </form>
@endsection
