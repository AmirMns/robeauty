@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Orders list</title>
@endsection
@php
    $orderby = request()->order == 'desc' ? 'asc' : 'desc';
@endphp
@section('pagecontent')
    <div class="col-12 float-left p-3">
        {{-- <a href="{{ route('Admin.customer-orders.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a> --}}
        <h1 class="float-left">سفارشات</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">نام مشتری</th>
                <th scope="col">تلفن</th>
                <th scope="col">مجموع فاکتور</th>
                <th scope="col">وضعیت</th>
                <th scope="col"><a class="text-light" href="{{ route('Admin.orders.index', array_merge( ['sort_by' => 'created_at'], request()->toArray(), ['order' => $orderby])) }}">تاریخ ایجاد</a></th>
                <th scope="col">اتمام سفارش</th>
                <th scope="col">مشاهده</th>
                <th scope="col">اعلام اتمام</th>
                {{-- <th scope="col">ویرایش</th> --}}
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr @if($order->invoice && (! $order->invoice->payment->paid)) style="background-color:#ffaaaa !important;" @endif>
                <th>#{{ $order->id + 10000 }} <br> کد پرداختی : {{ $order->invoice ? $order->invoice->payment->id + 10000 : '' }}</th>
                <td>{{ $order->customer->first_name . ' ' . $order->customer->last_name }}</td>
                <td>{{ $order->customer->phone_number }}</td>
                <td>{{ $order->total }}</td>
                <td>{{ $order->status_notes }} <br> {{ $order->invoice ? $order->invoice->payment->RefID : '' }}</td>
                <td>{{ $order->created_at->diffForHumans() }}</td>
                <td>{!! $order->finished ? '<i style="color:green" class="fas fa-check"></i>' : '<i style="color:red" class="fas fa-times"></i>'!!}</td>
                <td>
                    @if ($order->invoice)
                        <a class="btn btn-warning" href="{{ route('Admin.orders.show', ['order' => $order->id ]) }}">
                            <i class="fas fa-eye"></i>
                        </a>
                    @endif
                </td>
                <td>
                    @if(! $order->finished)
                    <form action="{{ route('Admin.orders.update',['order' => $order->id]) }}" method="post">
                    @method('PUT')
                    @csrf
                    <button type="submit" class="btn btn-outline-dark"><i class="fas fa-check"></i></button>
                    </form>
                    @endif
                </td>
                {{-- <td></td> --}}
                <td>
                    <form action="{{ route('Admin.orders.destroy',['order' => $order->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $orders->links() }}
    </div>
@endsection
