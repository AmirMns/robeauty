@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Trashed Orders list</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <h1 class="float-left">Trashed Customers orders</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Total</th>
                <th scope="col">Status</th>
                <th scope="col">Created Date</th>
                <th scope="col">Deleted Date</th>
                <th scope="col">Finished</th>
                <th scope="col">Return</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr>
                <th>#{{ $order->number }}</th>
                <td>{{ $order->user->customer->first_name . ' ' . $order->user->customer->last_name }}</td>
                <td><a href="mailto:{{ $order->user->email }}">{{ $order->user->email }}</a></td>
                <td>{{ format_price($order->total) }}</td>
                <td>{{ $order->status }}</td>
                <td>{{ $order->created_at->diffForHumans() }}</td>
                <td>{{ $order->deleted_at->diffForHumans() }}</td>
                <td>
                    <label style="margin-left:10px;" class="checkbox-c" for="active">
                        <input type="checkbox" disabled name="active" disabled id="active" @if ($order->finished) checked @endif value="true">
                        <span class="check-handle"></span>
                    </label>
                </td>
                <td>
                    <form action="{{ route('Admin.returnorders',['id' => $order->id]) }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-outline-info"><i class="fas fa-recycle"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $orders->links() }}
    </div>
@endsection
