@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Attributes list</title>
@endsection
@section('pagecontent')

    <div class="col-12 float-left p-3">
        <a href="{{ route('Admin.attributes.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a>
        <h1 class="float-left">Attributes</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Active</th>
                <th scope="col">Unit</th>
                <th scope="col">Created date</th>
                <th scope="col">Updated date</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($attributes as $attr)
            <tr>
                <th scope="row">{{ $attr->name }}</th>
                <td>@if ($attr->description) {{ $attr->description }} @else --- @endif</td>
                <td>
                    <label style="margin-left:10px;" class="checkbox-c" for="active">
                        <input type="checkbox" name="active" disabled id="active" @if ($attr->active) checked @endif value="true">
                        <span class="check-handle"></span>
                    </label>
                </td>
                <td>@if ($attr->unit) {{ $attr->unit }} @else --- @endif</td>
                <td>{{ $attr->created_at->diffForHumans() }}</td>
                <td>{{ $attr->updated_at->diffForHumans() }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.attributes.edit',['attr' => $attr->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.attributes.destroy',['id' => $attr->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $attributes->links() }}
    </div>
@endsection
