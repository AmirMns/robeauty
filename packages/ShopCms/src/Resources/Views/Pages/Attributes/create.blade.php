@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Add new attribute</title>
@endsection
@section('pagecontent')
    <h1>Add new attribute</h1>
    <form action="{{ route('Admin.attributes.store') }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="name" id="name"
        value="{{ old('name') }}" placeholder="Attribute name">
    </div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-balance-scale"></i></span>
        </div>
        <input type="text" class="form-control" name="unit" id="unit"
        value="{{ old('unit') }}" placeholder="Attribute unit">
    </div>
    <div class="input-group col-12 col-lg-8 float-left mb-4">
        <textarea name="description" id="description" class="form-control" cols="30" rows="10"
        placeholder="Attribute description" style="resize:none;" maxlength="150">{{ old('description') }}</textarea>
    </div>
    <div class="input-group col-12 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas">Data Accepts</i></span>
        </div>
        <select id="attr_accepts" name="accepts" title="accepts" class="form-control col-6">
            @foreach ($types as $type)
                <option value="{{ $type }}">{{ ucfirst($type) }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 float-left mb-4">
        <h4>Active : </h4>
        <label style="margin-left:10px;" class="checkbox-c" for="active">
            <input type="checkbox" name="active" id="active" value="true">
            <span class="check-handle"></span>
        </label>
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-left">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">Create</button>
    </div>
    </form>
@endsection
@section('scripts')
    <script>
    $('#attr_accepts').select2(); </script>
@endsection
