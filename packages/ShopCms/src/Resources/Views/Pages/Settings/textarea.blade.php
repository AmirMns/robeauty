
<div class="input-group col-md-8 offset-md-2 col-12 float-left mb-4">
    <h4 class="col-12 text-center rtl">
        <span class="mb-4 text-left badge badge-secondary">{{ $ph }}</span>
    </h4>    
    <textarea required spellcheck="false" placeholder="{{ $ph }}" name="value" class="text-left text-light bg-dark form-control" 
    style="direction: ltr; font-family: sans-serif;" cols="30" rows="10">{{ $slot }}</textarea>
    <div class="col-12 text-center mt-3">
        <button type="submit" class="btn btn-primary">ذخیره</button>
    </div>
</div>