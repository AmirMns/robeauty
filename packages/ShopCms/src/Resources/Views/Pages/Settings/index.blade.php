@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>لیست تنظیمات</title>
@endsection
@section('pagecontent')
<div class="col-12 float-right p-3">
    {{-- <a href="{{ route('Admin.tags.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a> --}}
    <h1 class="mb-4 col-12 text-right float-right">تنظیمات</h1>
    @foreach ($groups as $group)
        @if ($group->group == $selected_group)
        <a href="{{ route('Admin.settings.index', ['group' => $group->group]) }}" class="float-right ml-2 btn btn-secondary" disabled>
            {{ $group->group }}
        </a>
        @else
        <a href="{{ route('Admin.settings.index', ['group' => $group->group]) }}" class="float-right ml-2 btn btn-outline-secondary">
            {{ $group->group }}
        </a>
        @endif
    @endforeach
</div>
<div class="col-12 float-left">
    @foreach ($settings as $setting)
        <form action="{{ route('Admin.settings.update', ['setting' => $setting->id]) }}" method="post" class="form-group col-12">
        @csrf
        @method('PUT')
        @if ($setting->type == 'string')
            @component('SHOPCMSVIEW::Pages.Settings.text', ['ph' => $setting->display_name])
            {{ $setting->value }}
            @endcomponent
        @elseif($setting->type == 'html')
            @component('SHOPCMSVIEW::Pages.Settings.textarea', ['ph' => $setting->display_name])
            {{ $setting->value }}
            @endcomponent
        @endif
        </form>
    @endforeach
</div>
{{-- <div class="col-12 float-left text-center">
    {{ $tags->links() }}
</div> --}}
@endsection