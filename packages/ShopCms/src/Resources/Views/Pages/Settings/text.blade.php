<div class="input-group col-md-8 offset-md-2 col-12 float-left mb-4">
    <div class="input-group-prepend">
        <button type="submit" class="btn btn-primary">ذخیره</button>
    </div>
    <input spellcheck="false" required type="text" class="form-control text-left" style="direction:ltr; font-family: sans-serif !important;" name="value" value="{{ urldecode($slot) }}" placeholder="{{ $ph }}">
    <div class="input-group-append">
        <span class="input-group-text" style="font-family: BYekan !important;">{{ $ph }}</span>
        {{-- <span class="input-group-text">{{ $ph }}</span> --}}
    </div>
</div>