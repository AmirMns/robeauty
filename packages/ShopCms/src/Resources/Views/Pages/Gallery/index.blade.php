@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Gallery</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3 mt-4">
        <h1 class="float-left">گالری</h1>
        <div class="col-12 p-4 float-left">
            <form id="addPhotosForm" action="{{ route('Admin.file.store') }}" class="dropzone col-12" method="POST">
            @csrf
            </form>
        </div>
    </div>
    <div class="col-12 float-left p-0">
        <div id="react-gallery" data-delete="{{ route('Admin.file.index') }}"
        data-csrf={{ csrf_token() }} data-api-token="{{ auth()->user()->api_token }}" data-mainpage="{{ route('home') }}">
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ asset('js/Gallery.js') }}"></script>
    <script> $(".btn-warning, .btn-danger").hide(); </script>
@endsection
