@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Creating new Customer</title>
@endsection
@section('pagecontent')
    <h1>Creating new Customer</h1>
    <form action="{{ route('Admin.customers.store') }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input type="text" class="form-control" name="first_name" id="first_name"
        value="{{ old('first_name') }}" placeholder="First name">
    </div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="last_name" id="last_name"
        value="{{ old('last_name') }}" placeholder="Last name">
    </div>
    <div class="input-group col-12 col-sm-7 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user"></i></span>
        </div>
        <select id="user_types" name="user" title="user" class="form-control col-12">
            @foreach ($users as $user)
                @if ($user->customer == null)
                    <option value="{{ $user->id }}">{{ ucfirst($user->name) }} - {{ $user->email }}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-sm-5 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-passport"></i></span>
        </div>
        <input required type="text" class="form-control" name="VAT" id="VAT"
        value="{{ old('VAT') }}" placeholder="VAT No.">
    </div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-building"></i></span>
        </div>
        <input required type="text" class="form-control" name="company" id="company"
        value="{{ old('company') }}" placeholder="Company">
    </div>
    <div class="input-group col-8 float-left mb-4">
        <textarea style="resize:none;" placeholder="Notes" class="form-control" name="notes" id="notes"
        cols="30" rows="5">{{ old('notes') }}</textarea>
    </div>
    <div class="col-12 float-left mb-3">
        <h3>Location Info :</h3>
    </div>
    <div data-firstpage="{{ route('home') }}" id="react-countries"></div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-city"></i></span>
        </div>
        <input required type="text" class="form-control" name="city" id="city"
        value="{{ old('city') }}" placeholder="City">
    </div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
        </div>
        <input required type="text" class="form-control" name="postal_code" id="postal_code"
        value="{{ old('postal_code') }}" placeholder="Postal Code/Zip">
    </div>
    <div class="input-group col-8 float-left mb-4">
        <textarea style="resize:none;" placeholder="Address" class="form-control" name="address" id="address"
        cols="30" rows="7">{{ old('address') }}</textarea>
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-left">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">Create</button>
    </div>
    </form>
@endsection
@section('scripts')
    <script>
    function formatCountry (country) {
        if (!country.id) {
            return country.text;
        }
        var baseUrl =  "{{ route('home') }}/images/world-icons";
        var $country = $(
            '<span><img width="25" height="25" src="' + baseUrl + '/' + country.element.innerText.toLowerCase() + '.svg" class="img-flag img-flag-small" /> ' + country.text + '</span>'
        );
        return $country;
    };
    $(document).ready(function(){
        $('#user_types').select2();
        // $('.countries-list').select2({
        //     templateResult: formatCountry
        // });
    }); </script>
    <script src="{{ asset('js/Country.js') }}"></script>
@endsection

