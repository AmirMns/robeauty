@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>لیست مشتریان</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        {{-- <a href="{{ route('Admin.customers.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a> --}}
        <h1 class="float-left">مشتریان</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">نام و نام خانوادگی</th>
                <th scope="col">شرکت</th>
                <th scope="col">شهر</th>
                <th scope="col">آدرس</th>
                <th scope="col">تاریخ ثبت اطلاعات</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customers as $customer)
            <tr>
                <th scope="row">{{ $customer->id }}</th>
                <td>{{ $customer->first_name }} {{ $customer->last_name }}</td>
                <td>{{ $customer->company }}</td>
                <td>{{ $customer->city->state->name }}/{{ $customer->city->name }}</td>
                <td>{{ $customer->address1 }} <br><br> {{ $customer->address2 }}</td>
                <td>{{ $customer->created_at->diffForHumans() }}</td>
                <td>
                    <form action="{{ route('Admin.customers.destroy',['customer' => $customer->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{-- {{ $tags->links() }} --}}
    </div>
@endsection
