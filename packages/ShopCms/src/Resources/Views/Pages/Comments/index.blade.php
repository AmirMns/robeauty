@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Comments list</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        {{-- <a href="{{ route('Admin.comments.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a> --}}
        <h1 class="float-left">comments</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Body</th>
                <th scope="col">Active</th>
                <th scope="col">Date</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($comments as $comment)
            <tr>
                <th>{{ $comment->user->name }}</th>
                <td>{{ $comment->user->email }}</td>
                <td>{{ $comment->body }}</td>
                <td>
                    <label style="margin-left:10px;" class="checkbox-c" for="active">
                        <input type="checkbox" disabled name="active" disabled id="active" @if ($comment->active) checked @endif value="true">
                        <span class="check-handle"></span>
                    </label>
                </td>
                <td>{{ $comment->created_at->diffForHumans() }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.comments.edit',['id' => $comment->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.comments.destroy',['id' => $comment->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $comments->links() }}
    </div>
@endsection
