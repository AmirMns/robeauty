@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>فهرست کاربران</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        {{-- <a href="{{ route('Admin.users.create') }}" class="btn btn-info float-left mt-2 mr-2">+</a> --}}
        <h1 class="float-left">فهرست کاربران</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">نام</th>
                <th scope="col">ایمیل</th>
                <th scope="col">نقش</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <th>{{ $user->name }}</th>
                <td>{{ $user->email }}</td>
                <td>
                    @foreach ($user->roles as $role)
                        {{ $role->label }}
                    @endforeach
                </td>
                {{-- <td>
                    <label style="margin-left:10px;" class="checkbox-c" for="active">
                        <input type="checkbox" disabled name="active" disabled id="active" @if ($user->active) checked @endif value="true">
                        <span class="check-handle"></span>
                    </label>
                </td>
                <td>
                    <a class="btn btn-warning" href="{{ route('Admin.users.show',['country' => strtolower($country->name),'id' => $user->id]) }}">
                        <i class="fas fa-users"></i>
                    </a>
                </td> --}}
                <td><a class="btn btn-outline-info" href="{{ route('Admin.users.edit',['user' => $user->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.users.destroy',['user' => $user->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 float-left text-center">
        {{ $users->links() }}
    </div>
@endsection
