@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Creating new User</title>
@endsection
@section('pagecontent')
    <h1>Creating new User</h1>
    <form action="{{ route('Admin.users.store') }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="name" id="name"
        value="{{ old('name') }}" placeholder="Username">
    </div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-at"></i></span>
        </div>
        <input required type="text" class="form-control" name="email" id="email"
        value="{{ old('email') }}" placeholder="Country name">
    </div>
    <div class="input-group col-12 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-user-tag">Role</i></span>
        </div>
        <select id="role_types" name="role" title="role" class="form-control col-6">
            @foreach ($roles as $role)
                <option value="{{ $role->id }}">{{ ucfirst($role->label) }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-left">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">Create</button>
    </div>
    </form>
@endsection
@section('scripts')
    <script>
    $('#role_types').select2(); </script>
@endsection

