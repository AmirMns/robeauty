@extends('SHOPCMSVIEW::default')
@section('content')
    <div class="col-12 admin-area p-0 m-0 h-12">
        <div class="col-lg-2 col-md-3 d-md-block d-none h-12 p-0" id="left-menu">
            <div class="top-section text-center p-3">
                <p class="col-12 mt-3 p-0 text-right float-left"><i class="fas fa-pencil-alt"></i> پنل فروشگاه</p>
            </div>
            @include('SHOPCMSVIEW::Utils.menu')
        </div>
        <div class="col-lg-10 col-md-9 col-12 h-12"
        id="mainpage">
            <div class="col-12 navbar-top">
                <button class="collapser btn btn-secondary">
                    <i class="fas fa-angle-double-right"></i>
                </button>
                <button data-event="max" class="btn scr-controller">
                    <i class="fas fa-expand-arrows-alt fa-compress"></i>
                </button>
                <button class="btn btn-light collapser d-sm-none d-block ml-4 float-right">
                    <i class="fas fa-bars"></i>
                </button>
            </div>
            <div class="col-12 float-left" id="orderforprint">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @yield('pagecontent')
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
    var elem = document.documentElement;
    /* View in fullscreen */
    function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
    }
    /* Close fullscreen */
    function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
    }
    }
    $('.scr-controller').click(function(){
        $(' > i',this).toggleClass('fa-expand-arrows-alt');
        if ($(this).attr('data-event') == 'max') {
            $(this).attr('data-event','min');
            return openFullscreen();
        }
        $(this).attr('data-event','max');
        return closeFullscreen();
    });
    $('.collapser').click(function(){
        $(' > i',this).toggleClass('fa-angle-double-left fa-angle-double-right');
        $("#left-menu").toggleClass('collapsed active-mobile d-none');
        $("#mainpage").toggleClass('col-lg-10 col-md-9')
    });
    $('nav li').click(function() {
        $(' i.opener',this).toggleClass('opened');
        $(this).toggleClass('active');
    });
    function printdiv(printdivname)
    {
        var footstr = "</body>";
        var newstr = document.getElementById(printdivname).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
    $(document).on('click','#printOrder', function(){
        $('.hideforprint').hide();
        printdiv('orderforprint');
        $('.hideforprint').show();
    }); </script>
    @yield('scripts')
@endsection
