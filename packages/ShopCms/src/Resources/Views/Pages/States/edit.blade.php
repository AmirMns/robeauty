@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Editing {{ $state->name }} State of {{ $country->name }}</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3 float-left">
        <a class="btn btn-outline-secondary mt-2 float-left" href="{{ route('Admin.states.index', [
        'country' => strtolower($country->name)
        ]) }}"><i class="fas fa-angle-left"></i></a>
        <h1 class="col-8 float-left">Editing {{ $state->name }} State of {{ $country->name }}</h1>
    </div>
    <form action="{{ route('Admin.states.update',['country' => $country->id, 'state' => $state->id ]) }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0 mt-3 float-left">
    @csrf
    @method('PATCH')
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="name" id="name"
        value="{{ (old('name')) ? old('name') : $state->name }}" placeholder="State name">
    </div>
    <div class="input-group col-12 col-sm-6 float-left mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-euro-sign"></i></span>
        </div>
        <input required type="text" class="form-control" name="tax" id="tax"
        value="{{ (old('tax')) ? old('tax') : $state->cent($state->tax,false) }}" placeholder="Tax EUR">
        <input type="text" disabled class="form-control col-1 disabled" value=".">
        <input type="number" name="tax_cent" class="form-control col-2" max="99" min="0"
        value="{{ (old('tax_cent')) ? old('tax_cent') : $state->cent($state->tax) }}">
    </div>
    <div class="input-group col-12 float-left mb-4">
        <h4>Active : </h4>
        <label style="margin-left:10px;" class="checkbox-c" for="active">
            <input type="checkbox" name="active" id="active" value="true" checked>
            <span class="check-handle"></span>
        </label>
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-left">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">Update</button>
    </div>
    </form>
@endsection
