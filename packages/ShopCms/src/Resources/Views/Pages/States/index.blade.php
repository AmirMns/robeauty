@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>Countries list</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <a class="btn btn-info float-left mt-2 mr-2" href="{{ route('Admin.countries.index') }}"><i class="fas fa-angle-left"></i></a>
        <a href="{{ route('Admin.states.create', ['country' => strtolower($country->name) ]) }}" class="btn btn-info float-left mt-2 mr-2">+</a>
        <h1 class="float-left">States of {{ $country->name }} {!! $country->flag() !!}</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Tax rate</th>
                <th scope="col">Active</th>
                <th scope="col">Customers</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($states as $state)
            <tr>
                <th>{{ $state->name }}</th>
                <td>{{ $state->tax }}</td>
                <td>
                    <label style="margin-left:10px;" class="checkbox-c" for="active">
                        <input type="checkbox" disabled name="active" disabled id="active" @if ($state->active) checked @endif value="true">
                        <span class="check-handle"></span>
                    </label>
                </td>
                <td>
                    <a class="btn btn-warning" href="{{ route('Admin.states.show',['country' => strtolower($country->name),'id' => $state->id]) }}">
                        <i class="fas fa-users"></i>
                    </a>
                </td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.states.edit',['country' => strtolower($country->name),'id' => $state->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.states.destroy',['country' => strtolower($country->name),'id' => $state->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $states->links() }}
    </div>
@endsection
