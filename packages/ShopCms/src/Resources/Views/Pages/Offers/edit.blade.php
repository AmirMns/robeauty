@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>ویرایش تخفیف {{ $offer->code }}</title>
    <link rel="stylesheet" href="{{ asset('css/persian-datepicker.min.css') }}">
    <style>
        .datepicker-plot-area {
            font-family: Iransans;
        }
    </style>
@endsection
@section('pagecontent')
    <h1 class="col-12">ویرایش تخفیف {{ $offer->code }}</h1>
    <form action="{{ route('Admin.offers.store') }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    @method('PATCH')
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="code" id="code"
        value="{{ old('code') ?: $offer->code }}" placeholder="کد تخفیف">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-tag"></i></span>
        </div>
        <input required type="text" class="form-control" name="amount" id="amount"
        value="{{ old('amount') ?: $offer->amount }}" placeholder="میزان تخفیف">
        <select id="offer_types" name="type" title="type" class="form-control col-6">
            @foreach ($offerTypes as $en => $fa)
                <option value="{{ $en }}">{{ $fa }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text">محدودیت استفاده</span>
        </div>
        <input required type="number" min="1" value="1" class="form-control" name="max_use" id="max_use" value="{{ old('max_use') ?: $offer->max_use }}">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text">محدودیت مجموع سفارش</span>
        </div>
        <input required type="number" min="1000" value="1000" step="500" class="form-control" name="max_total" id="max_total" value="{{ old('max_total') ?: $offer->max_total }}">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-calendar"></i></span>
        </div>
        <input required class="form-control" name="expired_at" id="expired_at" data-expired_at="{{ $offer->expired_at }}">
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-right">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">ایجاد</button>
    </div>
    </form>
@endsection
@section('scripts')
<script src="{{ asset('js/datepicker.js') }}"></script>
@endsection
