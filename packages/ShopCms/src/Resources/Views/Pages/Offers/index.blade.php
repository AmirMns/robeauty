@extends('SHOPCMSVIEW::Pages.layout')
@section('title')
    <title>لیست تخفیف ها</title>
@endsection

@section('pagecontent')
    <div class="col-12 float-left p-3">
        <a href="{{ route('Admin.offers.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right">تخفیف ها</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">کد تخفیف</th>
                <th scope="col">میزان تخفیف</th>
                <th scope="col">تعداد استفاده</th>
                <th scope="col">تاریخ انقضا</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($offers as $offer)
                @if($offer->checkToDestroy())
                <tr>
                    <th>{{ $loop->index + 1 }}</th>
                    <td>{{ $offer->code }}</td>
                    <td>{{ $offer->amount . ' ' . $offerTypes[$offer->type] }}</td>
                    <td>{{ $offer->orders_count }}</td>
                    <td>{{ $offer->expired_at->diffForHumans() }}</td>
                    <td>
                        <a href="{{ route('Admin.offers.edit', ['offer' => $offer->id]) }}" class="btn btn-outline-info"><i class="fas fa-pencil-alt"></i></a>
                    </td>
                    <td>
                        <form action="{{ route('Admin.offers.destroy', ['offer' => $offer->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                        </form>
                    </td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $offers->links() }}
    </div>
@endsection