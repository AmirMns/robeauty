<nav>
    <div class="col-12 d-sm-none d-block float-left text-center">
        <button class="btn btn-light collapser">
            <i class="fas fa-bars"></i>
        </button>
    </div>
    <ul class="horizental-menu col-12">
        <li><a href="{{ route('Admin.') }}"><i class="fab fa-dashboard"></i> داشبورد</a></li>
        <li><p><i class="fas fa-shopping-cart"></i> فروشگاه <i class="opener fa fa-angle-left"></i></p>
            <ul>
                <li><a href="{{ route('Admin.products.index') }}">محصولات</a></li>
                <li><a href="{{ route('Admin.offers.index') }}">تخفیف ها</a></li>
                {{-- <li><a href="{{ route('Admin.attributes.index') }}">ویژگی ها</a></li> --}}
                <li><a href="{{ route('Admin.customers.index') }}">مشتریان</a></li>
            </ul>
        </li>
        <li><p><i class="fas fa-shopping-cart"></i> سفارشات <i class="opener fa fa-angle-left"></i></p>
            <ul>
                <li><a href="{{ route('Admin.orders.index') }}">همه سفارشات</a></li>
                <li><a href="{{ route('Admin.orders.index', ['where' => 'status', 'equals' => 'PAID']) }}">پرداخت شده</a></li>
                <li><a href="{{ route('Admin.orders.index', ['where' => 'finished', 'equals' => true]) }}">تکمیل شده</a></li>
                <li><a href="{{ route('Admin.orders.index', ['where' => 'finished', 'equals' => 0]) }}">در جریان</a></li>
            </ul>
        </li>
        <li><p><i class="fas fa-font"></i> محتوا <i class="opener fa fa-angle-left"></i></p>
            <ul>
                <li><a href="#">صفحات</a></li>
                <li><a href="#">مقالات</a></li>
            </ul>
        </li>
        <li><p><i class="fas fa-server"></i> اطلاعات <i class="opener fa fa-angle-left"></i></p>
            <ul>
                <li><a href="{{ route('Admin.categories.index') }}"><i class="fas fa-globe-europe"></i>دسته بندی ها</a></li>
                <li><a href="{{ route('Admin.tags.index') }}"><i class="fas fa-tags"></i> برچسب ها</a></li>
                <li><a href="{{ route('Admin.comments.index') }}"><i class="fas fa-comments"></i> نظرات</a></li>
            </ul>
        </li>
        <li><a href="{{ route('Admin.users.index') }}"><i class="fas fa-users"></i> کاربران</a></li>
        <li><a href="{{ route('Admin.galleries.index') }}"><i class="fas fa-images"></i> گالری ها</a></li>
        <li><a href="{{ route('Admin.settings.index') }}"><i class="fas fa-cog"></i> تنظیمات</a></li>
        <li><a href="{{ route('Admin.file.index') }}"><i class="fas fa-file"></i> فایل ها</a></li>
        <li><a href="{{ route('logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
        <i class="fas fa-power-off"></i> {{ __('خروج') }}</a></li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
        </form>
    </ul>
</nav>
