<?php

return [
    'developers' => [
        [
            'name' => 'Yoonus Tehraniam',
            'email' => 'yoonustehrani28@gmail.com',
            'tel' => 989150013422,
            'social_media' => [
                'instagram' => 'yoonustehrani',
                'telegram'  => 'yoonustehrani',
                'whatsapp'  => '989303868572',
            ]
        ]
    ],
    'app' => [
        'theme' => [
            'color' => 'default',
            'logo'  => ''
        ]
    ]
];
