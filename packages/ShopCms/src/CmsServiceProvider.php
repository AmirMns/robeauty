<?php

namespace ShopCms\Cms;

use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }
    public function register()
    {
        require __DIR__ . '/Http/routes.php';
        $this->loadViewsFrom( __DIR__ . '/Resources/Views', 'SHOPCMSVIEW');
        $this->mergeConfigFrom( __DIR__ . '/Config/main.php', 'cmsconfig');
        $this->app->singleton('shoppingcms', function($app){
            return new Cms;
        });
    }
}

