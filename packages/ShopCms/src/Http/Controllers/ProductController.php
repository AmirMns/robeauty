<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Tag;
use ShopCms\Cms\Http\Requests\ProductRequest;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(12);
        return view('SHOPCMSVIEW::Pages.Products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('SHOPCMSVIEW::Pages.Products.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        // dd($request);
        $product = new Product;
        $product->title = $request->title;
        $product->slug  = make_slug($request->title);
        $latestName = Product::whereRaw("`title` RLIKE '^{$product->slug}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('title');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $product->slug .= '-' . ($number + 1);
        }
        $product->short_description = $request->short_description;
        $product->description = ($request->description) ? $request->description : '';
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->off = $request->off;
        $product->code = strtoupper(\Illuminate\Support\Str::random(3)) . random_int(10000, 99999) . time();
        $product->weight = 0;
        $product->brand_id = 1;
        $product->save();
        $product->tags()->sync($request->tags);
        return redirect()->to(route('Admin.products.index'));
        // return redirect()->to(route('Admin.products.edit', ['product' => $product->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $customer = Customer::find($id)->firstOrFail()->load('state.country');
        // return $customer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = Tag::all();
        $product = Product::with('tags')->find($id);
        $product->load('photos.file');
        return view('SHOPCMSVIEW::Pages.Products.edit', compact('product', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->title = $request->title;
        $product->slug  = make_slug($request->title);
        $latestName = Product::whereRaw("`title` RLIKE '^{$product->slug}(-[0-9]*)?$' AND `id` != {$product->id}")
        ->orderBy('id')->pluck('title');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $product->slug .= '-' . ($number + 1);
        }
        $product->short_description = $request->short_description;
        $product->english_description = $request->english_description;
        $product->description = ($request->description) ? $request->description : '';
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->off = $request->off;
        $product->weight = 0;
        $product->brand_id = $request->brand;
        $product->save();
        $product->tags()->sync($request->tags);
        return redirect()->to(route('Admin.products.edit', ['product' => $product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::findOrfail($id)->delete();
        return back();
    }
}
