<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Info;

class OrderController extends BaseController
{
    /**
     * Display a list of Orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {    
        $orders = Order::with(["customer","invoice.payment"]);
        if ($request->where) {
            if ($request->equals == '0' || ! $request->equals) {
                $request->equals = false;
            } else if($request->equals == '1') {
                $request->equals = true;
            }
            $orders->where($request->where, $request->equals);
        }
        $sort = $request->order == 'desc' ? 'desc' : 'asc';
        if ($request->sort_by) {
            $orders->orderBy($request->sort_by, $sort);
        }
        $orders = $orders->paginate(20);
        return view('SHOPCMSVIEW::Pages.Orders.index', compact('orders'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($order)
    {
        $order = Order::whereId($order)->with(['items', 'customer.city.state', 'invoice.payment'])->firstOrFail();
        if (! $order->invoice->use_customer) {
            $order->invoice->load('recipient');
        }
        // return $order;
        return view('SHOPCMSVIEW::Pages.Orders.show', compact('order'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->finished = true;
        $order->note = "سفارش نهایی شده است";
        $order->save();
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return back();
    }
}
