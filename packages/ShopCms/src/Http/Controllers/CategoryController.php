<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Maincategory;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Maincategory::paginate(10);
        return view('SHOPCMSVIEW::Pages.Categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Maincategory::all();
        $types = ['product', 'page', 'post'];
        return view('SHOPCMSVIEW::Pages.Categories.create', compact('categories', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Maincategory;
        $category->name = strtolower(make_slug($request->name));
        $category->type = $request->type;
        $category->parent_id = ($request->main) ? $request->main : null;
        $category->note = '';
        $latestName = Maincategory::whereRaw("`type` = '{$category->type}' and `name` RLIKE '^{$category->name}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('name');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $category->name .= '-' . ($number + 1);
        }
        $category->save();
        return redirect()->to(route('Admin.categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(MainCategory $category)
    {
        $categories = Maincategory::all();
        $types = ['product', 'page', 'post'];
        return view('SHOPCMSVIEW::Pages.Categories.create', compact('categories', 'category', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MainCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainCategory $category)
    {
        $category->name = strtolower(make_slug($request->name));
        $category->type = $request->type;
        $latestName = Maincategory::whereRaw("`id` != '{$category->id}' and `name` RLIKE '^{$category->name}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('name');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $category->name .= '-' . ($number - 1);
        }
        $category->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {
        Maincategory::findOrFail($category)->delete();
        return back();
    }
}
