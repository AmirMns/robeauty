<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::paginate(12);
        return view("SHOPCMSVIEW::Pages.Cities.index", compact('cities'));
    }

    public function show($id)
    {
        $city = City::findOrFail($id)->load('states.customers');
        return view("SHOPCMSVIEW::Pages.Cities.show", compact('city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('SHOPCMSVIEW::Pages.Cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:cities|min:3|max:100',
            'code' => 'required|unique:cities|min:2|max:3'
        ]);
        $city = new City;
        $city->name = $request->name;
        $city->save();
        return redirect(route('Admin.cities.index'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        return view("SHOPCMSVIEW::Pages.Cities.edit", compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:3|max:100',
            'code' => 'required|min:2|max:3'
        ]);
        $city = City::findOrFail($id);
        $city->name = $request->name;
        $city->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::findOrfail($id)->delete();
        return back();
    }
}
