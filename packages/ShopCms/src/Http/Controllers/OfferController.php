<?php

namespace ShopCms\Cms\Http\Controllers;

use App\Offer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OfferController extends BaseController
{
    public function index()
    {
        $offers = Offer::withCount('orders')->latest()->paginate(20);
        $offerTypes = ['percent' => '%', 'price' => 'تومان'];
        return view('SHOPCMSVIEW::Pages.Offers.index', compact('offers', 'offerTypes'));
    }
    public function show(Offer $offer)
    {
        return view('SHOPCMSVIEW::Pages.Offers.show', compact('offer'));
    }
    public function create()
    {
        $offerTypes = ['percent' => '%', 'price' => 'تومان'];
        return view('SHOPCMSVIEW::Pages.Offers.create', compact('offerTypes'));
    }
    public function store(Request $request)
    {
        $dt = new Carbon(intval($request->gregorian_expired_at) / 1000);
        $offer = new Offer;
        $offer->code = $request->code;
        $offer->type = $request->type;
        $offer->amount = (int) $request->amount;
        $offer->max_use = (int) $request->max_use;
        $offer->max_total = (int) $request->max_total;
        $offer->expired_at = $dt->timezone('Asia/Tehran')->format('Y-m-d H:i:s');
        $offer->save();
        return redirect()->to(route('Admin.offers.index'));
    }
    public function edit(Offer $offer)
    {
        $offerTypes = ['percent' => '%', 'price' => 'تومان'];
        return view('SHOPCMSVIEW::Pages.Offers.edit', compact('offer', 'offerTypes'));
    }
    public function update(Request $request, Offer $offer)
    {
        $offer->code = $request->code;
        $offer->type = $request->type;
        $offer->amount = (int) $request->amount;
        $offer->max_use = (int) $request->max_use;
        $offer->max_total = (int) $request->max_total;
        $offer->expired_at = $request->expired_at;
        $offer->save();
    }
    public function destroy(Offer $offer)
    {
        $offer->delete();
    }
}