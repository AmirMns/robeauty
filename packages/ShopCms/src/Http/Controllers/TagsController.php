<?php

namespace ShopCms\Cms\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagsController extends BaseController
{
    public function index()
    {
        $tags = Tag::paginate(10);
        return view('SHOPCMSVIEW::Pages.Tags.index',compact('tags'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = ['product','page','post'];
		return view('SHOPCMSVIEW::Pages.Tags.create', compact('types'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = new Tag;
        $tag->name = strtolower(make_slug($request->name));
        $tag->type = $request->type;
        $latestName = Tag::whereRaw("`type` = '{$tag->type}' and `name` RLIKE '^{$tag->name}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('name');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $tag->name .= '-' . ($number + 1);
        }
        $tag->save();
        return redirect()->to(route('Admin.tags.index'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $types = ['product','page','post'];
        $tag->name = str_replace('-', ' ', $tag->name);
        return view('SHOPCMSVIEW::Pages.Tags.edit', compact('tag','types'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Tag $tag)
    {
        $tag->name = strtolower(make_slug($request->name));
        $tag->type = $request->type;
        $latestName = Tag::whereRaw("`id` != '{$tag->id}' and `name` RLIKE '^{$tag->name}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('name');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $tag->name .= '-' . ($number - 1);
        }
        $tag->save();
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tag)
    {
        Tag::findOrFail($tag)->delete();
        return back();
    }
}
