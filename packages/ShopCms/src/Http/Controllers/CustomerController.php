<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Role;
use App\User;
use App\State;

class CustomerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::paginate(12)->load('city.state');
        return view('SHOPCMSVIEW::Pages.Customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::with('customer')->get();
        return view('SHOPCMSVIEW::Pages.Customers.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
			'first_name'  => 'string|min:3|max:22',
			'last_name'   => 'required|string|min:3|max:22',
			'user'     => 'required|integer',
			'VAT'         => 'required|min:8|max:12',
			'Company'     => 'string|min:3|max:60',
			'state'    => 'required|integer',
            'city'        => 'required|string|min:2|max:65',
            'postal_code' => 'required|min:3|max:60',
            'address'     => 'string|min:3|max:400'
		]);
        $user  = User::with('roles')->find($request->user);
        $state = State::find($request->state);
        $customer = new Customer;
        $customer->first_name   = ($request->first_name) ? $request->first_name : '';
        $customer->last_name   = $request->last_name;
        $customer->user_id     = $user->id;
        $customer->VAT         = $request->VAT;
        $customer->company     = $request->company;
        $customer->state_id    = $state->id;
        $customer->city        = $request->city;
        $customer->postal_code = $request->postal_code;
        $customer->address     = $request->address;
        $customer->notes       = $request->notes;
        if ($state && $user->roles()->count() > 0) {
            if ($user->roles[0]->label == 'customer') {
                abort(403,"You are not allowed to continue !");
            }
        }
        $customer->save();
        return redirect()->to(route('Admin.customers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->load('state.country');
        return $customer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('SHOPCMSVIEW::Pages.Customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $state = State::findOrFail($request->state_id);
        $request->validate([
			'first_name'  => 'string|min:3|max:22',
			'last_name'   => 'required|string|min:3|max:22',
			'VAT'         => 'required|min:8|max:12',
			'Company'     => 'string|min:3|max:60',
			'state_id'    => 'required|integer',
            'city'        => 'required|string|min:2|max:65',
            'postal_code' => 'required|min:3|max:60',
            'address'     => 'string|min:3|max:400'
		]);
        $customer->first_name   = ($request->first_name) ? $request->first_name : '';
        $customer->last_name   = $request->last_name;
        $customer->VAT         = $request->VAT;
        $customer->company     = $request->company;
        $customer->state_id    = $state->id;
        $customer->city        = $request->city;
        $customer->postal_code = $request->postal_code;
        $customer->address     = $request->address;
        $customer->notes       = $request->notes;
        $customer->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::findOrfail($id)->delete();
        return back();
    }
}
