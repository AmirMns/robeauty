<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\File;

class FileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('SHOPCMSVIEW::Pages.Gallery.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photo = File::fromFile($request->file('photo'))->Upload('/products');
        $gallery = new File;
        $gallery->name = $photo->name;
        $gallery->path = $photo->path;
        $gallery->thumbnail_path = $photo->thumbnail_path;
        $gallery->type = 'photo';
        $gallery->save();
        return 'okay';
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gallery::findOrfail($id)->delete();
        return back();
    }
}
