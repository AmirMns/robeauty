<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\GalleryItem;

class GalleryController extends BaseController
{
    public function index()
    {
        $galleries = Gallery::all();
        return view('SHOPCMSVIEW::Pages.Galleries.index', compact('galleries'));
    }
    public function show($gallery)
    {
        $gallery = Gallery::whereId($gallery)->with('items.file')->firstOrFail();
        return view('SHOPCMSVIEW::Pages.Galleries.show', compact('gallery'));
    }
}