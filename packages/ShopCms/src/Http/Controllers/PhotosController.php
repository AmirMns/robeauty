<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChangeProductRequest;
use App\Product;
use App\Photo;

// use App\Http\Controllers\Traits\AuthorizesUsers;
// should be outcommand with trait way
class PhotosController extends BaseController
{
    // use AuthorizesUsers; should be outcommand with trait way
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Product $product,Request $request){
        if ($product) {
            $photo = Photo::fromFile($request->file('photo'))->Upload();
            return $product->addPhoto($photo);
        }
        return false;
    }
    public function destroy($id)
    {
        Photo::findOrfail($id)->delete();
        return back();
    }
}
