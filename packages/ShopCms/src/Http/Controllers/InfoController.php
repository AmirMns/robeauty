<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
// use App\Info;
use App\Product;
use App\Attribute;

class InfoController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Product $product,Request $request)
    {
        $request->validate([
            'info_value' => 'required|min:1',
            'attr'=> 'required|integer'
        ]);
        $attr = Attribute::find($request->attr);
        return $product;
        // if ($attr && $product) {
        //     $info = new Info;
		//     $info->value = $request->info_value;
        //     $info->product_id = $product->id;
        //     $info->attribute_id = $attr->id;
        //     $info->save();
        // }
        // return back();
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'name' => 'required|min:3|max:100',
        //     'code' => 'required|min:2|max:3'
        // ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Info::findOrfail($id)->delete();
        // return back();
    }
}
