<?php

namespace ShopCms\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Illuminate\Support\Facades\Cache;

class SettingController extends BaseController
{
    public function index(Request $request)
    {
        $groups = Setting::groupBy(['group'])->select('group')->orderBy('group', 'desc')->get();
        $selected_group = $request->group ?: $groups->toArray()[0]['group'];
        $settings = Setting::whereGroup($selected_group)->orderBy('order')->get();
        return view('SHOPCMSVIEW::Pages.Settings.index', compact('settings', 'groups', 'selected_group'));
    }
    public function show(Setting $setting)
    {
        return view('SHOPCMSVIEW::Pages.Settings.show', compact('setting'));
    }
    public function create()
    {
        $groups = Setting::groupBy(['group'])->select('group')->orderBy('group', 'desc')->get();
        return view('SHOPCMSVIEW::Pages.Offers.create', compact('groups'));
    }
    public function update(Request $request, Setting $setting)
    {
        $setting->value = $request->value;
        $setting->save();
        $cache_key = 'settings.' . $setting->key;
        if(Cache::has($cache_key)) {
            Cache::forget($cache_key);
        }
        Cache::forever($cache_key, $setting->value);
        return back();
    }
}