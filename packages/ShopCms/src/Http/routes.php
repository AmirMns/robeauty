<?php

use App\Gallery;
use App\GalleryItem;
use Illuminate\Http\Request;

Route::namespace('ShopCms\Cms\Http\Controllers')->middleware(['web','auth','roler'])->prefix('robeauty-shopcms')->name('Admin.')->group(function () {
    Route::get('/', ['uses' => 'AdminPanelController@index']);
    Route::resource('attributes', 'AttributeController')->except(['show']);
    Route::resource('buyer-orders', 'BuyerController');
    Route::resource('comments', 'CommentController')->except(['show']);
    // Route::resource('countries', 'CountryController');
    Route::resource('country/{country}/states', 'StateController');
    Route::resource('customers', 'CustomerController');
    Route::resource('customer-orders', 'OrderController');
    Route::resource('file', 'FileController')->except(['create','edit','update']);
    // Route::resource('orderinfos', 'OrderInfoController')->only(['store','update','destroy','edit']);
    Route::resource('pages', 'PageController');
    Route::resource('permissions', 'PermissionController')->except(['show']);
	Route::resource('posts', 'PostController');
    Route::resource('products', 'ProductController')->except(['show']);
    Route::resource('roles', 'RoleController')->except(['show']);
    Route::resource('tags', 'TagsController')->except(['show']);
    Route::resource('categories', 'CategoryController')->except(['show']);
    Route::resource('users', 'UsersController')->except(['show']);
    Route::resource('payments', 'PaymentController');
    Route::resource('orders', 'OrderController');
    Route::resource('offers', 'OfferController');
    Route::resource('settings', 'SettingController');
    Route::put('galleries/{gallery}/{item}', function (Request $request, Gallery $gallery, GalleryItem $item) {
        $item->title = $request->title;
        $item->description = $request->description;
        $item->url = $request->url;
        $item->save();
        return back();
    })->name('galleries.updateItem');
    Route::delete('galleries/{gallery}/{item}', function (Gallery $gallery, GalleryItem $item) {
        $item->delete();
        return back();
    })->name('galleries.destroyItem');
    Route::resource('galleries', 'GalleryController');
    
    Route::post('products/{product}/photos', [
        'as' => 'product_photos',
        'uses' => 'PhotosController@store'
    ]);


    
    Route::get('trash/customer-orders', 'TrashController@customer_orders')->name('customerstrash');
    Route::post('trash/orders/return/{id}', 'TrashController@return_orders')->name('returnorders');

    Route::post('products/{product}/infos/add', [
        'as' => 'infos.store',
        'uses' => 'InfoController@store'
    ]);
    Route::delete('infos/{id}', [
        'as' => 'infos.destroy',
        'uses' => 'InfoController@destroy'
    ]);

    Route::delete('/photos/{photo}/delete', function($photo) {
        $photo = \App\Photo::whereId($photo)->firstOrFail();
        $photo->delete();
        return back();
    })->name('products.photo.delete');
    // Route::get('config', ['as' => 'config','uses' => function () {
    //     return config('cms');
    // }]);
});
