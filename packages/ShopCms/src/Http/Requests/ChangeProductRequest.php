<?php

namespace ShopCms\Cms\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Product;

class ChangeProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        dd($this->id);
        return false;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|mimes:jpeg,png'
        ];
    }
}
