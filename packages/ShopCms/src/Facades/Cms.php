<?php

namespace ShopCms\Cms\Facades;

use Illuminate\Support\Facades\Facade;

class Cms extends Facade
{
    protected static function getFacadeAccessor() { return 'shoppingcms'; }
}

