<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//

use App\Brand;
use App\Product;
use Illuminate\Http\Request;

Route::get('/', function () {
    $products = Product::with('categories.maincategory')->inRandomOrder()->paginate(12); // with('categories')->
    $products->load('photos.file');
    $products2 = Product::with('categories.maincategory')->latest()->paginate(12); // with('categories')->
    $products2->load('photos.file');
    $brands = Brand::with('photo.file')->get();
    return view('theme.page', compact('products', 'products2', 'brands'));
})->name('home');


Route::name('shop.')->prefix('shop')->group(function(){
    Route::get('/', 'ShopController@index');
    Route::get('/product/{product}', 'ShopController@showProduct')->name('product');
    Route::get('/tag/{tag}/products', 'ShopController@showProductByTag')->name('tag');
    Route::get('/brand/{brand}/products', 'ShopController@showProductByBrand')->name('brand');
    Route::get('/category/{category}', 'ShopController@categorize')->name('category');
});

Route::view('about', 'theme.about')->name('about');

Route::view('contact', 'theme.contact')->name('contact');


Route::name('cart.')->prefix('cart')->group(function(){
    Route::get('/', 'CartController@showCart');
    Route::get('/empty', 'CartController@forgetCart')->name('forget');
    Route::get('/{product}/{action}', 'CartController@cartAction')->name('actions');
});

Route::name('checkout.')->middleware('auth')->prefix('checkout')->group(function() {
    Route::get('/', 'CheckoutController@index')         ->name('makeorder');
    Route::get('/{order}', 'CheckoutController@show')   ->name('order');
    Route::put('/{order}', 'CheckoutController@update')   ->name('order.update');
    Route::post('/{order}/pay', 'CheckoutController@pay')->name('payment');
    Route::get('/payment/{payment}/pay', 'CheckoutController@bill')->name('pay');
});

Route::name('userarea.')->prefix('userarea')->middleware(['phone', 'auth', 'verified'])->group(function() {
    Route::get('/', 'UserAreaController@index');
    Route::get('/orders', 'UserAreaController@orders')->name('orders');
    Route::get('/invoices', 'UserAreaController@invoices')->name('invoices');
    Route::get('/invoices/{invoice}', 'UserAreaController@invoice')->name('invoices.show');
    Route::get('/payments', 'UserAreaController@payments')->name('payments');
    Route::get('/payments/{payment}', 'UserAreaController@payment')->name('payment');
    Route::get('/customer', 'UserAreaController@customer')->name('customer');
    Route::post('/customer', 'UserAreaController@store')->name('customercreate');
    Route::post('/customer/{customer}', 'UserAreaController@update')->name('customeredit');
    Route::get('/user/edit', 'UserController@edit')->name('user.edit');
    Route::put('/user/update', 'UserController@update')->name('user.update');
});

Route::prefix('phone')->name('phone.')->group(function() {
   Route::get('verify', 'SMSController@sendVerification')->name('verify');
   Route::post('verify', 'SMSController@verifyCode')->name('verify');
   Route::post('verify/resend', 'SMSController@resendVerification')->name('resend-verify');
});

Route::get('gallery/main-gallery', 'GalleryController@index')->name('gallery.index');

Route::get('gallery/{gallery}', 'GalleryController@show')->name('gallery.show');

Route::prefix('form')->name('form.')->group(function() {
   Route::get('/', 'FormController@index');
   Route::post('/', 'FormController@store')->name('store');
});

Route::match(['get', 'post'], 'verify_payment/{gateway}', [
    'uses' => 'GateWaysController@verify',
    'as'   => 'verifypayment',
]);

Route::prefix('sitemaps')->name('sitemaps.')->group(function(){
    Route::get('/sitemaps.xml', 'SitemapsController@index');
    Route::get('/sitemap-{sitemap}.xml', 'SitemapsController@show')->name('show');
});

Auth::routes(['verify' => true]);

Route::prefix('auth/password')->name('auth.passwords.')->group(function(){
    Route::view('reset', 'auth.passwords.reset')->name('create');
    Route::post('reset/{type}', 'PasswordResetController@store')->name('store');
    Route::put('reset', 'PasswordResetController@update')->name('update');
    Route::get('change/{token}', 'PasswordResetController@show')->name('confirm.form');
    Route::post('sms/resend/{token}', 'PasswordResetController@resendSms')->name('resendsms');
    // Route::post('confirm/phone/{phone}', 'PasswordResetController@phoneVerify')->name('confirm.phone');
});

Route::get('search', 'SearchController@index')->name('pages.search');