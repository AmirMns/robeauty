<?php

use App\Offer;
use App\State;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){ // ->middleware('auth:api')
    Route::name('api.shop.')->group(function () {
        Route::get('/search', 'SearchController@main')->name('products.search.main');
        Route::get('/search_for/{query}', 'SearchController@pager')->name('products.search.pager');
        Route::get('/products', function () {
            $products = App\Product::with(['categories.maincategory', 'brand'])->paginate(12); //  // with('categories')->
            $products->load('photos.file');
            foreach ($products as $product) {
                $product->offered = $product->offer();
            }
            return $products;
        })->name('products.index');
        Route::get('/category/{category}', function ($category) {
            $main     = App\MainCategory::whereName($category)->firstOrFail();
            $products = App\Category::with('categorable')->where(['main_id' => $main->id, 'categorable_type' => 'App\Product'])->paginate(12);
            foreach ($products as $product) {
                $product->offered = $product->offer();
            }
            return $products;
        })->name('products.category');
        Route::get('/brand/{brand}', function ($brand) {
            $brand = App\Brand::whereName($brand)->firstOrFail();
            $products = App\Product::where('brand_id', $brand->id)->with(['photos.file', 'categories.maincategory'])->paginate(12);
            foreach ($products as $product) {
                $product->offered = $product->offer();
            }
            return $products;
        })->name('products.brand');
        Route::get('/tag/{tag}', function ($tag) {
            $tag = App\Tag::whereName($tag)->firstOrFail();
            $products = $tag->products()->with(['photos.file', 'categories.maincategory'])->paginate(12);
            foreach ($products as $product) {
                $product->offered = $product->offer();
            }
            return $products;
        })->name('products.tag');
    });
    Route::get('/products/categories', function () {
        return App\MainCategory::whereType('product')->get();
    })->name('products.categories');
    Route::get('gallery', function () {
        return App\File::whereType('photo')->orderBy('created_at', 'desc')->paginate(12);
    });
    Route::get('users', function(){
        return App\User::paginate(12);
    });
	Route::any('gallery/setImages/{method}/{id}', function($method,$id, Request $request){
        $photos = collect([]);
        if ($request->photos) {
            foreach ($request->photos as $photo) {
                $photos->push([
                    'file_id' => $photo['id'],
                    'title' => '',
                    'alt' => ''
                ]);
            }
        }
        switch ($method) {
            case 'products':
                $item = App\Product::find($id);
                $item->photos()->createMany($photos->toArray());
            break;
            case 'pages':
                $method = 'App\Page';
            break;
            case 'posts':
                $method = 'App\Page';
            break;
            case 'sliders':
                $method = 'App\Slider';
            break;
        }
        return [
            'request' => $item->photos
        ];
    })->name('setphoto');
    Route::any('addphoto/{method}/{id}', function($method,$id, Request $request){
        $photos = $request->photos;
        switch ($method) {
            case 'slideritem':
                $item = App\SliderItem::with('photo')->find($id);
                if ($item->photo) {
                    $item->photo()->update([
                        'gallery_id' => $photos[0]['id'],
                        'title' => 'slider item',
                    ]);
                } else {
                    $item->photo()->create([
                        'gallery_id' => $photos[0]['id'],
                        'title' => 'slider item',
                    ]);
                }
                break;
            case 'galleryitem':
                $gallery = \App\Gallery::findOrFail($id);
                foreach ($photos as $photo) {
                    $gallery->items()->create(['file_id' => $photo['id']]);
                }
            break;
            
        }
        return [
            'mehod' => $method,
            'id' => $id,
            'photos' => $photos
        ];
    })->name('addphoto');
});

Route::get('/state/{state}/cities', function (State $state) {
    $state->load('cities');
    return $state->cities->toJson();
})->name('stateapi');

Route::get('/products/{product}', function (App\Product $product) {
    return $product;
})->name('api.products.show');

Route::get('/orders/{order}/discount', function(App\Order $order, Request $request) {
    $offer = Offer::where('code', '=', $request->offer_code)->firstOrFail();
    return ['offer' => $order->discount($request->total_passed, $offer), 'offerId' => $offer->id];
})->name('api.discount');

Route::post('/orders/{order}/discount/{offer}/apply', function(App\Order $order, Offer $offer, Request $request) {
    $order->update(['offer_id' => $request->dismiss ? null : $offer->id]);
    return ['okay' => true, 'redirect' => route('checkout.order', ['order' => $order->id])];
})->name('api.discount.apply');
