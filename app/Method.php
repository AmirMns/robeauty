<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    public function photo()
    {
        return $this->morphOne(Photo::class, 'photoable');
    }
}
