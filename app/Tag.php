<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;
    public function pages()
    {
        return $this->belongsToMany(Page::class);
    }
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
    public function photo()
    {
        return $this->morphOne(Photo::class, 'photoable');
    }
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
