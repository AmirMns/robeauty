<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $casts = [
        'expired_at' => 'datetime',
    ];
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function checkToDestroy()
    {
        if ($this->max_use < 0 || $this->expired_at->unix() < time()) {
            $this->delete();
            return false;
        }
        return true;
    }
}
