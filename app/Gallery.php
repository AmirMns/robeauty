<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }
    public function items()
    {
        return $this->hasMany(GalleryItem::class);
    }
}
