<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'identification', 'password', 'api_token', 'verification_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified_at' => 'datetime',
    ];
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function likes()
    {
        return $this->hasMany(Like::class);
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    public function customer()
    {
        return $this->hasOne(Customer::class);
    }
	// public function active()
	// {
	// 	return $this->update(['active' => true]);
	// }
    public function hasRole($role)
    {
        if (isset($this->roles[0])) {
            if (is_string($role)) {
                return $this->roles->contains('name',$role);
            }
            return !! $role->intersect($this->roles)->count();
        }
        return false;
    }
    public function hasPermission($role, $permission)
    {
        $perms = $this->roles()->where("label",$role)->first()->permissions;

        if (is_string($permission)) {
            return $perms->contains('label',$permission);
        }

        $perms = $perms->mapToGroups(function ($item, $key) {
            return [$item['label']];
        })->first();

        $permission = collect($permission);

        return !! ($permission->intersect($perms)->count() == $permission->count());
    }
    public function assignRole($role)
    {
        return $this->roles()->sync(Role::whereId($role)->firstOrFail());
    }
}
