<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    public $timestamps = false;
    public function categories()
    {
        return $this->hasMany(Category::class, 'main_id', 'id');
    }
}
