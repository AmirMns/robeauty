<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['file_id', 'title', 'alt'];
    public function file()
    {
        return $this->belongsTo(File::class);
    }
    public function photoable()
    {
        return $this->morphTo();
    }
}
