<?php

namespace App;

use Illuminate\Support\Arr;

class Cart
{
    public $items = [];
    public $totalQuantity = 0;
    public $totalPrice    = 0;
    public $totalPay      = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQuantity = $oldCart->totalQuantity;
            $this->totalPrice = $oldCart->totalPrice;
            $this->totalPay = $oldCart->totalPay;
        }
    }

    public function add($item, $id)
    {
        $storedItem = ['quantity' => 0, 'price' => 0, 'item' => $item];
        if (count($this->items) > 0) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            }
        }
        $storedItem['quantity'] += 1;
        if (($item->quantity - $storedItem['quantity']) < 0) {
            flash()->error('عدم موحودی', 'کالای مورد نظر بیش از این تعداد موجود نیست');
            return false;
        }
        $storedItem['price']    = $item->price * $storedItem['quantity'];
        $this->items[$id] = $storedItem;
        $this->totalQuantity += 1;
        $this->totalPrice += $item->price;
        $this->totalPay += $item->offer();
        return true;
    }
    public function remove($id)
    {
        if ($this->items[$id]) {
            $item = $this->items[$id];
            $this->totalQuantity--;
            $this->totalPrice -= $item['item']->price;
            $this->totalPay   -= $item['item']->offer();
            if ($item['quantity'] > 1) {
                $item['quantity']--;
                $item['price'] = $item['item']->price * $item['quantity'];
                $this->items[$id] = $item;
            } else {
                Arr::forget($this->items, $id);
            }
        }
    }

    public function itemsThatAreNoLongerAvailable()
    {
        $errors = collect([]);
        foreach ($this->items as $item) {
            $product = Product::whereId($item['item']->id)->first();
            if ($product->quantity < $item['quantity']) {
                $decrease = ($product->quantity - $item['quantity']) * -1;
                $errors->push("{$item['item']->title} × {$decrease}");
                // $errors->push("آیتم {$item['item']->title} به میزان {$decrease} عدد در سبد خرید شما در انبار ما موجود نیست. \n می توانید میزان آن را کاهش داده و یا آن را از سبد خود حذف کنید.");
            }
        }
        return $errors;
    }

    public function toArray()
    {
        return [
            'items' => $this->items,
            'totalQuantity' => $this->totalQuantity,
            'totalPrice' => $this->totalPrice,
            'totalPay' => $this->totalPay
        ];
    }
}
