<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PaymentSuccedded extends Notification
{
    public $amount;
    public $pid;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($amount, $pid, $email)
    {
        $this->amount = $amount;
        $this->pid = $pid;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->email = $this->email;
        $message = (new  MailMessage)
                ->greeting('با سلام')
                ->subject('خرید از روبیوتی پرداخت با موفیت انجام شد')
                ->line('پرداخت با موفقیت در سایت انجام گرفت و اطلاعات سفارش و پرداخت شما در سایت ثبت شد')
                ->line('مبلغ پرداختی : ' . $this->amount .  'تومان')
                ->line('کد پرداخت' .  $this->pid)
                ->line('با تشکر از خرید شما');
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
