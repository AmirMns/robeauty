<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewFormSubmitted extends Notification
{
    public $manager;
    public $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($email, $request)
    {
        $this->manager = $email;
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $request = $this->request;
        $notifiable->email = $this->manager;
        return (new MailMessage)
            ->subject('پرسش جدید در فرم')
            ->line('نام مشتری : ' . $request->name)
            ->line('تلفن : ' . $request->phone)
            ->line('ایمیل : ' . $request->email)
            ->line('آشنایی : ' . $request->knowing)
            ->line('سوال : ', $request->question);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
