<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File as Document;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class File extends Model
{
    protected $fillable = ['name', 'path', 'thumbnail_path'];
    private $filename;
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
    public static function fromfile(UploadedFile $file)
    {
        $photo = new static;
        $photo->file = $file;
        $photo->fill([
            'name'          => $photo->fileName(),
            'path'          => $photo->MakeFilePath(),
            'thumbnail_path' => $photo->thumbPath()
        ]);
        return $photo;
    }
    public function fileName()
    {
        $org  = $this->file->getClientOriginalName();
        $name = pathinfo($org, PATHINFO_FILENAME);
        $ext  = $this->file->getClientOriginalExtension();
        return $this->filename = "{$name}" . '_' . time() . ".{$ext}";
    }
    public function MakeFilePath()
    {
        return $this->baseDir() . DIRECTORY_SEPARATOR . $this->filename;
    }
    public function thumbPath()
    {
        return $this->baseDir() . DIRECTORY_SEPARATOR . 'tn-' . $this->filename;
    }
    public function baseDir()
    {
        return 'images/gallery';
    }
    public function Upload()
    {
        $this->file->move($this->baseDir(), $this->filename);
        $this->makeThumbnail();
        return $this;
    }
    public function makeThumbnail()
    {
        Image::make($this->MakeFilePath())
        ->fit(200)->save($this->thumbPath());
        return $this;
    }
    public function delete()
    {
        parent::delete();
        Document::delete([
            $this->path,
            $this->thumbnail_path,
        ]);
    }
}
