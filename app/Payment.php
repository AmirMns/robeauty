<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }
    public function method()
    {
        return $this->belongsTo(Method::class, 'method_id');
    }
}
