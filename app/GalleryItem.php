<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryItem extends Model
{
    public $timestamps = false;
    protected $fillable = ['file_id', 'url', 'title', 'description'];

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
