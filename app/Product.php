<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model
{
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'title' => 10,
            'short_description' => 8,
            'english_description' => 6,
            'description' => 7,
        ],
    ];
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    public function categories()
    {
        return $this->morphMany(Category::class, 'categorable');
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function likes()
    {
        return $this->morphMany(Comment::class, 'likeable');
    }
    public function photo()
    {
        return $this->morphOne(Photo::class, 'photoable');
    }
    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }
    public function orderitems()
    {
        return $this->belongsToMany(OrderItem::class);
    }
    public function attribute()
    {
        return $this->belongsToMany(Attribute::class);
    }
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
    public function offer()
    {
        return ($this->off) ? ($this->price / 100) * (100 - $this->off) : $this->price;
    }
    public function displayPrice()
    {
        $price = number_format($this->price);
        if ($this->off) {
            $offered = number_format($this->offer());
            return "<span class='last-price'>{$price} تومان</span><br><span>{$offered} تومان</span>";
        }
        return "<span>{$price} تومان</span>";
    }
}
