<?php

namespace App\Http\Middleware;

use Closure;

class IfCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        $user->load('customer');
        if (! $user->customer) {
            return redirect()->to(route('userarea.customer'));
        } else {
            return $next($request);
        }
    }
}
