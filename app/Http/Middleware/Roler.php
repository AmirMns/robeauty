<?php

namespace App\Http\Middleware;

use Closure;

class Roler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! (auth()->user()->hasRole('ADM') || auth()->user()->hasRole('SYSADM'))) {
           abort(403, "No access");
        }
        return $next($request);
    }
}
