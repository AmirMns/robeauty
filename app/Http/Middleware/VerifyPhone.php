<?php

namespace App\Http\Middleware;

use Closure;

class VerifyPhone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = auth()->user()->identification;
        $phone = preg_match("/^[0-9]{11}$/", $id) ? $id : false;
        // $identifier = filter_var($id, FILTER_VALIDATE_EMAIL) ?: $phone;

        if ($phone && ! auth()->user()->verified_at) {
            return redirect()->to(route('phone.verify'));
        }
        return $next($request);
    }
}
