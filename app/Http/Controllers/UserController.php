<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        return view('userarea.edit',  compact('user'));
    }
    public function update(Request $request)
    {
        if ($request->new_password != $request->password_confirm) {
            flash()->error('خطا','تایید رمز عبور با رمز عبور همخوانی ندارد');
            return back();
        }
        $user = auth()->user();
        $user->password = bcrypt($request->new_password);
        $user->save();
        flash()->success('موفقیت','رمز عبور شما با موفقیت تغییر کرد');
        return back();
    }
}
