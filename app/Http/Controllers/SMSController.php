<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SMSController extends Controller
{
    public $from;
    public $pattern;
    public function __construct()
    {
        $this->middleware('auth')->only(['sendVerification', 'resendVerification','verifyCode']);
        $this->from = config('services.sms.from');
        $this->pattern = config('services.sms.verification_pattern');
    }
    public function sendVerification()
    {
        $user = auth()->user();
        if (! $user->verification_code) {
            $user->verification_code = rand(10000, 99999);
            $user->save();
            $this->sendVerifySms($this->pattern, $this->from, $user->identification,[
                'name' => $user->name,
                'verification_code' => strval($user->verification_code)
            ]);
        }
        return view('auth.verify-phone');
    }
    public function resendVerification()
    {
        $user = auth()->user();
        if ($user->verification_code) {
            $user->verification_code = rand(10000, 99999);
            $user->save();
            $this->sendVerifySms($this->pattern, $this->from, $user->identification,[
                'name' => $user->name,
                'verification_code' => strval($user->verification_code)
            ]);
            session()->put('resent', true);
        }
        return view('auth.verify-phone');
    }
    public function verifyCode(Request $request) {
        $user = auth()->user();
        if ($request->verfication_code == $request->verfication_code && is_null($user->verified_at)) {
            $user->verified_at = now();
            $user->save();
            return redirect()->to(route('userarea.'));
        }
        return back();
    }
    public function sendVerifySms($pattern,$from,$phone,$data)
    {
        $apiKey = "JWHFm5dTKYiuKxUQGH5tcsX-oSLlEBKcAhgruZ5LEEY=";
        $client = new \IPPanel\Client($apiKey);
        $client->sendPattern($pattern, $from, strval($phone), $data);
    }
}
