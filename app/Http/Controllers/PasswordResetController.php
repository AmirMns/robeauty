<?php

namespace App\Http\Controllers;

use App\Notifications\PasswordChangeRequested;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PasswordResetController extends Controller
{
    public function store(Request $request, $type)
    {
        $user = \App\User::whereIdentification($request->user)->whereNotNull('verified_at')->firstOrFail();
        $token = hash('sha256', $user->identification . '_' . time());
        $sms_code = rand(10000, 99999);
        $insert_array = [$user->identification, $token, $type, now(), $sms_code];
        DB::insert('INSERT INTO `password_resets` (identification, token, type, created_at, sms_code) values (?, ?, ?, ?, ?)', $insert_array);
        switch ($type) {
            case 'email':
                $request->validate([
                    'user' => 'required|email'
                ]);
                $user->notifyNow((new PasswordChangeRequested($user, route('auth.passwords.confirm.form', ['token' => $token]))));
                flash()->success('موفقیت', 'ایمیل بروزرسانی رمز عبور برای شما ارسال شد');
                return back()->with('email_sent', true)->with('email', $user->identification);
            break;
            case 'phone':
                $id = $user->identification;
                $phone = preg_match("/^[0-9]{11}$/", $id) ? $id : false;
                if (! $phone) {
                    return back();
                }
                $apiKey = "JWHFm5dTKYiuKxUQGH5tcsX-oSLlEBKcAhgruZ5LEEY=";
                $client = new \IPPanel\Client($apiKey);
                $data = [
                    'name' => $user->name,
                    'verification_code' => strval($sms_code)
                ];
                $client->sendPattern(config('services.sms.verification_pattern'), config('services.sms.from'), strval($phone), $data);
                return redirect()->to(route('auth.passwords.confirm.form', ['token' => $token]));
            break;
        }
    }
    public function show($token)
    {
        $raw = DB::select("SELECT * FROM `password_resets` WHERE `token` = '$token' ORDER BY `created_at` DESC LIMIT 1");
        if ($raw) {
            $pass_reset = (array) $raw[0];
            return view('auth.passwords.confirm', compact('token', 'pass_reset'));
        }
        abort(404);
    }
    public function update(Request $request)
    {
        $token = $request->token;
        $raw = DB::select("SELECT * FROM `password_resets` WHERE `token` = '$token' ORDER BY `created_at` DESC LIMIT 1");
        if ($raw) {
            $pass_reset = (array) $raw[0];
            $user = \App\User::whereIdentification($pass_reset['identification'])->whereNotNull('verified_at')->firstOrFail();
            if ($pass_reset['type'] == 'phone' && $request->sms_code != $pass_reset['sms_code']) { 
                flash()->error('خطا', 'کد تایید sms همخوانی ندارد');
                return back();
            }
            $user->password = bcrypt($request->password);
            $user->save();
            flash()->success('موفقیت', 'رمز عبور با موفقیت تغییر پیدا کرد');
            return redirect()->to(route('login'));
        }
        abort(403);
    }
    public function resendSms($token)
    {
        $raw = DB::select("SELECT * FROM `password_resets` WHERE `token` = '$token' ORDER BY `created_at` DESC LIMIT 1");
        if ($raw) {
            $phone_reset = (array) $raw[0];
            $sms_code = rand(10000, 99999);
            $id = $phone_reset['identification'];
            $phone = preg_match("/^[0-9]{11}$/", $id) ? $id : false;
            if (! $phone) {
                return back();
            }
            $apiKey = "JWHFm5dTKYiuKxUQGH5tcsX-oSLlEBKcAhgruZ5LEEY=";
            $client = new \IPPanel\Client($apiKey);
            $data = [
                'name' => 'کاربر',
                'verification_code' => strval($sms_code)
            ];
            DB::update("UPDATE `password_resets` SET `sms_code` = '$sms_code' WHERE `token` = '$token' ORDER BY `created_at` DESC LIMIT 1");
            $client->sendPattern(config('services.sms.verification_pattern'), config('services.sms.from'), strval($phone), $data);
            return ['okay' => true];
        }
        abort(403);
    }
}
