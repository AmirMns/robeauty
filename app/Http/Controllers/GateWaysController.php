<?php

namespace App\Http\Controllers;

use App\Notifications\PaymentSuccedded;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;
use SadadGateWay\Sadad\Facades\Sadad;
use Zarinpal\Laravel\Facade\Zarinpal;

class GateWaysController extends Controller
{
    public $from;
    public $id;
    public $phone;
    public function __construct()
    {
        $this->from = config('services.sms.from');
    }
    public function sms($amount , $paymentId, $target_phone)
    {
        $pid = $paymentId + 10000;
        if (config('services.sms.send_payment_sms')) {
            $apiKey = config('services.sms.api_token');
            $client = new \IPPanel\Client($apiKey);
            $bulkID = $client->send(
                $this->from,          // originator
                [$target_phone],    // recipients
                "پرداختی جدید در سایت مبلغ " . $amount . " تومان کد " . $pid // message
            );
            $message = $client->getMessage($bulkID);
        }
        if ($this->phone) {
            $bulkID = $client->send(
                $this->from,          // originator
                [$this->id],    // recipients
                "روبیوتی
                \n پرداخت به مبلغ $amount تومان انجام
                \n اطلاعات سفارش شما در وبسایت ما ثبت شده است
                \n کد پرداختی : $pid"
            );
            $message = $client->getMessage($bulkID);
        } else {
            if (auth()->user()) {
                auth()->user()->notifyNow(new PaymentSuccedded($amount, $pid, $this->id));
            }
        }
    }
    public function make_data($amount, $result,$data_validation = false)
    {
        $data = null;
        if ($data_validation) {
            $data = [
                'success' => true,
                'amount' => $amount,
                'RefID' => $result['RefID'],
                'title' => 'تراکنش موفق'
            ];
            if ($result['Status'] == 'success') {
                $data['again']   = false;
                $data['message'] = 'تراکنش تایید شد.';
            } else {
                $data['again']   = true;
                $data['message'] = 'تراکنش قبلا تایید شده است.';
            }
        } else {
            $data = [
                'success' => false,
                'amount' => $amount,
                'title' => 'تراکنش ناموفق',
                'message' => 'تراکنش توسط کاربر لغو شد.'
            ];
        }
        return $data;
    }
    public function zarinpal($request, $payment, $amount)
    {
        $data = null;
        if ($request->Status == 'OK') {
            $result = Zarinpal::verify($request->Status,$amount,$request->Authority);
            $data = $this->make_data($amount, $result, true);
            $payment->RefID = $data['RefID'];
            $payment->TraceID = null;
            $payment->status = 200;
            $payment->paid = true;
            $payment->save();
            $payment->load('invoice.order');
            $payment->invoice->order()->update(['status' => 'PAID', 'status_notes' => 'فاکتور پرداخت شده است']);
            $id = auth()->user()->identification;
            $this->id = $id;
            $this->phone = preg_match("/^[0-9]{11}$/", $id) ? $id : false;
            $this->sms($amount, $payment->id, "09222837511");
        } else {
            $data = $this->make_data($amount, null);
            $payment->status = 504;
            $payment->save();
        }
        return $data;
    }
    public function sadad($request, $payment, $amount)
    {
        $data = Sadad::validate($request->token, $request->ResCode);
        $id = auth()->user()->identification;
        if ($data) {
            $payment->RefID = $data['RefID'];
            $payment->TraceID = $data['TraceID'];
            $payment->status = 200;
            $payment->paid = true;
            $payment->save();
            $payment->load('invoice.order');
            $payment->invoice->order()->update(['status' => 'PAID', 'status_notes' => 'فاکتور پرداخت شده است']);
            $this->id = $id;
            $this->phone = preg_match("/^[0-9]{11}$/", $id) ? $id : false;
            $this->sms($amount, $payment->id, "09222837511");
        } else {
            $payment->status = 504;
            $payment->save();
        }
        return $data;
    }
    public function verify(Request $request, $gateway)
    {
        $authority = $request->Authority ?: $request->token;
        $payment = Payment::whereAuthority($authority)->first();
        $amount = $payment->amount;
        $data = null;
        switch ($gateway) {
            case 'zarinpal':
                $data = $this->zarinpal($request, $payment, $amount);
            break;
            case 'sadad':
                $data = $this->sadad($request, $payment, $amount);
            break;
        }
        return redirect()->to(route('userarea.payment', ['payment' => $payment->id, 'data' => $data]));
    }
}
