<?php

namespace App\Http\Controllers;

use App\Brand;
use App\MainCategory;
use App\Product;
use App\Tag;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index()
    {
        $categories = MainCategory::whereType('product')->get();
        $tags = Tag::whereType('product')->get();
        $brands = Brand::all();
        return view('theme.shop.index', compact('categories', 'tags', 'brands'))->with('target_url', route('api.shop.products.index'));
    }
    public function categorize($category)
    {
        $categories = MainCategory::whereType('product')->get();
        $tags = Tag::all();
        $brands = Brand::all();
        return view('theme.shop.index', compact('categories', 'tags', 'brands'))->with('target_url', route('api.shop.products.category', ['category' => $category]));
    }
    public function showProduct($product)
    {
        $product = Product::whereSlug($product)->firstOrFail();
        $product->load(['photos.file', 'brand', 'categories']);
        return view('theme.shop.show', compact('product'));
    }
    public function showProductByTag($tag)
    {
        $categories = MainCategory::whereType('product')->get();
        $tags = Tag::all();
        $brands = Brand::all();
        return view('theme.shop.index', compact('categories', 'tags', 'brands'))->with('target_url', route('api.shop.products.tag', ['tag' => $tag]));
    }
    public function showProductByBrand($brand)
    {
        $categories = MainCategory::whereType('product')->get();
        $tags = Tag::all();
        $brands = Brand::all();
        return view('theme.shop.index', compact('categories', 'tags', 'brands'))->with('target_url', route('api.shop.products.brand', ['brand' => $brand]));
    }
}
