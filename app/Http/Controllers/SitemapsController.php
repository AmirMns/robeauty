<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SitemapsController extends Controller
{
    public $sitemaps = ['products'];
    public function index()
    {
        $sitemaps = $this->sitemaps;
        return response()->view('layout.sitemaps.index', compact('sitemaps'))->header('Content-Type', 'text/xml');
    }
    public function show($sitemap)
    {
        abort_if(! in_array($sitemap, $this->sitemaps), 404);
        $target = $target = \App\Product::orderBy('updated_at', 'desc');
        $prefix  = ($sitemap == 'pages') ? '/' : $sitemap . '/';
        $results = $target->with(['photo.file'])->get();
        return response()->view('layout.sitemaps.show', compact('results', 'prefix'))->header('Content-Type', 'text/xml');
    }
}
