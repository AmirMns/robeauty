<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Method;
use App\Order;
use App\OrderItem;
use App\Payment;
use App\Product;
use App\Recipient;
use Illuminate\Http\Request;
use SadadGateWay\Sadad\Facades\Sadad;
use Zarinpal\Laravel\Facade\Zarinpal;

class CheckoutController extends Controller
{
    public function index(Request $request)
    {
        $order = null;
        if ($request->session()->has('cart')) {
            $user = auth()->user();
            if ($user->customer) {
                $cart = $request->session()->get('cart');
                $notAvailable = $cart->itemsThatAreNoLongerAvailable();
                if ($notAvailable->count() > 0) {
                    $toReturn = "<p>". config('shop.not-avilable-title') ."</p><ul class='col-12 float-right rtl text-right'>";
                    foreach($notAvailable as $err) {
                        $toReturn.= "<li>$err</li>";
                    }
                    $toReturn .= '</ul>';
                    flash()->error('عدم موجودی', $toReturn);
                    return redirect()->to(route('cart.'));
                }
                $order = $this->createOrder($cart, $user);
                foreach ($cart->items as $cartitem) {
                    $item = new OrderItem;
                    $item->product_id = $cartitem['item']->id;
                    $item->offer_amount = ($cartitem['item']->off) ? ($cartitem['item']->price / 100) * $cartitem['item']->off * $cartitem['quantity']: 0;
                    $item->quantity   = $cartitem['quantity'];
                    $item->weight     = 0;
                    $order->items()->create($item->toArray());
                    $this->decreaseQuantity($cartitem['item']->id, $cartitem['quantity']);
                }
                $request->session()->forget('cart');
            } else {
                return redirect()->to(route('userarea.customer'));
            }
        }
        return ($order) ? redirect()->to(route('checkout.order', ['order' => $order->id])) : redirect()->to(route('home'));
    }
    public function show(Order $order)
    {
        $order->load(['items.product', 'invoice.recipient.city' , 'invoice.payment']);
        auth()->user()->load('customer.city');
        $customer = auth()->user()->customer;
        $payment_methods = Method::with('photo.file')->whereType('payment')->get();
        $shipping_methods = Method::with('photo.file')->whereType('shippment')->get();
        $methods = ['payment' => $payment_methods, 'shippment' => $shipping_methods];
        return view('userarea.order', compact('order', 'customer', 'methods'));
    }
    public function update(Request $request, Order $order)
    {
        $invoice = Invoice::whereOrderId($order->id)->first();
        if (! $invoice->completed) {
            if ($request->use_customer) {
                $invoice->use_customer = true;
                $invoice->completed = true;
                $invoice->save();
            } else {
                $request->validate([
                    'fullname' => 'required|string|min:5|max:60',
                    'phone_number' => 'required|string|min:11|max:11|regex:/[0-9]/',
                    'postal_code' => 'required|string|min:5|max:25',
                    'address' => 'required|string|min:10|max:60'
                ]);
                $recipient = new Recipient;
                $recipient->fullname = $request->fullname;
                $recipient->address  = $request->address;
                $recipient->postal_code = $request->postal_code;
                $recipient->city_id = $request->city;
                $recipient->phone_number = $request->phone_number;
                $recipient->save();
                $invoice->use_customer = false;
                $invoice->completed = true;
                $invoice->recipient_id = $recipient->id;
                $invoice->save();
            }
            flash()->success('عملیات با موفقیت انجام شد', 'سفارش شما نهایی شد');
        }
        return back();
    }
    public function pay(Request $request, Order $order)
    {
        $shipping_method = Method::whereId($request->post_method)->first();
        $post_price = $shipping_method->price;
        $totalTax = ($order->total / 100) * 9;
        $totalforDiscount = $order->total + $totalTax; // checked
        $discounted = $order->discount($totalforDiscount);
        $totalAtAll = ($totalforDiscount - $discounted) + $post_price;
        $payment = new Payment;
        $payment->method_id = $request->method;
        $payment->status    = 0;
        $payment->paid      = false;
        $payment->authority = '';
        $payment->RefID     = null;
        $payment->amount    = $totalAtAll;
        $payment->save();
        $invoice = new Invoice;
        $invoice->payment_id = $payment->id;
        $invoice->completed      = false;
        $invoice->shipping_method_id = $shipping_method->id;
        $invoice = $order->invoice()->create($invoice->toArray());
        return redirect()->to(route('checkout.pay', ['payment' => $payment->id]));
    }
    public function bill(Request $request, Payment $payment)
    {
        $payment->load('method');
        switch ($payment->method->name) {
            case 'zarinpal':
                $user = auth()->user();
                $zp = new \Zarinpal;
                $result = $zp::request(
                    route('verifypayment', ['gateway' => 'zarinpal']),
                    $payment->amount,
                    "خرید اینترنتی محصولات روبیوتی"
                );
                $payment->authority = $result["Authority"];
                $payment->status = 1;
                $payment->save();
                return $zp::redirect();
            break;
            case 'sadad':
                $result = Sadad::send(1, route('verifypayment', ['gateway' => 'sadad']), $payment->amount);
                $payment->status = 1;
                $payment->authority = $result->Token;
                $payment->save();
                return Sadad::redirect($result);
            break;
            default:
                return back();
            break;
        }
    }

    public function decreaseQuantity($product, $quantity)
    {
        $product = Product::whereId($product)->first();
        $product->quantity -= $quantity;
        return $product->save();
    }

    public function createOrder($cart, $user)
    {
        $order = new Order;
        $order->total = $cart->totalPay;
        $order->total_number = $cart->totalQuantity;
        $order->total_weight = 0;
        $order->finished = false;
        $order->status = 'NOTPAID';
        $order->status_notes = "فاکتور هنوز پرداخت نشده است";
        $order = $user->customer->orders()->create($order->toArray());
        return $order;
    }
}

