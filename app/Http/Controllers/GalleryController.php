<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($gallery)
    {
        $gallery = Gallery::whereSlug($gallery)->with('photos.file')->firstOrFail();
        return view('theme.gallery', compact('gallery'));
    }

    public function index()
    {
        $images_gallery = Gallery::with('items.file')->whereSlug('images.gallery')->first();
        $videos_gallery = Gallery::with('items')->whereSlug('videos.gallery')->first();
        return view('theme.main-gallery', compact('images_gallery', 'videos_gallery'));
    }
}
