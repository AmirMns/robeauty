<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Payment;
use App\State;
use Illuminate\Http\Request;

class UserAreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $user = auth()->user();
        return view('userarea.index', compact('user'));
    }
    public function store(Request $request)
    {
        $customer = new Customer;
        $customer->first_name = $request->first_name;
        $customer->last_name  = $request->last_name;
        $customer->phone_number = $request->phone_number;
        $customer->postal_code = $request->postal_code;
        $customer->company = ($request->company) ? $request->company : '';
        $customer->city_id = $request->city;
        $customer->address1 = $request->address1;
        $customer->address2 = ($request->address2) ? $request->address2 : '';
        $customer->note = '';
        $user = auth()->user();
        $user->customer()->create($customer->toArray());
        return redirect()->to(route('userarea.customer'));
    }
    public function update(Request $request, Customer $customer)
    {
        $user = auth()->user();
        if ($customer->user_id == $user->id) {
            $customer->first_name = $request->first_name;
            $customer->last_name  = $request->last_name;
            $customer->phone_number = $request->phone_number;
            $customer->postal_code = $request->postal_code;
            $customer->company = ($request->company) ? $request->company : '';
            $customer->city_id = $request->city;
            $customer->address1 = $request->address1;
            $customer->address2 = ($request->address2) ? $request->address2 : '';
            $customer->save();
        }
        return back();
    }
    public function customer()
    {
        $user = auth()->user();
        $user->load('customer.city.state');
        $states = State::all();
        if ($user->customer) {
            $customer = $user->customer;
            $state = State::with('cities')->whereId($customer->city->state->id)->first();
            $cities = $state->cities;
            return view('userarea.customer.show', compact('user', 'customer', 'states', 'cities'));
        } else {
            $state = State::with('cities')->whereId(1)->first();
            $cities = $state->cities;
            return view('userarea.customer.create', compact('user', 'states', 'cities'));
        }
    }
    public function orders()
    {
        $user = auth()->user();
        $user->load('customer.orders.offer');
        if ($user->customer) {
            $customer = $user->customer;
            $orders = $customer->orders;
            return view('userarea.orders', compact('orders'));
        }
        return back();
    }
    public function invoices()
    {
        $user = auth()->user();
        $user->load('customer');
        if ($user->customer) {
            $invoices = \App\Invoice::whereHas("order", function($q) {
                $user = auth()->user();
                $q->where("customer_id", "=", $user->customer->id);
            })->latest()->get();
            $invoices->load(['order', 'payment.method']);
            return view('userarea.invoices', compact('invoices'));
        }
        return back();
    }
    public function payment(Request $request, Payment $payment)
    {
        $payment->load('method', 'invoice.order');
        $data = $request->data;
        return view('userarea.payment', compact('payment', 'data'));
    }
    public function payments()
    {
        $user = auth()->user();
        if ($user->customer) {
            $payments = Payment::whereHas('invoice.order', function($q){
                $user = auth()->user();
                $q->where('customer_id','=',$user->customer->id);
            })->get();
            $payments->load('method');
            return view('userarea.payments', compact('payments'));
        }
        return route('userarea.');
    }
}
