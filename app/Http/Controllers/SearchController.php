<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function main(Request $request)
    {
        $request->validate([
            'query' => 'string|required|min:3|max:80'
        ]);
        return Product::with('photo.file')->search($request['query'])->limit(5)->get();
    }
    public function pager(Request $request, $query)
    {
        $products =  Product::with(['photos.file', 'categories.maincategory', 'brand'])->search($query)->paginate(20);
        foreach ($products as $product) {
            $product->offered = $product->offer();
        }
        return $products;
    }
    public function index(Request $request)
    {
        $query = (isset($request['query']) && mb_strlen($request['query'], 'UTF-8') > 2) ? $request['query'] : false;
        $query_value = isset($request['query']) ? $request['query'] : false;
        $target_url = urldecode(route('api.shop.products.search.pager', ['query' => $query]));
        return view('theme.search', compact('target_url', 'query', 'query_value'));
    }
}
