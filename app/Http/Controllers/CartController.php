<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function showCart(Request $request)
    {
        $cart = $request->session()->get('cart', null);
        return ($request->wantsJson()) ? json_encode($cart->toArray()) : view('theme.shop.cart', compact('cart'));
    }
    public function forgetCart(Request $request)
    {
        $request->session()->forget('cart');
        return redirect()->to(route('cart.'));
    }
    public function cartAction(Request $request, Product $product, $action = 'add')
    {
        $product->load('photos.file');
        if ($product->photos) {
            $product->photo = $product->photos[0]->file->thumbnail_path;
            $product->photos = null;
        }
        $oldCart = $request->session()->has('cart') ? $request->session()->get('cart') : null;
		$cart    = new Cart($oldCart);
        switch ($action) {
            case 'remove':
                $cart->remove($product->id);
            break;
            default:
                $added = $cart->add($product, $product->id);
            break;
        }
        $request->session()->put('cart', $cart);
        if ($request->wantsJson()) {
            return ['okay' => $added, 'count' => count($cart->items)];
        }
        return back() ? back() : redirect()->to(route('shop'));
    }
}
