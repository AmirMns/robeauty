<?php
use App\Brand;
use App\Setting;
use Illuminate\Support\Facades\Cache;

function flash($title = NULL,$msg = NULL)
{
    $flash = app('App\Http\Flash');
    if (func_num_args() == 0) {
        return $flash;
    }
    return $flash->info($title,$msg);
}

function zeroIfNull($amount)
{
    return $amount ? $amount : 0;
}

function make_slug($string, $separator = '-')
{
    $string = trim($string);
	$string = mb_strtolower($string, 'UTF-8');
	$string = str_replace(' ', '-', $string);
	// Make alphanumeric (removes all other characters)
	// this makes the string safe especially when used as a part of a URL
	// this keeps latin characters and Persian characters as well
// 	$string = preg_replace("/[^a-z0-9_\s-۰۱۲۳۴۵۶۷۸۹ءاآؤئبپتثجچحخدذرزژسشصضطظعغفقکكگگلمنوهی]/u", '', $string);
// 	// Remove multiple dashes or whitespaces or underscores
//     dd($string);
// 	$string = preg_replace("/[\s-_]+/", ' ', $string);
// 	// Convert whitespaces and underscore to the given separator
// 	$string = preg_replace("/[\s_]/", $separator, $string);
//     $arabic_punct = ['،', '؛', '؟', '⠐', '!'];
//     foreach ($arabic_punct as $punct) {
//         $string = preg_replace("#".mb_strtolower($punct, 'UTF-8')."#", '', $string);
//     }
	return $string;
}

function quantity_message($q)
{
    return ($q > 1) ? "{$q} عدد موجود" : "ناموجود";
}

function ifPaid($paid)
{
    return ($paid) ? 'پرداخت شده <i class="fa fa-check" style="color:green;"></i>' : 'پرداخت نشده <i class="fa fa-times" style="color:red;"></i>';
}

function brands()
{
    return Brand::all();
}

if (! function_exists('setting')) {
    function setting($key)
    {
        $cache_key = 'settings.' . $key;
        if (Cache::has($cache_key)) {
            return Cache::get($cache_key);
        }
        $setting = Setting::where('key', $key)->first();
        if ($setting) {
            Cache::forever($cache_key, $setting->value);
            return $setting->value;
        }
        return null;
    }
}
