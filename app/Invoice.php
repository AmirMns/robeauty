<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['payment_id','valid','shipping_method_id', 'city_id', 'address', 'use_customer'];
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
    public function recipient()
    {
        return $this->belongsTo(Recipient::class);
    }
}
