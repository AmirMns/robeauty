<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $fillable = ['total','total_number','total_weight','offer_id','status_notes','finished','status'];
    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }
    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }
    public function discount($total_passed, $default_offer = null)
    {
        $total = 0;
        $offer = $default_offer ?: $this->offer()->first();
        if ($offer) {
            if ($offer->expired_at->unix() < time()) {
                $offer->delete();
                return $total;
            }
            switch ($offer->type) {
                case 'percent':
                    $total = ($total_passed / 100) * ($offer->amount);
                break;
                case 'price':
                    $total =  ($total_passed < $offer->max_total) ? $offer->amount : $total;
                break;
            }
            return ($total_passed > $offer->max_total) ? 0 : round(round($total, 0) / 1000, 1, PHP_ROUND_HALF_DOWN) * 1000;
        }
        return $total;
    }
}
