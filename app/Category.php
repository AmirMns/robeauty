<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function maincategory()
    {
        return $this->belongsTo(MainCategory::class, 'main_id');
    }
    public function categorable()
    {
        return $this->morphTo();
    }
}
