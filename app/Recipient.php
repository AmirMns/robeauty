<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    protected $fillable = ['fullname', 'phone_number', 'postal_code', 'city_id', 'address'];
    public function city()
    {
        return $this->belongsTo(City::class);
    }
    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }
}
