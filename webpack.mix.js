const mix = require('laravel-mix');

const route = {
    res: {
        sass: 'resources/sass/',
        js:   'resources/js/'
    },
    dest: {
        css: 'public/css',
        js:   'public/js'
    }
}

let {res, dest} = route;
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js(res.js + 'datepicker.js', dest.js)
// mix.js(res.js + 'time.js', dest.js)
mix.sass('resources/sass/animate.scss', 'public/css')
// .react(res.js + 'products.js', dest.js);
// mix.react('resources/js/products.js', 'public/js')
// mix.sass(res.sass + 'admin-rtl.scss', dest.css)
// .sass(res.sass + 'admin.scss', dest.css)
// .react(res.js + 'ck-editor.js', dest.js)
// mix.react(res.js + 'ck-editor.js', dest.js).sass(res.sass + 'app.scss', dest.css)
    // .react('resources/js/app.js', 'public/js')

// mix.react(res.js + 'ck-editor.js', dest.js)
// mix.react(res.js + 'app.js', dest.js)
// .sass(res.sass + 'app.scss', dest.css)
// .react(res.js + 'app.js', dest.js);
// .react(res.js + 'dropzone.js', dest.js)

// .react(res.js + 'CollapsableGallery.js', dest.js)
// mix.react(res.js + 'products.js', dest.js)
// .sass('resources/sass/animate.scss', 'public/css');
// mix.sass(res.sass + 'admin.scss', dest.css)
// .sass(res.sass + 'admin-rtl.scss', dest.css)
// .sass(res.sass + 'select2.scss', dest.css)
// mix.react('resources/js/shop.js', 'public/js');
