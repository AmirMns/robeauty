import React from 'react';
import ReactDOM from 'react-dom';
import Gallery from './components/Gallery';

var reactgallery = document.getElementById('react-gallery');
var mainpage = reactgallery.getAttribute('data-mainpage');
var deleter = reactgallery.getAttribute('data-delete');
var csrf = reactgallery.getAttribute('data-csrf');
var apikey = reactgallery.getAttribute('data-api-token');
ReactDOM.render(
	<Gallery Deleter={deleter} CSRF={csrf} APIToken={apikey} MainPage={mainpage} />, reactgallery
)

