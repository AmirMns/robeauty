try {
    // window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
require('select2');

$(document).on('select2:select', '#states', function (e) {
    let stateid = e.params.data.id;
    let http = $(this).attr('data-target');
    axios.get(`${http}/api/state/${stateid}/cities`)
    .then(function (response) {
        let {data} = response;
        $("#cities").html('');
        data.forEach(state => {
            $("#cities").append(`<option value="${state.id}">${state.name}</option>`);
        });
        $('#states').select2();
    });
    // console.log(stateid, http);
});




