import React from 'react';
import ReactDOM from 'react-dom';
import Products from './components/Products';

var target = document.getElementById('react-product-section');


if (target) {
    var URL = target.getAttribute('data-target');
    let cartAction = function(productId, action) {
        return CartAction.replace('productId', productId).replace('actionName', action);
    }
    let visitProduct = function(slug) {
        return VisitProduct.replace('slug', slug);
    }
    let visitCategory = function(name) {
        return VisitCategory.replace('categoryName', name);
    }
    ReactDOM.render(<Products asset={Asset} cartAction={cartAction} visitProduct={visitProduct} visitCategory={visitCategory} URL={URL} />, target);
}
