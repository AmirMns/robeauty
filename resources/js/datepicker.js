window.persianDate       = require('persian-date')
window.persianDatepicker = require('../../node_modules/persian-datepicker/dist/js/persian-datepicker');

$(document).ready(function() {
    var pd = $('#expired_at').persianDatepicker({
        format: 'dddd YYYY/MM/DD H:m:s',
        timePicker: {
            enabled: true,
            meridiem: {
                enabled: true
            }
        },
        checkYear: function(year) {
            return year >= new persianDate().year();
        },
        checkMonth: function(month) {
            return month >= new persianDate().month();
        },
        checkDate: function(date) {
            return date >= new persianDate();
        },
        onSelect: function(unix){
            $('input[name="gregorian_expired_at"]').val(unix)
        },
        onSet: function(unix){
            $('input[name="gregorian_expired_at"]').val(unix)
        },
        viewMode: 'year',
        calendar:{
            persian: {
                locale: 'fa'
            },
            gregorian: {
                locale: 'fa'
            }
        },
        toolbox:{
            calendarSwitch:{
                enabled: true
            },
            todayButton: {
                enabled: false
            }
        },
    });
    let expired_at = $('#expired_at');
    let defate = new persianDate().unix() * 1000;
    $('input[name="gregorian_expired_at"]').val(defate)
    if (expired_at.attr('data-expired_at')) {
        let dt = new Date(expired_at.attr('data-expired_at'));
        $('input[name="gregorian_expired_at"]').val(new persianDate(dt.valueOf()).unix() * 1000)
        defate = dt.valueOf();
    }
    pd.setDate(defate)
});