import React, { Component } from 'react'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import Axios from 'axios';

export default class SearchRow extends Component {
    constructor(props) {
        super(props);
    }
    handleItemClick(e) {
        var { Product } = this.props;
        const MySwal = withReactContent(Swal);
        MySwal.fire({
            title: 'Enter your IP address',
            input: 'text',
            inputValue: '',
            inputPlaceholder: 'max: ' + Product.count,
            showCancelButton: true,
            inputValidator: (value) => {
                if (!value) {
                    return 'You need to write Quantity!'
                } else if (Number(value) > Product.count) {
                    return "this quantity doesn't exist"
                }
            }
        }).then((result) => {
            if (result.value) {
                Swal.fire(
                    'Loading ...',
                    'please wait ...',
                    'info'
                )
                var target = `${this.props.PublicUrl}/api/v1/addtoOrder/${this.props.OrderId}/?api_token=thisistestapitoken`;
                Axios.post(target, {
                    product_id : Product.id,
                    quantity : Number(result.value)
                })
                .then(function (response) {
                    Swal.fire(
                        response.data.title,
                        response.data.message,
                        response.data.messageType
                    )
                    console.log(response);
                })
                .catch(function (error) {
                    Swal.fire(
                        'Error!',
                        '',
                        'error'
                    )
                });
            }
        })
    }
    render() {
        var { Product } = this.props;
        return (
            <li onClick={this.handleItemClick.bind(this)}>
                <img width="50" height="50" src={this.props.PublicUrl + '/' + Product.photos[0].gallery.thumbnail_path}/> { Product.title } - {Product.special_price}
            </li>
        )
    }
}
