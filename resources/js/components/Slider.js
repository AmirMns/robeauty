import React, { Component } from 'react'
import Slider from "react-slick";

export default class SimpleSlider extends Component {
  render() {
    var settings = {
      lazyLoad: 'ondemand',
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      fade:true,
    };
    return (
      <div>
        <Slider {...settings}>
            <div>
            <img src="/images/slider-images/slidert.png" alt=""/>
            <div className="shadow"></div>
            </div>
            <div>
            <img src="/images/slider-images/1.jpg" alt=""/>
            <div className="shadow"></div>
            </div>
            <div>
            <img src="/images/slider-images/slidert.png" alt=""/>
            <div className="shadow"></div>
            </div>
        </Slider>
      </div>

    );
  }
}
