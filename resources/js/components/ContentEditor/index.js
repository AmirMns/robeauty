import React, { Component } from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Axios from 'axios';

class ContentEditor extends Component {
    constructor(props) {
        super(props)
        this.state = {
            body: 'در حال لود ...'
        }
        Axios.get(this.props.Target).then((res) => {
            let { english_description } = res.data
            this.setState(prevState => ({
                body: english_description ? english_description : ''
            }));
            $("#english_description").html(body)
        })
    }
    render() {
        return (
            <div className="ContentEditor col-12 float-right">
                <CKEditor
                    editor={ ClassicEditor }
                    data={this.state.body}
                    config={{ language: this.props.Language }}
                    onInit={ editor => {} }
                    onChange={ ( event, editor ) => {
                        let body = editor.getData();
                        this.setState(prevState => ({
                            body: body
                        }));
                        $("#english_description").html(this.state.body)
                    } }
                    onBlur={ ( event, editor ) => {} }
                    onFocus={ ( event, editor ) => {} }
                />
            </div>
        );
    }
}

export default ContentEditor;
