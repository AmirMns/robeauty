import React, { Component } from 'react'

export default class Pagination extends Component {

    changePage = (index, e) => {
        this.props.setNewPage(index);
        this.props.sendAjax();
    }

    prePage = (e) => {
        let { currentPage } = this.props;
        if(currentPage === 1) {
            e.preventDefault();
        } else {
            this.props.setNewPage(currentPage - 1);
            this.props.sendAjax();
        }
    }

    nextPage = (e) => {
        let { pagesCount, currentPage } = this.props;
        if(currentPage === pagesCount) {
            e.preventDefault();
        } else {
            this.props.setNewPage(currentPage + 1);
            this.props.sendAjax();
        }
    }

    render() {
        let { pagesCount, currentPage } = this.props;
        let pages = [];
        for (let index = 1; index <= pagesCount; index++) {
            pages.push(<li key={index} className={currentPage === index ? "page-item active" : "page-item"} onClick={this.changePage.bind(this, index)}><a className={currentPage === index ? "page-link active bg-dark" : "page-link"} href="#">{index}</a></li>)
        }

        return (
            <div>
                <nav aria-label="Page navigation">
                    <ul className="pagination pagination-ul">
                        <li className={currentPage === 1 ? "page-item disabled" : "page-item"} onClick={this.prePage.bind(this)}><a className="page-link" href="#">قبلی</a></li>
                        {
                            pages.map((page, i) => {
                                return page
                            })
                        }
                        <li className={currentPage === pagesCount ? "page-item disabled" : "page-item"} onClick={this.nextPage.bind(this)}><a className="page-link" href="#">بعدی</a></li>
                    </ul>
                </nav>
            </div>
        )
    }
}
