import React, { Component } from 'react'
import Axios from 'axios';
import Item from './Item';
import Pagination from './Pagination';

export default class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            currentPage: 1
        }
        this.sendAjax();
    }

    setNewPage = (newPage) => {
        this.setState({
            currentPage: newPage
        })
    }

    sendAjax = () => {
        setTimeout(() => {
            Axios.get(`${this.props.URL}?page=${this.state.currentPage}`)
            .then(res => {
                this.setState({
                    data: res.data
                })
            })
        }, 1000);
    }

    render() {
        let { cartAction, visitProduct, visitCategory, asset } = this.props;
    
        if(this.state.data !== null) {
            let products = this.state.data.data;
            let pagesCount = this.state.data.last_page;
            let currentPage = this.state.data.current_page;

            return (
                <div className="products-section-container">
                    <div className="product-section">
                        {
                            products.map((item, i) => {
                                return (
                                    <Item asset={asset} cartAction={cartAction} visitProduct={visitProduct} visitCategory={visitCategory} key={i} {...item}/>
                                )
                            })
                        }
                        {products.length < 1 &&
                        <p className="p-3 loader text-center animated fadeIn">محصولی موجود نمی باشد</p>}
                    </div>
                    <Pagination pagesCount={pagesCount} sendAjax={this.sendAjax} setNewPage={this.setNewPage} currentPage={currentPage}/>
                </div>
            )
        } else {
            return (
                <div className="product-section animated fadeIn p-4"><p className="loader">در حال بارگیری <img src={asset + 'images/spinner.gif'} alt="loader image"/></p></div>
            )
        }
    }
}
