import React, { Component } from 'react'
import Axios from 'axios';

export default class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: this.props.quantity
        };
    }
    addToCart = (id) => {
        let target = this.props.cartAction(id, 'add');
        Axios.get(target, {"headers" : {"Accept" : "application/json"}}).then((res) => { 
            let {okay, count} = res.data;
            if (okay) {
                this.setState(prevState => ({
                    quantity: prevState.quantity - 1
                }));
                $('.mini-cart > span.badge').html(Number(count)); 
                alert('به سبد خرید اضافه شد')
            } else {
                alert('این محصول دیگر در انبار موجود نیست')
            }
        });
    }
    render() {
        let { id, slug, title, off, short_description, offered, categories, price, photos, cartAction, visitProduct, visitCategory, asset } = this.props;
        return (
            <div className="col-md-4 p-0 col-12 product animated fadeIn">
                <div className="product-info">
                    <a href={visitProduct(slug)}>
                        <div className="cover">
                            {photos.length > 0 &&
                                <img src={asset + photos[0].file.path} alt={photos[0].file.alt}></img>
                            }
                        </div>
                    </a>
                    <div className="undercover">
                        <a href={visitProduct(slug)} className="product-title">{title}</a>
                        <p className="product-summary">{short_description}</p>
                        <div className="categories">
                            {categories.map((cat, i) => {
                                <a key={i} href={visitCategory(cat.maincategory.name)}  className="badge badge-secondary badge-pill">{cat.maincategory.name}</a>
                            })}
                        </div>
                    </div>
                </div>
                <div className="down">  
                    <p className="price">{(off > 0) ? <span className="last-price">{price.toLocaleString('en')}</span> : price.toLocaleString('en')}
                    <span> تومان</span><br />
                    {off > 0 && 
                        <span>{offered.toLocaleString('en')} تومان</span>
                    }
                    {(this.state.quantity > 0 ) ? <span className="badge badge-success">{this.state.quantity} عدد موجود است</span> : <span className="badge badge-danger">ناموجود</span>}</p>
                    <div className="col-12 pl-2 pb-2 float-left toolbar">
                        <a href={visitProduct(slug)} className="btn ml-2"><i className="fa fa-eye"></i></a>
                        {this.state.quantity > 0 &&
                            <a onClick={this.addToCart.bind(this, id)} className="btn"><i className="fa fa-shopping-cart"></i></a>
                        }
                    </div>
                </div>
            </div>
        )
    }
}
