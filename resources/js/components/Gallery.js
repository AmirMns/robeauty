import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import Axios from 'axios';
export default class Gallery extends Component {
    constructor(props)	{
        super(props);
        this.state = {
            gallery: [],
            nextPage: 1,
            hasMore: true,
            selectedphotos: [],
            collapsed: true,
        }
    }
    handleLoadMore () {
        const target = `${this.props.MainPage}/api/v1/gallery/?page=${this.state.nextPage}&api_token=${this.props.APIToken}`;
        Axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
        Axios.get(target)
        .then(response => {
            const { current_page, last_page, data } = response.data;
            this.setState(prevState => ({
                gallery: [...prevState.gallery, ...data],
                hasMore: (current_page === last_page) ? false : true,
                nextPage: current_page + 1
            }))
        })
        .catch(error => {
            console.log(error);
        })
    }
    handleselectingimage(e) {
        if (this.state.selectedphotos.length < 9) {
            var imageid   = e.target.getAttribute('imgsrc');
            var imagesrc  = e.target.getAttribute('mainsrc');
            var thumbnail = e.target.getAttribute('src');
            if (this.props.SelectMulti) {
                this.setState(prevState => ({
                    selectedphotos: [
                        ...prevState.selectedphotos,{
                            thumbnail_path: thumbnail,
                            path: imagesrc,
                            id: imageid
                        }
                    ]
                }));
            } else {
                this.setState(prevState => ({
                    selectedphotos: [
                        {
                            thumbnail_path: thumbnail,
                            path: imagesrc,
                            id: imageid
                        }
                    ]
                }));
            }
        }
    }
    triggerDelete(index){
        if(window.confirm("Are you sure you want to do this action?")){

           this.setState(prevState => ({
               selectedphotos: [...prevState.selectedphotos.slice(0, index), ...prevState.selectedphotos.slice(index + 1)]
           }));

        }
    }
    renderdelete(img) {
        if (this.props.Deleter) {
            return (
                <form className="text-left" action={this.props.Deleter + '/' + img.id} method="post">
                    <input type="hidden" name="_method" value="DELETE"/>
                    <input type="hidden" name="_token" value={this.props.CSRF}/>
                    <button type="submit" className="btn btn-danger">&times;</button>
                </form>
            )
        }else {
            return (
                <div></div>
            )
        }
    }
    renderimageas(img) {
        if (this.props.SelectMulti) {
            return (
                <div className="selectableimage" onClick={this.handleselectingimage.bind(this)}>
                    <img imgsrc={img.id} mainsrc={this.props.MainPage + '/' + img.path} className="card-img-top" src={this.props.MainPage + '/' + img.thumbnail_path} alt=""/>
                </div>
            );
        } else {
            return (
                <div>
                    { this.renderdelete(img) }
                    <div className="selectableimage" onClick={this.handleselectingimage.bind(this)} href={this.props.MainPage + '/' + img.path}>
                        <img imgsrc={img.id} mainsrc={this.props.MainPage + '/' + img.path} className="card-img-top" src={this.props.MainPage + '/' + img.thumbnail_path} alt=""/>
                    </div>
                </div>
            );
        }
    }
    rendersavebtn()
    {
        if (this.state.selectedphotos.length > 0) {
            return (
                <div className="col-12 float-left"><button onClick={this.handleSaveSelected.bind(this)} className="btn btn-primary"><i className="fas fa-save"></i></button></div>
            )
        } else {
            return (
                <div></div>
            )
        }
    }
    loadClasses()
    {
        if (!this.props.Deleter) {
            if (this.state.collapsed) {
                return "collapsed-gallery";
            } else {
                return "collapsed-gallery opened";
            }
        }
        return "";
    }
    handleOpenGallery(e) {
        if (this.state.collapsed) {
            this.setState({
            collapsed: false,
            });
            return;
        }
        this.setState({
            collapsed: true,
        });
    }
    handleSaveSelected(e) {
        var url    = window.location.href;
        // var method = url.substring((url.indexOf('robeauty-shopcms/') + 10),url.indexOf('/edit'));
        const target = `${this.props.TargetPage}&api_token=${this.props.APIToken}`;
        Axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
        Axios.post(target, {
            photos: this.state.selectedphotos,
        })
        .then(response => {
            console.log(response);
            this.setState(prevState => ({
                selectedphotos: []
            }));
        })
        .catch(error => {
            console.log(error);
        })
    }
    render() {
        return (
            <div>
                <button className="btn btn-warning" onClick={this.handleOpenGallery.bind(this)}>
                    <i className="fas fa-images"></i>
                </button>
                <div className={this.loadClasses()}>
                    <div className="col-12-float-left text-right">
                    <button className="btn btn-danger" onClick={this.handleOpenGallery.bind(this)}>
                        <i className="fas fa-times"></i>
                    </button>
                    </div>
                    <div className="col-12 float-left mb-3">
                    {
                        this.state.selectedphotos.map((photo,i) => {
                            return (
                                <div key={i} className="card col-lg-1 col-md-2 col-3 float-left mb-3 p-0 text-center">
                                    <button className="btn btn-sm btn-danger" onClick={(e)=>{
                                        this.triggerDelete(i);
                                    }}><i className="fas fa-times"></i></button>
                                    <a href={photo.path} data-lity>
                                        <img className="text-center" src={photo.thumbnail_path} alt="" style={{ height: '50px',width: 'auto' }}/>
                                    </a>
                                </div>
                            )
                        })
                    }
                    { this.rendersavebtn() }
                    </div>
                    <div className="float-left col-12 hasscroller">
                        <InfiniteScroll	pageStart={0} loadMore={this.handleLoadMore.bind(this)} hasMore={this.state.hasMore}
                            loader={<div className="loader" key={0}>Loading ...</div>}
                            useWindow={false}
                            >
                            {
                                this.state.gallery.map((img,i) => {
                                return (
                                    <div key={i} className="card col-md-2 col-3 float-left p-0 mb-3">
                                        { this.renderimageas(img) }
                                    </div>
                                )
                                })
                            }
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
        )
    }
}
