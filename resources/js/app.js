
require('./bootstrap');
window.Swiper = require('swiper');

function isScrolledIntoView(elem) {
    var docViewTop = $('body').scrollTop();
    var docViewBottom = docViewTop + $('body').height();
    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height() - 100;
    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

$(document).ready(function(){
    $('body').scroll(function(){
       $('.scroll-animations .animated').each(function(){
            if (isScrolledIntoView(this) === true) {
                $(this).addClass($(this).attr('data-animation')).attr('data-animation', '');
                $(this).css({opacity: 1, animationFillMode: 'none'});
            }
       });
    });
    var productLink = (id) => {
        let url = $('.mini-cart').attr('data-product');
        return url.replace('productId', id);
    }
    $('.mini-cart').hover(function() {
        $('.flow-cart').html('<p class="col-12 text-right">در حال بارگذاری ...</p>')
        axios.get($(this).attr('data-cart'), {"headers" : {"Accept" : "application/json"}}).then(res => {
           let cart = res.data;
           if (cart && cart.items) {
                $('.flow-cart').html('');
                Object.values(cart.items).forEach(item => {
                    $('.flow-cart').append(
                    `<div class="cart-item p-3">
                    <img src="${(item.item.photo) ? $(this).attr('data-main') + item.item.photo : ''}">
                    <p class="text-right">
                        <a href="${productLink(item.item.slug)}">
                        ${item.item.title}(${item.quantity}x)
                        <br>
                        قیمت واحد : ${item.item.price.toLocaleString('en')} تومان
                        <b>مجموع: ${item.price.toLocaleString('en')} تومان</b>
                        </a>
                    </p>
                    </div>`
                    );
                });
               $('.flow-cart').append(`<a class="btn btn-sm btn-dark float-left m-3" href="${$(this).attr('data-cart')}">مشاهده سبد <i class="fa fa-shopping-bag fa-2x"></i></a>`);
           }
        });
    }, function() {});
});

$('#logouter').on('click', function() {
   $('#logout-form').submit();
});

$('#openmenu').on('click', function() {
    $('.menu').toggleClass('active');
});

$('#search-toggle').on('click', function() {
    
})
