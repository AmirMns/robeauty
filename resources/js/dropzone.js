window.Dropzone = require('dropzone');

Dropzone.options.addPhotosForm = {
    paramName: "photo",
    maxFilesize: 3,
    acceptedFiles: ".jpg, .jpeg, .png",
    dictInvalidFileType: "شما نمی توانید این فایل را آپلود کنید",
    dictDefaultMessage: "فایل ها را در اینجا آپلود کنید. \n\nحتی میتوانید آنها را در اینجا بیاندازید",
    dictFileTooBig: "حجم فایل بسیار زیاد است"
}
