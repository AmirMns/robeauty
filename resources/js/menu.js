$(document).ready(function(){
    $("#menuEditor").sortable();
    $('#menuEditor > li').each(function(){
        $(this).append('<span class="btn btn-sm btn-danger deletepar float-right m-0"><i class="fas fa-times"></i></span>');
        $(this).append('<span class="btn btn-sm mr-3 btn-info editpar float-right m-0"><i class="fas fa-pencil-alt"></i></span>');
    });
});
$("#addMenuItem").click(function(){
    var MNode = document.createElement("li");
    MNode.setAttribute("class", "item");
    var edit = '<span class="btn btn-sm mr-3 btn-info editpar float-right m-0"><i class="fas fa-pencil-alt"></i></span>';
    MNode.innerHTML =  '<a href="#test">آیتم منو</a><span class="btn btn-sm btn-danger deletepar float-right m-0"><i class="fas fa-times"></i></span>' + edit;
    var mparent = $(this).parent().parent();
    $(" > #menuEditor", mparent).append(MNode);
    $("#menuEditor").sortable();
});
$(document).on('click','.deletepar',function(){
    $(this).parent().remove();
});
$(document).on('click','.editpar',function(){
    var parenet = $(this).parent();
    var anchori = $(' > a', parenet);
    var thing = $(anchori).html().toString().replace(/"/g,"'");
    console.log(thing);
    $(parenet).html('').append('<input type="text" value="' + thing + '" class="form-control float-right col-6">')
    .append('<input type="text" value="' + $(anchori).attr('href') + '" class="form-control float-right col-6">')
    .append('<button type="button" class="btn btn-sm btn-primary mt-2 savepar"><i class="fas fa-save"></i></button>');
});
$(document).on('click','.savepar',function(){
    var parent = $(this).parent();
    let menuText = $(' > input:eq(0)', parent).val();
    let menuLink = $(' > input:eq(1)', parent).val();
    $(parent).html("");
    $(parent).append('<a href="' + menuLink + '">' + menuText + '</a>')
    .append('<span class="btn btn-sm btn-danger deletepar float-right m-0"><i class="fas fa-times"></i></span>')
    .append('<span class="btn btn-sm mr-3 btn-info editpar float-right m-0"><i class="fas fa-pencil-alt"></i></span>');
});
$("#addnewmenu").on('submit',function(e) {
	e.preventDefault();
    $(".deletepar,.editpar").remove();
    let vals = $("#menuEditor").html();
    $("#menubody").html(vals);
    this.submit();
});
