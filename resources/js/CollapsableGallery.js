import React from 'react';
import ReactDOM from 'react-dom';
import Gallery from './components/Gallery';
var collgallery = document.getElementById('react-collapsable-gallry');
if (collgallery) {
    var mainpage = collgallery.getAttribute('data-mainpage');
    var SelectMulti = collgallery.getAttribute('selectmulti');
    var csrf = collgallery.getAttribute('data-csrf');
    var targetpage = collgallery.getAttribute('data-target');
    var apikey = collgallery.getAttribute('data-api-token');
    ReactDOM.render(
        <Gallery SelectMulti={SelectMulti} APIToken={apikey} CSRF={csrf} TargetPage={targetpage} MainPage={mainpage} />, collgallery
    )
}
