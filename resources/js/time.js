var persianDate = require('persian-date')

$('.time-jalali').each(function() {
    let gdate = new Date($(this).attr('data-date'));
    let pdate = new persianDate(gdate.valueOf());
    $(this).html(pdate.format('dddd YYYY/MM/DD H:m:s'));
});
