@extends('layout.default')

@section('content')
<header>@include('theme.header')</header>
<div class="content shop" style="text-align:left;">
    <nav class="breadcrumb-holder col-12 float-left p-3" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">خانه</a></li>
            @yield('breadcrumb')
        </ol>
    </nav>
    @yield('shop-content')
</div>
@include('theme.components.footer')
@endsection

@section('script')
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scriptcode')
@endsection
