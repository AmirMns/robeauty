<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <style>
        .alert {
            font-family: Iransans;
        }
        .closedaddy {
            float: left;
        }
        .closedaddy:hover {
            cursor: pointer;
        }
    </style>
    @yield('head')
</head>
<body class="no-sc">
    @if (session()->has('flash_message'))
        <div class="col-12 float-left text-right alert alert-{{ session('flash_message.level') == 'error' ? 'danger' : session('flash_message.level') }}">{{ session('flash_message.title') }} : {!! session('flash_message.message') !!} <i class="closedaddy fa fa-times"></i></div>
    @endif
    @if ($errors->any())
        @foreach ($errors->all() as $err)
            <div class="col-12 float-left m-0 text-right alert alert-danger">{{ $err }} <i class="closedaddy fa fa-times"></i></div>
        @endforeach
    @endif
    @yield('content')
    @yield('script')
    <script>
        $('.closedaddy').click(function() { $(this).parent().slideUp() })
    </script>
</body>
</html>
