@php echo '<?xml version="1.0" encoding="UTF-8"?>'; @endphp

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    @foreach ($results as $result)
    <url>
        <loc>{{ asset($prefix . $result->slug) }}</loc>
        @if($result->updated_at)
        <lastmod>{{ $result->updated_at->format('Y-m-d') }}</lastmod>
        @endif
        @if ($result->seo)
        <changefreq>{{ $result->seo->changefreq }}</changefreq>
        <priority>{{ $result->seo->priority }}</priority>
        @endif
        @if ($result->photo)
        <image:image>
            <image:loc>{{ asset($result->photo->file->path) }}</image:loc>
            <image:caption>{{ $result->short_description }}</image:caption>
            <image:title>{{ $result->title }}</image:title>
        </image:image>
        @endif
    </url>
    @endforeach
</urlset>