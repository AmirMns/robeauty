@extends('layout.default')

@section('content')
<header>
    @include('theme.header')
</header>
<div class="content col-12 p-3 userarea">
    <div class="title-row float-left">
        <h4 class="title">حساب کاربری من</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
    </div>
    <div class="account col-12 float-left">
        <div class="col-md-3 col-12 p-0 account-menu">
            <ul>
                <li><a href="{{ route('userarea.') }}"><i class="fa fa-dashboard"></i>داشبورد</a></li>
                <li><a href="{{ route('userarea.orders') }}"><i class="fa fa-cart-arrow-down"></i>سفارشات</a></li>
                <li><a href="{{ route('userarea.customer') }}"><i class="fa fa-map-marker"></i>اطلاعات ارسال</a></li>
                <li><a href="{{ route('userarea.invoices') }}"><i class="fa fa-invoice"></i>رسید ها</a></li>
                <li><a href="{{ route('userarea.payments') }}"><i class="fa fa-credit-card"></i>پرداختی ها</a></li>
                <li><a href="{{ route('userarea.user.edit') }}"><i class="fa fa-user"></i>اطلاعات حساب کاربری</a></li>
                <li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
                    <a href="#" id="logouter"><i class="fa fa-sign-out"></i>خروج</a>
                </li>
            </ul>
        </div>
        <div class="col-md-9 col-12 p-3 account-content">
            @yield('dashboardcontent')
        </div>
    </div>
</div>
@include('theme.components.footer')
<script src="{{ asset('js/app.js') }}"></script>
@endsection

