@extends('layout.app')

@section('content')
<div class="container" style="direction: rtl;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">شماره تلفن خود را تایید کنید</div>

                <div class="card-body">
                    <form class="form-group float-left col-12" action="{{ route('phone.verify') }}" method="post">
                    @csrf
                        <div class="col-12 float-left input-group">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fa fa-hashtag"></i></span>
                            </div>
                            <input class="form-control" type="text" name="verfication_code" id="verfication_code">
                            <button type="submit" class="btn btn-primary">تایید</button>
                        </div>
                    </form>
                    @if (session('resent'))
                        <div class="alert alert-success float-right" role="alert">
                            یک کد تایید تازه به شماره شما ارسال شد
                        </div>
                    @endif

                    <div class="col-12 float-right p-3">
                        <p class="text-right float-right">قبل از ادامه پیامک های خود را چک کنید
                        اگر کدی دریافت نکردید, </p>
                        <form class="d-inline text-right float-right" method="POST" action="{{ route('phone.resend-verify') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">روی این دکمه کلیک کنید</button>.
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
