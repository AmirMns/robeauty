@extends('layout.app')

@section('head')
    <title>ورود</title>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-register">
                <div class="card-header">ورود</div>
                <div class="card-body">
                    <div class="col-12 flaot-left text-center">
                        <a href="{{ asset('/') }}"><img src="/images/logo.png" alt="Robeauty logo"></a>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group col-md-8 col-12 offset-md-2 offset-0">
                            <div class="col-12 mb-4">
                                <input placeholder='آدرس ایمیل یا شماره تلفن' id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="identification" value="{{ old('identification') }}" required autofocus>
                                @error('identification')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 mb-4">
                                <input placeholder='رمز عبور' id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-8 offset-md-2 col-12 offset-0 float-left">
                                <button type="submit" class="btn btn-primary float-left col-10 offset-1">ورود</button>
                                <div class="col-12 mt-3 text-center float-left">
                                    <a class="col-12 text-center" href="{{ route('register') }}">ثبت نام ؟</a>
                                    <br>
                                    <br>
                                    <a class="col-12 text-center" href="{{ route('auth.passwords.create') }}">رمز عبور را فراموش کرده اید ؟</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
