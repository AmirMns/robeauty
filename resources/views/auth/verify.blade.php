@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-register">
                <div class="card-header">حساب خود را تایید کنید</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            یک لینک تایید تازه به ایمیل شما ارسال شد
                        </div>
                    @endif

                    <p class="text-white text-right col-12 rtl">
                    قبل از ادامه ایمیل خود را چک کنید
                    اگر ایمیلی دریافت نکردید،
                    </p>
                    <form class="col-12 text-center" class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-dark m-0">اینجا کلیک کنید</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
