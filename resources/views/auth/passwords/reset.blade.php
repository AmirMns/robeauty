@extends('layout.app')
@section('head')
<title>فراموشی رمز عبور</title>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-register">
                <div class="card-header">فراموشی رمز عبور</div>
                <div class="card-body">
                    <div class="col-12 text-center">
                        <a href="{{ asset('/') }}"><img src="/images/logo.png" alt="Robeauty logo"></a>
                    </div>
                    @php
                        $type = request()->type ? request()->type : 'email';
                    @endphp
                    <form method="POST" action="{{ route('auth.passwords.store', ['type' =>  $type]) }}">
                        @csrf
                        <div class="col-12 float-left p-3 input-group">
                            <div class="input-group-prepend">
                                <button type="submit" class="btn btn-dark">تایید</button>
                            </div>
                            <input required placeholder="{{ $type == 'email' ? 'ایمیل' : 'شماره تلفن'}} خود را وارد کنید" type="text" class="form-control" name="user" id="user">
                        </div>
                        <div class="col-12 float-left text-center p-3 input-group">
                            @if($type == 'email')
                            <a href="{{ route('auth.passwords.create', ['type' => 'phone']) }}" class="btn btn-primary float-right">با شماره تلفن ثبت نام کرده ام</a>
                            @else
                            <a href="{{ route('auth.passwords.create', ['type' => 'email']) }}" class="btn btn-danger float-right">با ایمیل ثبت نام کرده ام</a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
