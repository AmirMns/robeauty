@extends('layout.app')
@section('head')
<title>تغییر رمز عبور</title>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-register">
                <div class="card-header">تغییر رمز عبور</div>
                <div class="card-body">
                    <div class="col-12 text-center">
                        <a href="{{ asset('/') }}"><img src="/images/logo.png" alt="Robeauty logo"></a>
                    </div>
                    <form method="POST" action="{{ route('auth.passwords.update') }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="token" value="{{ $token }}">
                        @if ($pass_reset['type'] == 'phone')
                        <div class="col-12 float-left p-3 input-group">
                            <div class="input-group-prepend">
                                <button type="button" disabled id="resend_sms" class="btn btn-dark"><span class="badge badge-light p-1 time-counter">0</span> ارسال مجدد</button>
                            </div>
                            <input required placeholder="کد تایید ارسالی" type="text" class="form-control" name="sms_code" id="sms_code">
                        </div>
                        @endif
                        <div class="col-12 float-left p-3 input-group">
                            <input required placeholder="رمز عبور جدید" class="form-control" name="password" id="password" type="password">
                        </div>
                        <div class="col-12 float-left text-center p-3 input-group">
                            <button type="submit" class="btn btn-primary">تایید</button>
                            {{-- @if($type == 'email')

                            <a href="{{ route('auth.passwords.create', ['type' => 'phone']) }}" class="btn btn-primary float-right">با شماره تلفن ثبت نام کرده ام</a>
                            @else
                            <a href="{{ route('auth.passwords.create', ['type' => 'email']) }}" class="btn btn-danger float-right">با ایمیل ثبت نام کرده ام</a>
                            @endif --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        var DefaultTime = 3;
        var TimerCount = DefaultTime;
        var Times = 1;
        var timeInterVal = null;
        var resend_sms_url = "{{ route('auth.passwords.resendsms', ['token' => $token]) }}";
        function changeTimer (parent) {
            if (timeInterVal) {
                clearInterval(timeInterVal)
            }
            timeInterVal = setInterval(function() {
                $(`#${parent} > span.time-counter`).html(TimerCount + ' ثانیه ');
                TimerCount -= 1;
                if (TimerCount < 0) {
                    clearInterval(timeInterVal)
                    $(`#${parent}`).attr('disabled', false);
                }
            }, 1000)
        }
        $(document).ready(function() {
            changeTimer('resend_sms');
            $('#resend_sms').on('click', function() {
                $(this).attr('disabled', true)
                Times += 1;
                TimerCount = DefaultTime * Times;
                changeTimer('resend_sms');
                axios.post(resend_sms_url, {}).then(res => {
                    console.log(res.data);
                })
            });
        })
    </script>
@endsection