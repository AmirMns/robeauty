@extends('layout.app')
@section('head')
<title>ثبت نام</title>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-register">
                <div class="card-header">ثبت نام</div>
                <div class="card-body">
                    <div class="col-12 text-center">
                        <a href="{{ asset('/') }}"><img src="/images/logo.png" alt="Robeauty logo"></a>
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group col-md-8 col-12 offset-md-2 offset-0">
                            <div class="col-12 float-left mb-4">
                                <input placeholder='نام و نام خانوادگی' id="name" type="text" class="form-control text-right @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 float-left mb-4">
                                <input placeholder='آدرس ایمیل یا شماره تلفن' id="identification" type="text" class="form-control @error('email') is-invalid @enderror" name="identification" value="{{ old('identification') }}" required autocomplete="email">

                                @error('identification')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 float-left mb-4">
                                <input placeholder='رمز عبور' id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 float-left mb-4">
                                <input placeholder='تایید رمز عبور' id="password-confirm" type="password" class="form-control mb-1" name="password_confirmation" required autocomplete="new-password">
                            </div>
                            <div class="col-md-4 offset-md-4 col-12 offset-0 float-left mb-4 ">
                                <button type="submit" class="btn btn-primary float-left col-10 offset-1">
                                    ثبت نام
                                </button>
                                <div class="col-12 float-left">
                                    <a class="col-12 text-center" href="{{ route('login') }}">ورود ؟</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
