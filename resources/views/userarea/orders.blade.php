@extends('layout.userarea')

@section('head')
    <title>ناحیه کاربری - سفارش ها</title>
@endsection

@section('dashboardcontent')
<div class="title-row float-left">
    <h4 class="title">سفارش ها ({{ $orders->count() }}) :</h4>
    <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
</div>
<table class="table text-center">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">کد سفارش</th>
            <th scope="col">تعداد کل</th>
            <th scope="col">جمع (تومان)</th>
            <th scope="col">تخفیف</th>
            <th scope="col">پرداختی (تومان)</th>
            <th scope="col">جزئیات</th>
            {{-- <th scope="col">حذف</th> --}}
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
        <tr class="@if($order->status == "NOTPAID") notpaid @endif" title="{{ $order->status_notes }}">
            <th scope="row">{{ $loop->index + 1 }}</th>
            <td>{{ $order->id }}</td>
            <td>{{ $order->total_number }}</td>
            <td>{{ number_format($order->total) }}</td>
            <td>
                @if ($order->offer)
                {{ $order->offer->amount }} {{ $order->offer->type }}
                @else
                <i class="fa fa-1x fa-times"></i>
                @endif
            </td>
            <td>{{ number_format($order->total) }}</td>
            <td><a href="{{ route('checkout.order', ['order' => $order->id]) }}" class="btn btn-sm btn-outline-info"><i class="fa fa-eye"></i></a></td>
            {{-- <td><a href="#" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></a></td> --}}
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

