@extends('layout.userarea')

@section('head')
    <title>ناحیه کاربری - مشاهده پرداخت شماره {{ $payment->id }}</title>
@endsection

@section('dashboardcontent')
<h3 class="col-12">پرداخت شماره {{ $payment->id }}# - <span id="counter">5</span></h3>
<table class="table text-center">
    <thead class="thead-dark">
        <tr>
            <th scope="col">وضعیت</th>
            <th scope="col">مبلغ</th>
            <th scope="col">روش پرداخت</th>
            <th scope="col">زمان پرداخت</th>
            <th scope="col">کد تراکنش</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{!! ifPaid($payment->paid) !!}</td>
            <td>{{ number_format($payment->amount) }}</td>
            <td>{{ $payment->method->name }}</td>
            <td>{{ $payment->updated_at->diffForHumans() }}</td>
            <td>{{ $payment->RefID }}</td>
            {{-- <td><a href="#" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></a></td> --}}
        </tr>
    </tbody>
</table>
@if ($data)
<script>
    var seconds = 5;
    const intb = window.setInterval(function() {
        seconds -= 1;
        $('#counter').html(seconds);
    }, 1000);
    window.setTimeout(function() {
        window.clearInterval(intb);
        window.location.href = "{{ route('checkout.order', ['order' => $payment->invoice->order->id]) }}"
    }, seconds * 1000)
</script>
@endif
@endsection

