@extends('layout.userarea')

@section('head')
    <title>رسید</title>
@endsection

@section('dashboardcontent')
<div class="title-row float-left">
    <h4 class="title">رسید ها ({{ $invoices->count() }}) :</h4>
    <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
</div>
<table class="table text-center">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">مبلغ(تومان)</th>
            <th scope="col">پرداخت شده</th>
            <th scope="col">روش پرداخت</th>
            <th scope="col">مشاهده سفارش</th>
            <th scope="col">پرداخت</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($invoices as $invoice)
        <tr title="{{ $invoice->status_notes }}">
            <th scope="row">{{ $loop->index + 1 }}</th>
            <td>{{ number_format($invoice->payment->amount) }}</td>
            <td>{!! ifPaid($invoice->payment->paid) !!}</td>
            <td>{{ $invoice->payment->method->name }}</td>
            <td><a href="{{ route('checkout.order', ['order' => $invoice->order->id]) }}" class="btn btn-sm btn-outline-info"><i class="fa fa-eye"></i></a></td>
            <td>
                @if ($invoice->payment->paid)
                ---
                @else
                <form action="{{ route('checkout.pay', ['payment' => $invoice->payment->id]) }}" method="get">
                    <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-credit-card"></i></button>
                </form>
                @endif
            </td>
            {{-- <td><a href="#" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></a></td> --}}
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
