@extends('layout.userarea')

@section('head')
    <title>ناحیه کاربری - مشاهده سفارش شماره ({{ $order->id  }})</title>
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
@endsection

@php
    $totalWithTax = ($order->total / 100) * 9;
    $all_offer = 0;
@endphp

@section('dashboardcontent')
<h6>سفارش شماره ({{ $order->id }})</h6>
<table class="table table-bordered text-center">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">عنوان</th>
            <th scope="col">تعداد</th>
            <th scope="col">قیمت واحد</th>
            <th scope="col">مجموع</th>
            <th scope="col">تخفیف</th>
            <th scope="col">مجموع پس از کسر</th>
        </tr>
    </thead>
    <tbody class="order-body">
        @foreach ($order->items as $item)
        <tr>
            <th scope="row">{{ $loop->index + 1 }}</th>
            <td>{{ $item->product->title }}</td>
            <td>{{ $item->quantity }}</td>
            <td>{{ number_format($item->product->price) }}</td>
            <td>{{ number_format($item->product->price * $item->quantity) }}</td>
            <td>{{ $item->offer_amount ? number_format($item->offer_amount) : '%' . zeroIfNull($item->product->off) }}</td>
            <td>{{ $item->offer_amount ? number_format(($item->product->price * $item->quantity) - $item->offer_amount) : number_format($item->product->offer() * $item->quantity) }}</td>
        </tr>
        @php $all_offer += $item->offer_amount ?: 0; @endphp
        @endforeach
        <tr>
            <td></td>
            <td>9% مالیات ارزش افزوده</td>
            <td>---</td>
            <td>---</td>
            <td>{{ number_format($totalWithTax) }}</td>
            <td>---</td>
            <td>{{ number_format($totalWithTax) }}</td>
        </tr>
        @php
            $post_price = 0;
        @endphp
        @if ($order->invoice && $order->invoice->shipping_method_id)
            @php
                $shipping_method = \App\Method::whereId($order->invoice->shipping_method_id)->first();
                $post_price = $shipping_method->price;
            @endphp
            <tr id='shipping-pricing'>
                <td></td>
                <td>هزینه ارسال</td>
                <td>---</td>
                <td>---</td>
                <td>{{ number_format($post_price) }}</td>
                <td>---</td>
                <td>{{ number_format($post_price) }}</td>
            </tr>
        @endif
        <tr class="col-12">
            <td></td>
            <th scope="row">مجموع کل</th>
            <td>{{ $order->total_number }}</td>
            <td></td>
            @php
                $totalforDiscount = $order->total + $totalWithTax + $post_price;
                $discounted = $order->discount($totalforDiscount);
                $totalAtAll = $totalforDiscount - $discounted;
            @endphp
            <td>{{ number_format($totalforDiscount + $all_offer) }}</td>
            <th style="direction: ltr;color: red;">{{ number_format(($all_offer) * -1 ) }}</th>
            <td>{{ number_format($totalforDiscount) }}</td>
            @if($order->offer_id)<td class="text-danger" style="direction:ltr;"><b>{{ $discounted * -1 }}</b></td>@endif
            <th>{{ number_format($totalAtAll) }} تومان</th>
        </tr>
    </tbody>
</table>
@if (! $order->invoice)
<div class="col-12 p-3 text-left float-left">
    <form action="{{ route('checkout.payment', ['order' => $order->id]) }}" method="post">
        @csrf
        <div class="payment-methods m-3">
            @include('userarea.components.offer_code')
            @include('userarea.components.payment_method')
            @include('userarea.components.shippment_method')
        </div>
        <div class="col-12 mt-4 float-left text-center">
            <button type="submit" id="pay-btn" class="btn btn-dark cart" href="{{ route('checkout.makeorder') }}"
            data-amount="{{ $totalforDiscount }}"
            >پرداخت</button>
        </div>
    </form>
</div>
@else
<div class="col-12 p-3 text-center float-left">
    <h6>وضعیت پرداخت : {{ ! $order->invoice->payment->paid ? null : $order->invoice->payment->updated_at->diffForHumans() }} {!! ifPaid($order->invoice->payment->paid) !!}</h6>
    @if (! $order->invoice->payment->paid)
        <a href="{{ route('checkout.pay', ['payment' => $order->invoice->payment->id]) }}" class="btn btn-dark">پرداخت</a>
    @else
        @component('userarea.components.reciever', ['order' => $order, 'customer' => $customer]) @endcomponent
    @endif
</div>
@endif
@endsection
@section('script')
<script>

$('#states,#cities').select2();

var Total = {{ $totalAtAll }};
var Total1 = {{ $totalforDiscount + $all_offer }};
var TotalWithoutOff = {{ $totalforDiscount }}

$('.priceable').on('click', function() {
    let price = Number($(this).attr('data-price'));
    let amount = Total + price;
    let amountwithoutoffer = TotalWithoutOff + price
    let amountbefore = Total1 + price;
    console.log(amount);
    $('.order-body > tr:last-child > td:nth-child(5)').html(amountbefore.toLocaleString('en'));
    $('.order-body > tr:last-child > td:nth-child(7)').html(amountwithoutoffer.toLocaleString('en'));
    $('.order-body > tr:last-child > th:last-child').html(amount.toLocaleString('en') + 'تومان ');
    var lasttr = $('.order-body > tr:last-child').html();
    console.log(lasttr);
    $('.order-body > tr:last').remove();
    $('#shipping-pricing').remove();
    let newtr = `<tr id='shipping-pricing'>
        <td></td>
        <td>هزینه ارسال</td>
        <td>---</td>
        <td>---</td>
        <td>${price.toLocaleString('en')}</td>
        <td>---</td>
        <td>${price.toLocaleString('en')}</td>
    </tr>`;
    $('.order-body').append(newtr);
    $('.order-body').append(`<tr>${lasttr}</tr>`);
});

$('#use_customer').on('click', function() {
    $('#recivient_data').toggleClass('d-none');
});

$(document).ready(function() {
    $(document).on('select2:select', '#states', function (e) {
        let stateid = e.params.data.id;
        let http = $(this).attr('data-target');
        axios.get(`${http}/api/state/${stateid}/cities`)
        .then(function (response) {
            let {data} = response;
            $("#cities").html('');
            data.forEach(state => {
                $("#cities").append(`<option value="${state.id}">${state.name}</option>`);
            });
            $('#states').select2();
        });
    });
    $('#shipping-data-button').on('click', function(e) {
        e.preventDefault();
        if (! $('#use_customer').is(':checked')) {
            $('#recivient_data input,textarea,select').each(function(e) {
                // console.log(this);
                $(this).attr('name', $(this).attr('data-name'));
            });
        } else {
            $('#recivient_data input,textarea,select').each(function() {
                $(this).attr('name', null);
            });
        }
        $('#shipping-data-form').submit();
    });
    $('#offer-verification').click(function() {
        let offer_code = $('#offer-code').val();
        let http = $(this).attr('data-api');
        axios.get(`${http}?total_passed=${Total}&offer_code=${offer_code}`).then(res => {
            let {offer, offerId} = res.data;
            if (offerId) {
                $('#off-summary').html("این سفارش با کد ورودی شامل " + offer + 'تومان تخفیف خواهد شد ')
                http = $('#offer-submit').attr('data-api');
                $('#offer-submit').attr('disabled', false).attr('data-api', http.replace('offerId', offerId));
                $('#offer-submit').focus();
            }
        });
    });
    $('#offer-submit').click(function() {
        let http = $(this).attr('data-api');
        axios.post(http).then(res => {
            let {okay, redirect} = res.data;
            if(okay) {
                window.location.href = redirect;
            }
        });
    });
    $('#offer-dismiss').click(function() {
        let http = $('#offer-submit').attr('data-api');
        http = http.replace('offerId', $('#offer-submit').attr('data-current'));
        axios.post(http, {dismiss: true}).then(res => {
            let {okay, redirect} = res.data;
            if(okay) {
                window.location.href = redirect;
            }
        });
    });
});

</script>
@endsection
