<div class="title-row mb-4 float-left">
    <h4 class="title">کد تخفیف</h4>
    <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
</div>

<div class="input-group col-md-6 col-12 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-percent"></i></span>
    </div>
    <input type="text" class="form-control" autocomplete="offer-code" data-name="offer-code" id="offer-code" value="{{ old('offer-code') }}" placeholder="کد تخفیف">
    <div class="input-group-prepend">
        <button class="btn btn-sm btn-secondary rounded-left" data-api="{{ route('api.discount', ['order' => $order->id]) }}" type="button" id="offer-verification">راستی آزمایی</button>
    </div>
    <div class="input-group-prepend">
        <button class="btn btn-sm btn-primary rounded-left" disabled @if($order->offer) data-current="{{ $order->offer->id }}" @endif
        data-api="{{ route('api.discount.apply', ['order' => $order->id, 'offer' => 'offerId']) }}" type="button" id="offer-submit">اعمال</button>
    </div>
    @if ($order->offer)
        <p id="offer-text" class="col-12 mt-3 text-secondary text-right">
            در حال حاضر از کد تخفیف {{ $order->offer->code }} استفاده می شود . <a id="offer-dismiss" class="text-danger" href="#delete">حذف <i class="fa fa-times"></i></a>
        </p>
    @else
        <p class="col-12 mt-3 text-secondary text-right">
        در صورتی که کد تخفیف دارید میتوانید آن را در کادر بالا وارد نمایید.
        </p>
    @endif
    <p class="col-12 text-success text-right" id="off-summary"></p>
</div>


