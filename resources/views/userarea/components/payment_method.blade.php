<div class="title-row mb-4 float-left">
    <h4 class="title">روش پرداختی</h4>
    <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
</div>
@foreach ($methods['payment'] as $method)
<div class="method col-md-3 col-12 float-right mb-3">
    @if ($method->photo)
        <img class="float-right ml-2 mb-2" height="50" src="{{ asset($method->photo->file->path) }}" alt="{{ $method->note }}">
    @endif
    <input type="radio" @if ($loop->last) checked @endif value="{{ $method->id }}" required name="method" id="payment-method{{ $method->id }}">
    <p>
        <label for="payment-method{{ $method->id }}">{{ $method->title }} <br> <span>{{ $method->note }}</span> </label>
    </p>
</div>
@endforeach
