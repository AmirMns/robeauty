<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-user"></i></span>
    </div>
    <input type="text" class="form-control" @if($disabled) disabled @endif placeholder="نام و نام خانوادگی گیرنده" required
    data-name="fullname" value="{{ $name ?: old('fullname') }}">
</div>
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-phone"></i></span>
    </div>
    <input type="text" class="form-control" @if($disabled) disabled @endif placeholder="شماره تلفن گیرنده" required
    data-name="phone_number" value="{{ $phone_number ?: old('phone_number') }}">
</div>
<div class="input-group col-12 col-sm-6 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
    </div>
    <input type="text" class="form-control" @if($disabled) disabled @endif placeholder="کد پستی گیرنده" required
    data-name="postal_code" value="{{ $postal_code ?: old('postal_code') }}">
</div>
<div class="col-12 float-left p-0">
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-globe"></i></span>
        </div>
        <select class="form-control" @if($disabled) disabled @endif data-name="state" data-target="{{ route('home') }}" id="states">
            @foreach ($states as $state)
                <option @if ($state_id == $state->id) selected @endif value="{{ $state->id }}">{{ $state->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-globe"></i></span>
        </div>
        <select class="form-control" @if($disabled) disabled @endif data-name="city" id="cities">
            @foreach ($cities as $city)
                <option @if ($city_id == $city->id) selected @endif value="{{ $city->id }}">{{ $city->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="input-group col-12 float-right mb-4">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
    </div>
    <input required type="text" class="form-control" @if($disabled) disabled @endif data-name="address" id="address1"
    value="{{ old('address1') ? old('address1') : $address }}" placeholder="آدرس 1">
</div>
