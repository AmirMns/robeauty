@php
    $states = \App\State::all();
    $disabled = false;
    if ($order->invoice && $order->invoice->recipient) {
        $stateId = $order->invoice->recipient->city->state_id;
        $state = \App\State::with('cities')->whereId($stateId)->first();
        $city_id  = $order->invoice->recipient->city_id;
        $state_id = $order->invoice->recipient->city->state_id;
        $address  = $order->invoice->recipient->address;
        $name = $order->invoice->recipient->fullname;
        $phone_number = $order->invoice->recipient->phone_number;
        $postal_code = $order->invoice->recipient->postal_code;
    } else {
        $stateId = $customer->city->state_id;
        $state = \App\State::with('cities')->whereId($stateId)->first();
        $city_id = $customer->city->id;
        $state_id = $customer->city->state_id;
        $address  = $customer->address1;
        $name = $customer->first_name . ' ' . $customer->last_name;
        $phone_number = $customer->phone_number;
        $postal_code = $customer->postal_code;
    }
    $cities = $state->cities;
@endphp

@if($order->invoice->completed)
    @php
        $disabled = true;
    @endphp
    <div class="col-12 float-left">
        @include('userarea.components.recieve_form')
    </div>
@else
<form action="{{ route('checkout.order.update', ['order' => $order->id]) }}" method="POST" id="shipping-data-form"  class="col-12 float-left">
    @csrf
    @method('PUT')
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <p class="ml-2"><label for="use_customer">از اطلاعات ارسالی خودم استفاده کن</label></p>
        <input value="true" type="checkbox" @if($order->invoice->use_customer) checked @endif name="use_customer" id="use_customer">
    </div>
    <div class="col-12 float-left p-0 d-none" id="recivient_data">
        @include('userarea.components.recieve_form')
    </div>
    <div class="col-12 float-left" id="invoice_update_form"></div>
    <div class="col-12 float-right text-center">
        <button type="submit" id="shipping-data-button" class="btn btn-primary">ثبت اطلاعات ارسالی</button>
    </div>
</form>
@endif
