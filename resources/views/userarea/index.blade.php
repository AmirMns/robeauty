@extends('layout.userarea')

@section('head')
    <title>ناحیه کاربری</title>
@endsection

@section('dashboardcontent')
<h3>{{ auth()->user()->name }} عزیز خوش آمدید !</h3>
@endsection
