@extends('layout.userarea')

@section('head')
    <title>ناحیه کاربری - اطلاعات حساب کاربری</title>
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
    <style>
        .select2-container {
            width: 80% !important;
            float: right;
            font-family: Iransans;
            font-size: 18px;
        }
        .select2-container .select2-selection--single {
            height: 34px !important;
            padding: 2px;
            border-radius: 4px 0 0 4px !important;
        }
    </style>
@endsection

@section('dashboardcontent')
<h3 class="col-12 mb-4 float-left">اطلاعات ارسال</h3>
<form action="{{ route('userarea.customeredit', ['customer' => $customer->id]) }}" class="col-12 mt-4 float-left" method="post">
    @csrf
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="first_name" id="first_name"
        value="{{ old('first_name') ? old('first_name') : $customer->first_name }}" placeholder="نام">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-font"></i></span>
        </div>
        <input type="text" class="form-control" name="last_name" id="last_name"
        value="{{ old('last_name') ? old('last_name') : $customer->last_name }}" placeholder="نام خانوادگی">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-hashtag"></i></span>
        </div>
        <input required type="text" class="form-control text-left" name="phone_number" id="phone_number"
        value="{{ old('phone_number') ? old('phone_number') : $customer->phone_number }}" placeholder="شماره تلفن">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
        </div>
        <input required type="text" class="form-control text-left" name="postal_code" id="postal_code"
        value="{{ old('postal_code') ? old('postal_code') : $customer->postal_code }}" placeholder="کد پستی">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-building"></i></span>
        </div>
        <input type="text" class="form-control" name="company" id="company"
        value="{{ old('company') ? old('company') : $customer->company }}" placeholder="شرکت">
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-globe"></i></span>
        </div>
        <select class="form-control" name="state" data-target="{{ route('home') }}" id="states">
            @foreach ($states as $state)
                <option @if ($customer->city->state->id == $state->id) selected @endif value="{{ $state->id }}">{{ $state->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-globe"></i></span>
        </div>
        <select class="form-control" name="city" id="cities">
            @foreach ($cities as $city)
                <option @if ($customer->city->id == $city->id) selected @endif value="{{ $city->id }}">{{ $city->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
        </div>
        <input required type="text" class="form-control" name="address1" id="address1"
        value="{{ old('address1') ? old('address1') : $customer->address1 }}" placeholder="آدرس 1">
    </div>
    <div class="input-group col-12 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-map-marker"></i></span>
        </div>
        <input type="text" class="form-control" name="address2" id="address2"
        value="{{ old('address2') ? old('address2') : $customer->address2 }}" placeholder="آدرس 2">
    </div>
    <div class="input-group col-12 float-right">
        <button type="submit" class="btn btn-secondary float-left">ذخیره</button>
    </div>
</form>
@endsection
@section('script')
    <script> $('#cities,#states').select2(); </script>
@endsection
