@extends('layout.userarea')

@section('head')
    <title>اطلاعات کاربری</title>
    <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
@endsection

@section('dashboardcontent')
<h3 class="col-12 mb-4 float-left">اطلاعات حساب کاربری</h3>
<form action="{{ route('userarea.user.update') }}" class="col-12 mt-4 float-left" method="post">
    @csrf
    @method('PUT')
    <div class="input-group col-12 col-sm-6 offset-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-user"></i></span>
        </div>
        <input disabled type="text" class="form-control" id="username" value="{{ $user->identification }}">
    </div>
    <h4 class="col-12 text-right mb-2">تغییر رمز عبور : </h4>
    <p class="col-12 text-right">رمز عبور جدید را در کادر زیر وارد کرده و سپس در کادر بعدی آنرا تایید کنید و برروی تغییر کلیک کنید</p>
    <div class="input-group col-12 mt-4 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fa fa-lock"></i></span>
        </div>
        <input required type="password" autocomplete="false" class="form-control" name="new_password" id="new_password"
        value="{{ old('new_password') }}" placeholder="رمز عبور جدید">
    </div>
    <div class="input-group col-12 mt-4 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text">2<i class="mr-1 fa fa-lock"></i></span>
        </div>
        <input required type="password" class="form-control" name="password_confirm" id="password_confirm"
        value="{{ old('password_confirm') }}" placeholder="تایید رمز عبور">
    </div>
    <div class="input-group col-12 mt-4 col-sm-6 text-center float-right mb-4">
        <button type="submit" class="btn btn-primary">تغییر رمزعبور</button>
    </div>
</form>
@endsection