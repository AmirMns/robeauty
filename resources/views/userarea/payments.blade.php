@extends('layout.userarea')

@section('head')
    <title>ناحیه کاربری - مشاهده پرداخت ها</title>
@endsection

@section('dashboardcontent')
<h3 class="col-12">پرداختی ها</h3>
<table class="table text-center">
    <thead class="thead-dark">
        <tr>
            <th scope="col">ردیف</th>
            <th scope="col">وضعیت</th>
            <th scope="col">مبلغ</th>
            <th scope="col">روش پرداخت</th>
            <th scope="col">زمان پرداخت</th>
            <th scope="col">کد تراکنش</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($payments as $payment)
        <tr>
            <th scope="row">{{ $loop->index + 1 }}</th>
            <td>{!! ifPaid($payment->paid) !!}</td>
            <td>{{ number_format($payment->amount) }}</td>
            <td>{{ $payment->method->title }}</td>
            <td>{{ $payment->updated_at->diffForHumans() }}</td>
            <td>{{ $payment->RefID }}</td>
            {{-- <td><a href="#" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></a></td> --}}
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
