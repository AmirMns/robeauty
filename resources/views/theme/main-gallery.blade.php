@extends('layout.default')
@section('head')
<title>گالری شرکت</title>
<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
@endsection
@section('content')
<header>
    @include('theme.header')
</header>
<div class="col-12 content">
    <div class="title-row float-left animated fadeInRight mb-5">
        <h4 class="title">تصاویر</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
        <div class="swiper-container productslider psl-0 col-12 mt-3 float-left">
            <div class="swiper-wrapper">
                @foreach ($images_gallery->items as $item)
                    <div class="swiper-slide">
                        <div class="cover">
                            <img src="{{ asset($item->file->path) }}" alt="">
                        </div>
                        @if ($item->title)
                        <div class="info text-center">
                            <a href="{{ $item->url }}"><h3 class="book-title">{{ $item->title }}</h3></a>
                            <p class="book-subtitle">{{ $item->description }}</p>
                        </div>
                        @endif
                    </div>
                @endforeach
            </div>
            <!-- Add Pagination -->
            <div class="swiper-button-prev"><i class="fa fa-3x fa-angle-left"></i></div>
            <div class="swiper-button-next"><i class="fa fa-3x fa-angle-right"></i></div>
        </div>
    </div>
    <div class="title-row float-left animated fadeInRight mb-5">
        <h4 class="title">ویدیو ها</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
        <div class="swiper-container productslider psl-1 col-12 mt-3 float-left">
            <div class="swiper-wrapper">
                @foreach ($videos_gallery->items as $item)
                    <div class="swiper-slide">
                        <div class="cover">
                            <img src="{{ asset($item->file->path) }}" alt="">
                            <a href="{{ $item->url }}" class="video-icon text-danger">
                                <i class="fa fa-play fa-4x"></i>
                            </a>
                        </div>
                        @if ($item->title)
                        <div class="info text-center">
                            <a href="{{ $item->url }}"><h3 class="book-title">{{ $item->title }}</h3></a>
                            <p class="book-subtitle">{{ $item->description }}</p>
                        </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="title-row float-left animated fadeInRight mb-5">
        <h4 class="title">کاتالوگ ها</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
        <div class="col-12 text-center float-left mt-4">
            <a href="{{ setting('site.catalog') }}">
            <div class="catalog btn btn-outline-danger">
                <i class="fa fa-file-pdf-o"></i>
                کاتالوگ محصولات
            </div>
            </a>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/app.js') }}"></script>
<script>var swiper0 = new Swiper.default('.psl-0', {
    slidesPerView: 4,
    spaceBetween: 10,
    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        500: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 5,
            spaceBetween: 20,
        },
        2560: {
            slidesPerView: 6,
            spaceBetween: 40
        }
    },
    navigation: {
        nextEl: '.psl-0 > .swiper-button-next',
        prevEl: '.psl-0 > .swiper-button-prev',
    },
});
var swiper0 = new Swiper.default('.psl-1', {
    slidesPerView: 4,
    spaceBetween: 10,
    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        500: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        768: {
            slidesPerView: 5,
            spaceBetween: 20,
        },
        2560: {
            slidesPerView: 6,
            spaceBetween: 40
        }
    },
    navigation: {
        nextEl: '.psl-1 > .swiper-button-next',
        prevEl: '.psl-1 > .swiper-button-prev',
    },
});</script>
@endsection