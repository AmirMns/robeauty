@extends('layout.shop')

@section('head')
<title>روبیوتی | فروشگاه</title>
@endsection

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">فروشگاه</li>
@endsection
@section('shop-content')
    <div class="col-md-9 col-12 float-left shopbar p-3 pt-0">
        <div id="react-product-section" data-target="{{ $target_url }}"></div>
    </div>
    <div class="col-md-3 col-12 float-left sidebar p-4">
        <div class="part category-part">
            <p class="title">دسته بندی ها</p>
            <ul class="categories">
                @if ($categories)
                    @foreach ($categories as $category)
                    <li class="cat-item">
                        <a
                        href="{{ route('shop.category', ['category' => $category->name]) }}">
                        {{ $category->name }}
                        <span class="badge badge-light">{{ $category->categories()->count() }}</span>
                        </a>
                        </li>
                    @endforeach
                @else
                <p>دسته بندی موجود نمی باشد</p>
                @endif
            </ul>
        </div>
        {{-- <div class="part price-part">
            <p class="title">قیمت</p>
            <form action="{{ route('shop.') }}" method="get">
                <div class="price-slider">
                    <div class="sliders col-12">
                        <input type="range" name="minprice" class="pricechange col-12 float-left" min="0"
                        step="1000" max="100000" value="1000">
                        <input type="range" name="minprice" class="pricechange col-12 float-left" min="0"
                        step="1000" max="100000" value="100000">
                    </div>
                    <div class="info col-12">

                    </div>
                </div>
            </form>
        </div> --}}
        <div class="part category-part">
            <p class="title">برند</p>
            <ul class="categories">
                @if ($categories)
                    @foreach ($brands as $brand)
                    <li class="cat-item">
                        <a
                        href="{{ route('shop.brand', ['brand' => $brand->name]) }}">
                        {{ $brand->name }}
                        <span class="badge badge-light">{{ $brand->products()->count() }}</span>
                        </a>
                        </li>
                    @endforeach
                @else
                <p>هیچ برندی موجود نمی باشد</p>
                @endif
            </ul>
        </div>
        <div class="part">
            <p class="title">برچسب ها</p>
            <div class="col-12 float-left p-0">
                @if ($tags)
                    @foreach ($tags as $tag)
                    <a class="btn btn-sm btn-outline-primary float-right m-2"
                    href="{{ route('shop.tag', ['tag' => $tag->name]) }}">
                    {{ str_replace('-', ' ',$tag->name) }} ({{ $tag->products()->count() }})
                    </a>
                    @endforeach
                @else
                <p>برچسبی موجود نمی باشد</p>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scriptcode')
<script>
var VisitProduct  = "{{ route('shop.product', ['product' => 'slug']) }}";
var VisitCategory = "{{ route('shop.category', ['category' => 'categoryName']) }}";
var CartAction = "{{ route('cart.actions', ['product' => 'productId', 'action' => 'actionName']) }}";
var Asset      = "{{ asset('/') }}";
</script>
<script src="{{ asset('js/products.js') }}"></script>
@endsection
