@extends('layout.shop')
@section('breadcrumb')
{{-- <li class="breadcrumb-item"><a href="{{ route('home') }}">خانه</a></li> --}}
<li class="breadcrumb-item p-0"><a href="{{ route('shop.') }}">فروشگاه</a></li>
<li class="breadcrumb-item active" aria-current="page">سبد خرید</li>
@endsection
@section('shop-content')
<div class="content col-12 p-3" style="min-height:500px;">
    <div class="cart col-12">
        @if ($cart && count($cart->items) > 0)
        <div class="title-row float-left">
            <h4 class="title">سبد خرید ({{ count($cart->items) }}) :</h4>
            <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
        </div>
        <table class="table text-center">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">عنوان</th>
                    <th scope="col">تعداد</th>
                    <th scope="col">قیمت واحد</th>
                    <th scope="col">مجموع</th>
                    <th scope="col">تخفیف</th>
                    <th scope="col">مجموع پس از کسر</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cart->items as $item)
                <tr>
                    <th scope="row">{{ $loop->index + 1 }}</th>
                    <td>{{ $item['item']->title }}</td>
                    <td>
                        <a href="{{ route('cart.actions', ['product' => $item['item']->id, 'action' => 'remove']) }}" class="badge badge-secondary"><i class="fa fa-minus"></i></a>
                        {{ $item['quantity'] }}
                        <a href="{{ route('cart.actions', ['product' => $item['item']->id, 'action' => 'add']) }}" class="badge badge-secondary"><i class="fa fa-plus"></i></a>
                    </td>
                    <td>{{ $item['item']->price }}</td>
                    <td>{{ $item['price'] }}</td>
                    <td>%{{ zeroIfNull($item['item']->off) }}</td>
                    <td>{{ $item['item']->offer() * $item['quantity'] }}</td>
                </tr>
                @endforeach
                <tr>
                    <th scope="row">مجموع : </th>
                    <td>---</td>
                    <td>{{ $cart->totalQuantity }}</td>
                    <td>---</td>
                    <td>{{ $cart->totalPrice }}</td>
                    <td>---</td>
                    <td>{{ $cart->totalPay }}</td>
                </tr>
            </tbody>
        </table>
        <div class="col-12 text-left float-left">
            <a class="btn btn-primary cart" href="{{ route('checkout.makeorder') }}">نهایی کردن</a>
            <a class="btn btn-danger cart" href="{{ route('cart.forget') }}"><i class="fa fa-trash"></i> خالی کردن سبد</a>
        </div>
        @else
        <div class="title-row float-left">
            <h4 class="title alert alert-danger">سبد خرید شما خالی است</h4>
        </div>
        @endif
    </div>
</div>
@endsection
