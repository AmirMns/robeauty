@extends('layout.shop')

@section('head')
<title>{{ $product->title }} | فروشگاه روبیوتی</title>
@endsection

@section('breadcrumb')
<li class="breadcrumb-item p-0"><a href="{{ route('shop.') }}">فروشگاه</a></li>
<li class="breadcrumb-item active">{{ $product->title }}</li>
@endsection
@section('shop-content')
    <!-- Swiper -->
    <div class="product-slider col-md-4 p-0 float-right col-12">
        <div class="sliders-container col-12">
            <div class="swiper-container gallery-top">
                <div class="swiper-wrapper">
                @foreach ($product->photos as $photo)
                    <div class="swiper-slide top"><img src="{{ asset($photo->file->path) }}" alt=""></div>
                @endforeach
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
            </div>
            @if (count($product->photos) > 1)
                <div class="swiper-container gallery-thumbs">
                    <div class="swiper-wrapper">
                    @foreach ($product->photos as $photo)
                        <div class="swiper-slide thumb"><img src="{{ asset($photo->file->path) }}" alt=""></div>
                    @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="pr-section col-md-8 pl-0 col-12">
        <div class='product-info-container col-md-9 col-12'>
            <h1 class='product-name'>{{ $product->title }}</h1>
            <div class="col-12 float-left text-right">
                <div class="float-right ml-2">
                    @if ($product->quantity > 0)
                        <span class="badge badge-success">{{ $product->quantity }} عدد موجود است</span>
                    @else
                        <span class="badge badge-danger">ناموجود</span>
                    @endif
                </div>
                <div class="product-info ml-2">
                    <p>برند : <a href="{{ route('shop.brand', ['brand' => $product->brand->name]) }}">
                    <span>{{ $product->brand->name }}</span>
                    </a></p>
                </div>
                <div class="product-info">
                    <p> دسته بندی :
                    @foreach ($product->categories as $category)
                        <span>{{ $category->name }}</span>
                    @endforeach </p>
                </div>
            </div>
            <div class="product-properties">
                <div class="col-12 col-md-6 float-right persian">
                    <h5>ویژگی های محصول</h5>
                    <p>{{ $product->short_description }}</p>
                </div>
                <div class="col-12 col-md-6 float-right text-left" style="direction: ltr;">
                    {!! $product->english_description !!}
                </div>
            </div>
            <p class="price-section text-right float-left col-12">{!! $product->displayPrice() !!}</p>
            <br/>
            <div class="col-12 mt-4 float-left text-center">
                @if ($product->quantity > 0)
                <a href="{{ route('cart.actions', ['product' => $product->id, 'action' => 'add']) }}" class='btn btn-dark btn-large'>افزودن به سبد خرید <i class="fa fa-shopping-cart"></i></a>
                @else
                <button disabled type="button" class="btn btn-dark btn-large">در انبار موجود نیست</button>
                @endif
            </div>
        </div>
        {{-- <div class="buy-section-container col-md-5 col-12">
            <ul>
                <li>گارانتی اصالت و سلامتی فیزیکی کالا</li>
                <li>زیر قیمت بازار</li>
                <li>آماده ارسال</li>
                <li>بهترین کیفیت</li>
            </ul>

            <br/>
        </div> --}}
    </div>
    <div class="pro-desc col-12 float-left p-3">
        <div class="card text-center col-12 p-0 float-right">
            <div class="card-header bg-dark">
                <ul class="nav nav-tabs card-header-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">توضیحات</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <article class="col-12 text-right float-right p-3">
                    {!! $product->description !!}
                </article>
            </div>
        </div>
    </div>
@endsection

@section('scriptcode')
<!-- Initialize Swiper -->
<script>
    var galleryThumbs = new Swiper.default('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 3,
    loop: true,
    freeMode: true,
    loopedSlides: 5, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    });
    var galleryTop = new Swiper.default('.gallery-top', {
    spaceBetween: 10,
    loop:true,
    slidesPerView: 1,
    loopedSlides: 5, //looped slides should be the same
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
    });</script>
@endsection
