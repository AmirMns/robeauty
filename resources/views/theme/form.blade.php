@extends('layout.default')
@section('head')
<title>{{ setting('site.company-name') }} - فرم مشاوره</title>
@endsection
@section('content')
    <header>
        @include('theme.header')
    </header>
    <div class="col-12 content">
        <div class="col-12 mb-4 section scroll-animations">
            <div class="title-row float-left animated fadeInRight">
                <h2 class="title">فرم مشاوره</h2>
                <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
            </div>
            <p class="col-md-8 offset-md-2 col-12 offset-0 text-center float-left">
            دوست عزیز سلام.
<br>
جهت مشاوره در امر زیبائی و سلامت مو و پوست سر ، لطفا سوال خود را پس از تکمیل موارد ذیل مطرح فرمائید.
            </p>
            <form class="col-md-6 offset-md-3 col-12 offset-0 float-left" action="{{ route('form.store') }}" method="post">
                @csrf
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                    </div>
                    <input type="text" name="name" required class="form-control" placeholder="نام و نام خانوادگی">
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone"></i></span>
                    </div>
                    <input type="text" name="phone" required class="form-control" placeholder="شماره تماس">
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                    </div>
                    <input type="text" name="email" class="form-control" placeholder="ایمیل">
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">حوزه فعالیت</span>
                    </div>
                   <select required class="form-control" name="bussiness">
                       <option selected value="مصرف کننده">مصرف کننده</option>
                       <option value="فروشنده محصولات">فروشنده محصولات</option>
                       <option value="بازاریاب">بازاریاب</option>
                       <option value="آرایشگر">آرایشگر</option>
                       <option value="سایر">سایر</option>
                   </select>
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">چگونه با ما آشنا شدید</span>
                    </div>
                   <select required class="form-control" name="knowing">
                       <option selected value="تبلیغات">تبلیغات</option>
                       <option value="استفاده از محصولات ما">استفاده از محصولات ما</option>
                       <option value="شبکه های اجتماعی">شبکه های اجتماعی</option>
                       <option value="با معرفی دوستان و آشنایان">با معرفی دوستان و آشنایان</option>
                       <option value="ارتباط با شرکت">ارتباط با شرکت</option>
                   </select>
                </div>
                <div class="input-group float-right col-12 mb-3">
                    <textarea required minlength="10" maxlength="700" class="form-control float-right" name="question" id="question" cols="30" rows="10" placeholder="سوال (جهت راهنمایی دقیق تر و بهتر در امر مشاوره لطفا سوال خود را با ذکر جزئیات ، کامل بپرسید) "></textarea>
                    <div class="col-12 mt-3 p-0 float-right">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('theme.components.footer')
@endsection
@section('script')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
