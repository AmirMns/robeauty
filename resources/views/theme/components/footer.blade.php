<footer class="footer p-3">
    <div class="col-md-3 col-12 text-right">
        <h5 class="title">{{ setting('footer.section1.title') }}</h5>
        {!! setting('footer.section1') !!}
    </div>
    <div class="col-md-3 col-12">
        <h5 class="title">{{ setting('footer.section2.title') }}</h5>
        {!! setting('footer.section2') !!}
    </div>
    <div class="col-md-3 col-12">
        <h5 class="title">{{ setting('footer.section3.title') }}</h5>
        {!! setting('footer.section3') !!}
    </div>
    <div class="col-md-3 col-12">
        <h5 class="title">{{ setting('footer.section4.title') }}</h5>
        {!! setting('footer.section4') !!}
    </div>
</footer>
