<div class="swiper-container" id="slider">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        {!! setting('header.slider') !!}
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>
    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"><i class="fa fa-4x fa-angle-left"></i></div>
    <div class="swiper-button-next"><i class="fa fa-4x fa-angle-right"></i></div>
</div>
