@extends('layout.default')
@section('head')
<title>{{ setting('site.company-name') }} - تماس با ما</title>
@endsection
@section('content')
    <header>
        @include('theme.header')
    </header>
    <div class="col-12 content">
        <div class="col-12 mb-4 section scroll-animations">
            <div class="title-row float-left animated fadeInRight">
                <h4 class="title">تماس با ما</h4>
                <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
            </div>
            <form class="col-md-6 offset-md-3 col-12 offset-0 float-left" action="{{ route('form.store') }}" method="post">
                @csrf
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                    </div>
                    <input type="text" name="name" required class="form-control" placeholder="نام و نام خانوادگی">
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-phone"></i></span>
                    </div>
                    <input type="text" name="phone" required class="form-control" placeholder="شماره تماس">
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope"></i></span>
                    </div>
                    <input type="text" name="email" class="form-control" placeholder="ایمیل">
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">حوزه فعالیت</span>
                    </div>
                   <select required class="form-control" name="bussiness">
                       <option selected value="مصرف کننده">مصرف کننده</option>
                       <option value="فروشنده محصولات">فروشنده محصولات</option>
                       <option value="بازاریاب">بازاریاب</option>
                       <option value="آرایشگر">آرایشگر</option>
                       <option value="سایر">سایر</option>
                   </select>
                </div>
                <div class="input-group float-right col-lg-6 col-md-12 mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">چگونه با ما آشنا شدید</span>
                    </div>
                   <select required class="form-control" name="knowing">
                       <option selected value="تبلیغات">تبلیغات</option>
                       <option value="استفاده از محصولات ما">استفاده از محصولات ما</option>
                       <option value="شبکه های اجتماعی">شبکه های اجتماعی</option>
                       <option value="با معرفی دوستان و آشنایان">با معرفی دوستان و آشنایان</option>
                       <option value="ارتباط با شرکت">ارتباط با شرکت</option>
                   </select>
                </div>
                <div class="input-group float-right col-12 mb-3">
                    <textarea required minlength="10" maxlength="700" class="form-control float-right" name="question" id="question" cols="30" rows="10" placeholder="سوال (جهت راهنمایی دقیق تر و بهتر در امر مشاوره لطفا سوال خود را با ذکر جزئیات ، کامل بپرسید) "></textarea>
                    <div class="col-12 mt-3 p-0 float-right">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-12 mb-4 section scroll-animations contactus-section">
			<div class="contact-datas">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p class="titlec">آدرس</p><p>{{ setting('site.address') }}</p></div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p class="titlec">شماره تماس</p><p class="phone">{!! setting('site.phone') !!}</p></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p class="titlec">ایمیل</p><p>{{ setting('site.email') }}</p></div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><p class="titlec">ساعات کاری</p><div class="working-date"><p class="days">{{ setting('site.workdays') }}</p><p class="timed">{{ setting('site.workhours') }} </p></div></div>
			</div>
			<div class="backmap">
				<div>
				<p><i class="fa fa-map-marker fa-3x"></i><span>{{ setting('site.company-name') }}<br>{{ setting('site.company-subtitle') }}</span></p>
				<p class="ads">{{ setting('site.address') }}</p>
				<a class="btn btndir" target="_blank" href="{{ setting('site.googlemaps') }}">دریافت مسیر ها<i class="fa fa-angle-right"></i></a>
				</div>
			</div>
		</div>
    </div>
    @include('theme.components.footer')
@endsection
@section('script')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
