@extends('layout.default')
@section('head')
<title>{{ setting('site.company-name') }} - {{ $gallery->title }}</title>
<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
@endsection
@section('content')
<header>
    @include('theme.header')
</header>
<div class="col-12 content">
    <div class="title-row float-left animated fadeInRight mt-3 mb-5">
        <h4 class="title">{{ $gallery->title }}</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
    </div>
        <div class="gallery-container col-12 mb-5">
            @foreach ($gallery->items as $item)
            @if($item->url)<a href="{{ $item->url }}">@endif
            <div class="col-md-2 col-6 float-left text-center">
                <img class="float-left h-auto col-12" src="{{ asset($item->file->path) }}" alt="{{ $item->title }} - روبیوتی">
                <div class="text-dark">
                    <p class="title">{{ $item->title }}</p>
                    <p class="persian">{{ $item->description }}</p>
                </div>
            </div>
            @if($item->url)</a>@endif
            @endforeach
        </div>
    </div>
</div>
@endsection
