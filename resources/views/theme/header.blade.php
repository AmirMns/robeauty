@php
    $cart = \Session::get('cart', null);
@endphp
<div class="topbar">
    <div class="col-md-6 col-12">
        <p><i class="fa fa-phone"></i>{!! setting('header.phone') !!}</p>
        <p><i class="fa fa-envelope-open-o"></i><a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a></p>
        <ul class="socialmedia">
            {!! setting('header.socialmenu') !!}
        </ul>
    </div>
    <div class="col-md-6 col-12"></div>
</div>
<div class="navbar">
    <div class="col-md-2 col-4 logo"><a href="{{ route('home') }}"><img src="{{ asset(setting('site.logo')) }}" alt="Robeauty"></a></div>
    <div class="col-lg-6 offset-lg-1 col-md-7 offset-md-1 col-12 offset-0 menu">
        <nav class="main-menu">
            <ul>
                <li><a href="{{ route('home') }}">صفحه اصلی</a></li>
                <li><a href="{{ route('shop.') }}">فروشگاه</a></li>
                {{-- <li><a href="/cosmetics">محصولات آرایشی</a></li> --}}
                <li><a href="{{ asset('gallery/main-gallery') }}">گالری شرکت</a></li>
                <li><a href="{{ route('gallery.show', ['gallery' => 'گالری-محصولات']) }}">تصاویر محصولات</a></li>
                <li><a href="{{ route('about') }}">درباره ما</a></li>
                <li><a href="{{ route('contact') }}">تماس با ما</a></li>
                <li><a id="search-toggle" href="{{ route('pages.search') }}">جستجو <i class="fa fa-search mr-2"></i></a></li>
            </ul>
        </nav>
    </div>
    <div class="col-md-3 col-8 control-panel">
        @guest
        <a href="{{ route('register') }}"><i class="fa fa-user fa-2x"></i><span class="d-sm-inline d-none">عضویت</span></a>
        <a href="{{ route('login') }}"><i class="fa fa-lock fa-2x"></i><span class="d-sm-inline d-none">ورود</span></a>
        @else
        <a href="{{ route('userarea.') }}"><i class="fa fa-user fa-2x"></i><span class="d-sm-inline d-none">{{ auth()->user()->name }} - ناحیه کاربری</span></a>
        @endguest
        <div class="mini-cart" data-cart="{{ route('cart.') }}" data-main="{{ asset('/') }}" data-product={{ route('shop.product', ['product' => 'productId']) }}>
            <i class="fa fa-shopping-bag fa-2x"></i>
            <span class="d-sm-inline d-none">سبد خرید</span>
            <span class="badge badge-pill badge-light">{{ ($cart) ? count($cart->items ): 0 }}</span>
            <div class="flow-cart">
                @if ($cart && $cart->items)
                    @foreach ($cart->items as $item)
                    <div class="cart-item p-3">
                        @if ($item['item']->photo)
                            <img src="{{ $item['item']->photo }}" alt="">
                        @endif
                        <p class="text-right">
                            <a href="{{ route('shop.product', ['product' => $item['item']->slug]) }}">
                            {{ $item['item']->title }}({{ $item['quantity'] }}x)
                            <br>
                            قیمت واحد : {{ number_format($item['item']->price) }} تومان
                            <b>مجموع: {{ number_format($item['price']) }} تومان</b>
                            </a>
                        </p>
                    </div>
                    @endforeach
                @endif
                <a class="btn btn-sm btn-dark float-left m-3" href="{{ route('cart.') }}">مشاهده سبد <i class="fa fa-shopping-bag fa-2x"></i></a>
            </div>
        </div>
        <div id="openmenu" class="btn btn-light m-0 mr-4 d-sm-none d-block"><i class="fa fa-2x fa-bars"></i></div>
    </div>
</div>
