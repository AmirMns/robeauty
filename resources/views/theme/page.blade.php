@extends('layout.default')
@section('head')
<title>{{ setting('site.title') }}</title>
@endsection
@section('content')
    <header>
        @include('theme.header')
        @include('theme.components.slider')
    </header>
    <div class="col-12 content">
        <div class="col-12 section banner-section scroll-animations">
            <a href="{{ setting('mainpage.banner1.url') }}">
                <div class="col-md-3 col-12 banner">
                    <div class="card animated" data-animation="fadeInLeft">
                        <img src="{{ setting('mainpage.banner1') }}" alt="">
                    </div>
                </div>
            </a>
            <a href="{{ setting('mainpage.banner2.url') }}">
                <div class="col-md-6 col-12 banner">
                    <div class="card-full animated" data-animation="fadeInUp">
                        <img src="{{ setting('mainpage.banner2') }}" alt="">
                    </div>
                </div>
            </a>
            <a href="{{ setting('mainpage.banner3.url') }}">
                <div class="col-md-3 col-12 banner">
                    <div class="card animated" data-animation="fadeInRight">
                        <img src="{{ setting('mainpage.banner3') }}" alt="">
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 mb-4 section scroll-animations">
            <div class="title-row float-left animated" data-animation="fadeIn slow delay-1s">
                <h4 class="title">پیشنهادات ویژه</h4>
                <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
            </div>
            <div class="swiper-container productslider psl-2 col-12 mt-3 float-left animated" data-animation="slideInRight fast">
                <div class="swiper-wrapper">
                    @foreach ($products as $product)
                    <div class="swiper-slide">
                        <div class="cover">
                            @if (count($product->photos) > 0)
                                <img src="{{ asset($product->photos[0]->file->path) }}" alt="">
                            @endif
                        </div>
                        <div class="info">
                            <h3 class="book-title">{{ $product->title }}</h3>
                            <p class="book-subtitle">{{ $product->short_description }}</p>
                            <p>{{ number_format($product->price) }} تومان</p>
                            <div class="generes">
                                @foreach ($product->categories as $category)
                                    <a href="#" class="badge badge-secondary badge-pill">{{ $category->name }}</a>
                                @endforeach
                            </div>
                            <div class="generes">
                                <a href="{{ route('shop.product', ['product' => $product->slug]) }}" class="btn btn-secondary float-left">مشاهده</a>
                                <a class="btn btn-secondary float-left" href="{{ route('cart.actions', ['product' => $product->id, 'action' => 'add']) }}"
                                class="btn"><i class="fa fa-shopping-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-button-prev"><i class="fa fa-3x fa-angle-left"></i></div>
                <div class="swiper-button-next"><i class="fa fa-3x fa-angle-right"></i></div>
            </div>
        </div>
        <div class="col-12 mb-4 section">
            <div class="title-row float-left">
                <h4 class="title">محصولات جدید</h4>
                <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
            </div>
            <div class="swiper-container productslider psl-0 col-12 mt-3 float-left">
                <div class="swiper-wrapper">
                    @foreach ($products2 as $product)
                    <div class="swiper-slide">
                        <div class="cover">
                            @if (count($product->photos) > 0)
                                <img src="{{ asset($product->photos[0]->file->path) }}" alt="">
                            @endif
                        </div>
                        <div class="info">
                            <a href="{{ route('shop.product', ['product' => $product->slug]) }}"><h3 class="book-title">{{ $product->title }}</h3></a>
                            <p class="book-subtitle">{{ $product->short_description }}</p>
                            <p>{{ number_format($product->price) }} تومان</p>
                            <div class="generes">
                            @foreach ($product->categories as $category)
                                <a href="#" class="badge badge-secondary badge-pill">{{ $category->name }}</a>
                            @endforeach
                            </div>
                            <div class="generes">
                                <a href="{{ route('shop.product', ['product' => $product->slug]) }}" class="btn btn-secondary float-left">مشاهده</a>
                                <a class="btn btn-secondary float-left" href="{{ route('cart.actions', ['product' => $product->id, 'action' => 'add']) }}"
                                class="btn"><i class="fa fa-shopping-cart"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-button-prev"><i class="fa fa-3x fa-angle-left"></i></div>
                <div class="swiper-button-next"><i class="fa fa-3x fa-angle-right"></i></div>
            </div>
        </div>
        <div class="col-12 mb-4 section">
            <div class="title-row float-left">
                <h4 class="title">برند ها</h4>
                <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3"></span>
            </div>
            <div class="swiper-container brandslider psl-3 col-12 mt-3 float-left">
                <div class="swiper-wrapper">
                    @foreach ($brands as $brand)
                        <div class="swiper-slide">
                            <a href="{{ route('shop.brand', ['brand' => $brand->name]) }}">
                            <div class="cover">
                                <img src="{{ asset($brand->photo->file->path) }}" alt="">
                            </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-button-prev"><i class="fa fa-3x fa-angle-left"></i></div>
                <div class="swiper-button-next"><i class="fa fa-3x fa-angle-right"></i></div>
            </div>
        </div>
    </div>
    @include('theme.components.footer')
@endsection
@section('script')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
    var mySwiper = new Swiper.default('#slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#slider > .swiper-button-next',
            prevEl: '#slider > .swiper-button-prev',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        }
    });
    var swiper0 = new Swiper.default('.psl-0', {
        slidesPerView: 4,
        spaceBetween: 10,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            500: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 20,
            },
            2560: {
                slidesPerView: 6,
                spaceBetween: 40
            }
        },
        navigation: {
            nextEl: '.psl-0 > .swiper-button-next',
            prevEl: '.psl-0 > .swiper-button-prev',
        },
    });
    var swiper1 = new Swiper.default('.psl-1', {
        slidesPerView: 4,
        spaceBetween: 10,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            500: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 20,
            },
            2560: {
                slidesPerView: 6,
                spaceBetween: 40
            }
        },
        navigation: {
            nextEl: '.psl-1 > .swiper-button-next',
            prevEl: '.psl-1 > .swiper-button-prev',
        },
    });
    var swiper2 = new Swiper.default('.psl-2', {
        slidesPerView: 4,
        spaceBetween: 10,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            500: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 20,
            },
            2560: {
                slidesPerView: 6,
                spaceBetween: 40
            }
        },
        navigation: {
            nextEl: '.psl-2 > .swiper-button-next',
            prevEl: '.psl-2 > .swiper-button-prev',
        },
    });
    var swiper3 = new Swiper.default('.psl-3', {
        slidesPerView: 4,
        spaceBetween: 10,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            500: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
            2560: {
                slidesPerView: 4,
                spaceBetween: 40
            }
        },
        navigation: {
            nextEl: '.psl-3 > .swiper-button-next',
            prevEl: '.psl-3 > .swiper-button-prev',
        },
    }); </script>
@endsection
