@extends('layout.shop')

@section('head')
<title>@if($query) جستجوی "{{ $query }}" @else جستجو @endif| {{ setting('site.title') }}</title>
@endsection

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">@if($query) جستجوی "{{ $query }}" @else جستجو @endif</li>
@endsection
@section('shop-content')
    <div class="col-md-10 offset-md-1 col-12 float-left shopbar p-3 pt-0">
        <form method="GET" @if (! $query) style="height:400px;" @endif class="col-12 float-left p-3" action="{{ route('pages.search') }}">
            <div class="col-md-4 col-12 float-right input-group px-0">
                <input minlength="3" maxlength="80" type="text" name="query" id="query" value="{{ old('query') ?: $query_value }}" class="form-control">
                <div class="input-group-prepend">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
        @if ($query)
            <div id="react-product-section" data-target="{{ $target_url }}"></div>
        @endif
    </div>
@endsection
@section('scriptcode')
<script>
var VisitProduct  = "{{ route('shop.product', ['product' => 'slug']) }}";
var VisitCategory = "{{ route('shop.category', ['category' => 'categoryName']) }}";
var CartAction = "{{ route('cart.actions', ['product' => 'productId', 'action' => 'actionName']) }}";
var Asset      = "{{ asset('/') }}";
</script>
<script src="{{ asset('js/products.js') }}"></script>
@endsection