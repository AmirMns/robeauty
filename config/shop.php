<?php
return [
    'not-avilable-title' => 'محصولات زیر در تعداد درخواستی سبد شما در همین لحظه در انبار ما موجود نیست' . 'لطفا به تعداد ذکر شده در فهرست زیر جلوی هر آیتم از سبد خرید خود کم کنید و یا آن آیتم را برای ادامه حذف کنید'
];
