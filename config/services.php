<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'zarinpal' => [
        // 'merchantID' => '4ff63174-57ee-11ea-8497-000c295eb8fc',
        'merchantID' => 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX',
        'zarinGate' => false,
        'sandbox' => true,
    ],

    'sadad' => [
        'key' => 'kLheA+FS7MLoLILVESE3v3/FP07uLaRw', // 'Jd5uV9M+bzED5tfqFEPKzEt5FM1WT4o1',
        'merchant_id' => '000000140212149', // '000000140333643',
        'terminal_id' => '24000615', // '24089031'
    ], // callcenter@sadadpsp.ir terminal merchant new ip melli code

    'sms' => [
        'api_token' => "JWHFm5dTKYiuKxUQGH5tcsX-oSLlEBKcAhgruZ5LEEY=",
        'send_payment_sms' => false,
        'from' => "+9810005571",
        'verification_pattern' => "12hxje3kiy"
    ],

    'manager' => 'babakkhoram3020@gmail.com'
];
