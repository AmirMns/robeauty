<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $offers = [10,20,null,null,null,null,null,null,null,5];
    return [
        'title' => $faker->colorName,
        'description' => $faker->paragraph,
        'short_description' => $faker->sentences(2, true),
        'price' => (rand(1, 200) * 1000),
        'off' => $offers[rand(0,9)],
        'special_price' => null,
        'weight' => null,
    ];
});
