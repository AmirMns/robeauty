<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GallerySeeder::class);
        // factory(User::class, 10)->create();
        // $user = new User;
        // $user->name = "یونس طهرانی";
        // $user->password = bcrypt("Yoonu$1328");
        // $user->email = "yoonustehrani28@gmail.com";
        // $user->api_token = \Illuminate\Support\Str::random(12);
        // $user->save();
        // $role = new Role;
        // $role->name = "ADM";
        // $role->title = "ادمین";
        // $role->save();
        // $user->assignRole($role->id);
        // $this->call(IranSeeder::class);
    }
}
