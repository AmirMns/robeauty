<?php

use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = \App\File::where('id', '>', 109)->get();
        $gallery = \App\Gallery::whereId(2)->first();
        foreach ($files as $f) {
            $gallery->items()->create(['file_id' => $f->id]);
        }
    }
}
