<?php

use App\Iran;
use Illuminate\Database\Seeder;

class IranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $iran = new Iran;
        $provinces = $iran->all();
        $iran = new App\Iran;
        $provinces = $iran->all();
        foreach ($provinces as $province) {
            $cities = $province['cities'];
            $state = new App\State;
            $state->name = $province['Province']['province_nameFa'];
            $state->save();
            foreach ($cities as $city) {
                $state->cities()->create(['name' => $city['city_name']]);
            }
        };
    }
}
