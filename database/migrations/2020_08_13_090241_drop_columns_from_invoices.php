<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFromInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->addColumn('boolean', 'use_customer', ['default' => true]);
            $table->dropForeign('invoices_city_id_foreign');
            $table->dropColumn(['city_id','valid']);
            $table->addColumn('boolean', 'completed', ['default' => false, 'after']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
